// scripts comunes a todos los módulos ..... ! ... jjy
/**
  A CONTINUACION FUNCIONES DEPENDIENTES DE JQUERY ! ...... jjy
**/
$(document).ready(function(){

  $('.menu-secundario > ul > li > a').click(function(){
    $(this).parent().parent().children('li.active, li.activo').removeClass('act-ive activo');
    $(this).parent('li').addClass('act-ive activo'); // active .. boostrap 3.0 ... jjy v2
  });

  //para crear el efecto readonly en los checkbox! ... !!! para los componentes osti! ... jjy
  $(':checkbox[readonly=readonly]').click(function(){
    return false;        
  });

  //selecciona el contenido cuando se le da el focus ... jjy
  $('.formulario input').focus(function(){
    $(this).select();
  });

  // se renumeran TODOS los tabindex del documento!! ... jjy
  var indice = 1; 
  $('input, select, textarea, a.btn').not('.oculto').each( function(){
    if (this.type != "hidden") {
      $(this).attr( 'tabindex' , indice );
      indice++;
    }
  });

  // se posiciona el puntero en el primer objeto de entrada ... jjy
  $('input, select, textarea').not('.oculto, [type="hidden"]').first().focus();

  //control de elementos de aparicion en hover !!!!!!!!!!!!!!!!!!!!! ... jjy
  $('.solo-visible-hover').addClass('invisible');
  $('.hover-activo').mouseover(function(){
    $(this).children('.solo-visible-hover').removeClass('invisible');
  })
  .mouseout(function(){
    $(this).children('.solo-visible-hover').addClass('invisible');
  });

});

if (jQuery.validator) {

  /************** PROBANDO INTEGRACION DE JS GENERICO ************************/
  jQuery.extend( jQuery.validator.messages, {
      required: "El campo es requerido.",
      remote: "Corrija el error.",
      email: "Ingrese una dirección de correo-e válida",
      url: "Ingrese una URL válida.",
      date: "Ingrese una fecha válida.",
      dateISO: "Ingrese una fecha (ISO) válida.",
      number: "Ingrese un número válido.",
      digits: "Ingrese sólo dígitos.",
      creditcard: "Ingrese un número de TDC válido.",
      equalTo: "Ingrese el mismo valor.",
      accept: "Ingrese valores con extensiones válidas.",
      maxlength: jQuery.validator.format("Ingrese más de {0} caracteres."),
      minlength: jQuery.validator.format("Ingrese al menos {0} caracteres."),
      rangelength: jQuery.validator.format("Ingrese un valor con una longitud entre {0} y {1}."),
      range: jQuery.validator.format("Ingrese un valor entre {0} y {1}."),
      max: jQuery.validator.format("Ingrese un valor mayor o igual a {0}."),
      min: jQuery.validator.format("Ingrese un valor mayor o igual a {0}.")
  });

  jQuery.validator.addMethod(
    "esta_en_lista", 
    function(value, lista) {
      var in_array = $.inArray(value.toUpperCase(), lista);
      if (in_array == -1) {
          return false;
      }else{
          return true;
      }
    }, 
    "El valor no es valido (según lista)"
  );
  
  jQuery.validator.setDefaults({
    focusCleanup: true,
    focusInvalid: false,
    errorClass: "campo_con_errores",
    onkeyup: false,
    onfocusout: false,
    errorContainer: '#mensaje_validacion',
    errorLabelContainer: '#mensaje_validacion p',
    wrapper: "div",
    invalidHandler: function(event, validator) {
      // 'this' refers to the form
      var errors = validator.numberOfInvalids();
      if (errors) {
        var message = errors == 1
          ? 'Existe 1 error en los datos suministrados.'
          : 'Existen ' + errors + ' errores en los datos suministrados.';
        $("div.mensaje .msj").html(message);
        $("div.mensaje").removeClass('invisible');
        setTimeout('desvanecer_mensaje()', 3000);
      } else {
        $("div.mensaje").addClass('invisible');
      }
    }

  });

} // FIN funciones JQUERY validate! ... 


// **************** funciones genéricas .... jjy v2 ******************

function desvanecer_mensaje(){
  $(".mensaje").fadeOut("fast");
}

// EN EVALUACION !!!!!! forma parte de un COMPONENTE! ... jjy v2
function armar_filtro_grid( id_dialogo ){
  var columna_filtro  = $( '#f_filtro_grid_'+id_dialogo+'>#l_columna_filtro' ).val();
  var operador_filtro = $( '#f_filtro_grid_'+id_dialogo+'>#l_operador_filtro' ).val();
  var terminos_filtro = $( '#f_filtro_grid_'+id_dialogo+' #t_terminos_filtro' ).val();
  var filtro = columna_filtro + ':' + operador_filtro + ":" + terminos_filtro;
  return filtro;
}

(function($){
    $(function(){
        $('input[type="text"]').each(function() {
            var input = $(this);
            $(this).setMask($(this).attr('alt'));
        });
        //$('#decimal_data-mask, #date-mask').setMask({'attr': 'data-mask'});
    });
})(jQuery);
