<?php
/**
 * Plantilla-OSTI 2.1b - Mayo 2014 (04-05-2014) ...
 * Estructura basada en CodeIgniter para la creación de aplicaciones en el INN-CCS-VE
 * 
 * @author jjyepez - jyepez@inn.gob.ve + Equipo de desarrollo OSTI-INN
 *
 * @package plantilla-osti
 * @version 2.1b
 * @license http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es
 * 
 */
/* *******************************************************************
| Datos sobre la aplicación ...
|
| NOVEDADES en la v2.1b 
|  - Lista para INTRANET INN (modulo tipo portal - demo)
|  - Implementados scroll personalizados
|  - Implementado formato en componentes
|  - Mejoras en los componentes 2.1.x
| 
| POR HACER
|  - Corregir manejo de sesiones de usuarios
|  - Mejorar el manejo de scrollbars para IFRAME de módulos
|  - etc, etc ... 
| 
|  --
|  Adelante!
|  jjy
|
/* ******************************************************************/
/*
      ___  __  __________       _____    __    __ 
     /___\/ _\/__   \_   \      \_   \/\ \ \/\ \ \
    //  //\ \   / /\// /\/____   / /\/  \/ /  \/ /
   / \_// _\ \ / //\/ /_|_____/\/ /_/ /\  / /\  / 
   \___/  \__/ \/ \____/      \____/\_\ \/\_\ \/  2014 (¡Chávez Vive!)
       jjy                                               

*/
session_start();

global $config;

$script = $_SERVER['PHP_SELF'];
$base_url = str_replace( '/index.php', '/', $script );

$config ['aplicacion'] = array(

  'plantilla_osti'  => 'v2.1b',
  'id'              => 's/i',
  'nombre_completo' => 's/i',
  'nombre_corto'    => 's/i',
  'version_mayor'   => 's/i',
  'version_menor'   => 's/i',
  
  'mostrar_encabezado_gobierno' => TRUE,
  
  'descripcion'     => 's/i',
  'mostrar_logo'    => FALSE,
  'mostrar_titulo'  => TRUE,
  'mostrar_portada' => TRUE,
  
  'mostrar_fondos_al_azar' => TRUE,
  'imagen_portada'         => 'fondo-7v2.jpg',
  'imagen_fondo'           => 'fondo1_v2.png',

  'tema_bs'         => 'ninguno', // tema base de bootstrap ... jjy v2

  'tiempo_sesion'   => "4800", // 5 minutos (60 * 5)

  'entorno'         => 'PRODUCCION', //// 'PRODUCCION' // para que no muestre la pestaña corrediza ... por ahora ! ... jjy v2

  'ancho_maximo'    => '100%',
);

if ( ! isset ( $_SESSION ['sesion'] ) ) {
  $_SESSION = array( 'sesion' => array(
      'codigo_usuario' => "",
      'cuh'            => "",
    ),
  );
}

//se cargan los datos específicos de la aplicación ... según el nombre del directorio RAIZ! ... jjy v2
$info_archivo = pathinfo( $_SERVER["SERVER_NAME"] . $base_url );
$archivo_configuracion_app = $_SERVER['DOCUMENT_ROOT'] . $base_url . $info_archivo['basename'] . '.conf';
@include ( $archivo_configuracion_app ); // si no existe fallará .. pero será capturado por el @ ... jjy v2

if ( $config['aplicacion']['entorno'] === 'DESARROLLO' ){
  $nuevo = '<small class="color-rojo">nuevo</small> ';

  // SÓLO en entorno desarrollo
  $config ['aplicacion']['enlaces_apoyo'] = array(
    //Solo visibles en entorno de DESARROLLO .... jjy
    array('Iconos FontAwesome 4.0.3',   $base_url . 'libs/font-awesome/4.0.3/lista-iconos_fa4.php'),
    array('Helper Componentes OSTI',    $base_url . 'docs/autodoc.php?hlpr=componentes_html'),
    array('Clases CSS disponibles',     $base_url . 'libs/font-awesome/lista-clases.php'),
    array($nuevo.'Bootstrap 3.x',         'http://getbootstrap.com/css/'),
    array($nuevo.'Demo Componentes OSTI',    $base_url . 'modulo_demo.php'),
    array($nuevo.'Meiomask formatos!',    $base_url . 'libs/jq-formato/doc/index.html'),

    array('Helper Control de Usuarios', $base_url . 'docs/autodoc.php?hlpr=control_usuarios'),
    array('Helper Funciones Comunes',   $base_url . 'docs/autodoc.php?hlpr=funciones_comunes'),
    array('Ayuda CodeIgniter<hr>',      $base_url . 'docs/CodeIgniter/user_guide'),
    array('Iconos FontAwesome',         $base_url . 'libs/font-awesome/lista-iconos.php'),
  );

  $config ['aplicacion']['scripts_apoyo'] = array(
    //Solo visibles en entorno de DESARROLLO .... jjy
    array('Desaparecer iframe (módulo)', "javascript:quitar_frame_modulo();", "_self"),
  );

  $config ['aplicacion']['configuraciones'] = array(
    //Solo visibles en entorno de DESARROLLO .... jjy
    array('<small class="color-amarillo">EN DESARROLLO</small><hr>'),
  );

  $config ['aplicacion']['git'] = array(
    //Solo visibles en entorno de DESARROLLO .... jjy
    array('git status', "javascript:comando_git('consola|git status');","_self"),
    array('git branch', "javascript:comando_git('consola|git branch');","_self"),
    array('git add .', "javascript:comando_git('consola|git add .');","_self"),
  );
}

?>