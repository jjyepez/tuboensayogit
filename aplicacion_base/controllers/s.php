<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S extends CI_Controller {

	var $tabs;
  
  public function __construct() {
		parent::__construct();
      /**
      Definición de Módulos del Sistema ... jjy ***
      **/

      /**  
      	Definición de las pestañas del menú de navegación principal .... jjy ***********
      **/
      $directorio = opendir( realpath( '.' ) ); //ruta actual
      while ( $id = readdir( $directorio ) ) {
          if ( is_dir( $id ) && substr( trim( strtolower( $id ) ) , 0, 7) === 'modulo_' ) {
              if ( ! isset( $modulos [$id] ) ) {
                  $modulos [ $id ] = ucwords( str_replace( '_', ' ', $id ) );
              }
          }
      }
      // se cargan los datos de navegacion ---- pestañas .... v2 jjy
      $base_url                  = $GLOBALS['config']['base_url'];
      $info_archivo              = pathinfo( $_SERVER["SERVER_NAME"] . $base_url );
      $archivo_configuracion_nav = $_SERVER['DOCUMENT_ROOT'] . '/' . $info_archivo['basename'] .'/' . $GLOBALS['application_folder'] . '/config/_ap_nav_auto.php';

      //se cargan los datos específicos de la aplicación ... según el nombre del directorio RAIZ! ... jjy v2
      include ( $archivo_configuracion_nav ); // si no existe fallará .. pero será capturado por el @ ... jjy v2
      if( ! isset( $arr_navegacion ) ){

          $this->reconfigurar_datos_aplicacion('');

      } else {

        $this->tabs = $arr_navegacion; // se anexa arreglo de pestañas ... v2 jjy

        // se incorporan los módulos a la definición de pestañas .. jjy
        foreach( $modulos as $id_modulo => $titulo_pestana ) {
           $this->tabs['pestanas']    += array( $id_modulo => $titulo_pestana );
           if( isset( $this->tabs['url_enlaces'] ) ){
            $this->tabs['url_enlaces'] += array( $id_modulo => site_url().'s/cm/'.$id_modulo );
           }
           if ( ! isset( $menus_izquierda_activos[$id_modulo] ) ) {
              $menus_izquierda_activos[$id_modulo] = array();
           }
        }
        // ****** Fin de la definición de pestañas .... jjy *********************
      }

   }

	public function index(){
    // ********** LO PRIMERO ..... siempre !
    if ( $this->verificacion_general_plantilla('v2') !== "OK" ) {
      //die( 'ERROR General de versi&oacute;n (v2) se terminar&aacute; la ejecuci&oacute;n!' );
      // sigue su camino pero sin pasar por inicio ... esto es porque si se hace die .. la vista nunca se carga ! ... ? jjy v2
    } else {
  		$this->cs("inicio");
    }
    // *********************************** !
	}

	public function cs( $seccion = "inicio", $parametros = array() ){
		global $config;
		$config['sesion']['seccion_activa'] = $seccion;

		$accion=( isset($parametros['accion']) && $parametros['accion']!="" )
                ?'_'.$parametros['accion']
                :"";
    $parametros['seccion']         = $seccion;
    $parametros['vista_principal'] = '/'.$seccion.'/'.$seccion.$accion.'_v';
    $parametros['tabs']            = $this->tabs;

		$this->load->view( 'comun/contenido_general_v', $parametros );
	}

	public function inicio(){
		$this->cs( "inicio" );	
	}

  public function cm ($id_modulo=""){  //cargar módulo ... jjy

    $url_modulo="javascript:alert('E!');"; // alerta de error ... 
    global $config;
    $config['sesion']['seccion_activa']	= $id_modulo;
    $config['sesion']['modulo_activo']	= $id_modulo;
    
    $url_modulo         = base_url()."{$id_modulo}.php";

    $parametros['url_modulo'] = $url_modulo;
    $parametros['seccion']         = $config['sesion']['seccion_activa'];
    $parametros['vista_principal'] = 'comun/cuerpo_v';
    $parametros['tabs']            = $this->tabs;
  
    $this->load->view( 'comun/contenido_general_v', $parametros );
  }

  public function iniciar_sesion(){
    $this->load->model( 'seguridad/control_usuarios_m' );

    extract( $this->input->post() ); // se extraen los valores que vienen por POST

    $resultado = validar_usuario_contrasena( $nombre_usuario, $contrasena );

    $variables = array();

    if( $resultado['rsp'] === 'OK' ){
      $variables ['mensaje']     = 'Has iniciado sesión exitosamente';
      $variables['tipo_mensaje'] = "verde";
      $codigo_usuario            = $resultado ['codigo_usuario'];
      unset ( $resultado );

      $resultado = $this->control_usuarios_m->iniciar_sesion( $codigo_usuario ); // inicia la sesion

      // REVISAR Y DEFINIR BIEN EL USO DE LAS VARIABLES DE SESSION !!!!! .. jjy  .... evitar COOKIES!!
      $_SESSION['sesion']['codigo_usuario'] = $codigo_usuario;
      $_SESSION['sesion']['cuh']            = $resultado['cuh'];

    } else {

      $variables ['mensaje']      = $resultado ['mensaje'];
      $variables ['tipo_mensaje'] = 'error';

    }

    $this->cs ("inicio", $variables ); 
  }

  public function cerrar_sesion( $causa = "" ){
    $codigo_usuario = $_SESSION['sesion']['codigo_usuario'];
    $cuh            = $_SESSION['sesion']['cuh'];

    cerrar_sesion_usuario ( $codigo_usuario, $cuh );
    $variables['mensaje'] = "Has abandonado la sesión.";

    if ( trim( $causa ) == 'exp' ){
      $variables['mensaje'] = "El tiempo de la sesión ha expirado.";
    }

    $this->cs ("inicio", $variables); 
  }

  /**
  
  NO MODIFICAR!!!! ... peligro! .. jjy v2
  Se verifica la plantilla según la versión ... y se hacen los ajustes necesarios de forma automática ! ... jjy

  **/
  public function verificacion_general_plantilla( $version, $seccion='' ){ 

    $base_url = $GLOBALS['config']['base_url'];
    $info_archivo = pathinfo( $_SERVER["SERVER_NAME"] . $base_url );
    $archivo_configuracion_app = $_SERVER['DOCUMENT_ROOT'] . '/' . $info_archivo['basename'] .'/' . $info_archivo['basename'] . '.conf';

    $nombre_corto = $info_archivo['basename'];    

    if( isset( $_POST ) && $this->input->post('nombre_completo') ) {
     
      $nombre_corto                = $info_archivo['basename'];
      $mostrar_encabezado_gobierno = FALSE;
      $mostrar_logo                = FALSE;
      $mostrar_portada             = FALSE;
      $mostrar_titulo              = FALSE;
      $mostrar_fondos_al_azar      = FALSE;
      $entorno                     = 'DESARROLLO';

      extract ( $_POST );

      /**
      REVISAR SI EXISTEN ALTERNATIVAS más SEGURAS !!! para hacer esto!
      **/
      // con las siguientes lineas se pretende corregir el .htaccess ... que apunta a otra aplicacion ... jjy v2
      $dir_base = str_replace( '/', '', str_replace( "http://".$_SERVER["SERVER_NAME"] , "", site_url() ) );
      $datos = <<<FIN
      <IfModule mod_rewrite.c>
          RewriteEngine On
          RewriteBase /
          RewriteCond %{REQUEST_FILENAME} !-f
          RewriteCond %{REQUEST_FILENAME} !-d
          RewriteRule . /$dir_base/index.php [L]
      </IfModule>
FIN;
      $archivo = realpath( '.' ).'/.htaccess' ;
      $f = fopen( $archivo, 'w+' ); 
      if ( ! $f ) {
        die( 'Error al modificar el archivo '.$archivo .'<br>Aseg&uacute;rese de que el archivo existe y que tiene permisos de escritura.<br>Tambi&eacute;n puede editar el contenido manualmente y colocar lo siguiente<br><pre>' . $datos );
      } else {
        $l = fgets($f);
        fclose($f);
      }
      file_put_contents( $archivo, $datos);
      /**
      REVISAR SI EXISTEN ALTERNATIVAS más SEGURAS !!! para hacer esto!
      **/

      $mostrar_encabezado_gobierno = ( $mostrar_encabezado_gobierno == '1' )?'TRUE':'FALSE';
      $mostrar_logo                = ( $mostrar_logo == '1' )?'TRUE':'FALSE';
      $mostrar_titulo              = ( $mostrar_titulo == '1' )?'TRUE':'FALSE';
      $mostrar_portada             = ( $mostrar_portada == '1' )?'TRUE':'FALSE';
      $mostrar_fondos_al_azar      = ( $mostrar_fondos_al_azar == '1' )?'TRUE':'FALSE';

      $hoy = date("d-m-Y H:i.s");
      $datos = <<<FIN
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
      // creado automáticamente $hoy -4.30 ... jjy v2
      \$config['aplicacion']['id']                          = '$id';
      \$config['aplicacion']['nombre_completo']             = '$nombre_completo';
      \$config['aplicacion']['nombre_corto']                = '$nombre_corto';
      \$config['aplicacion']['descripcion']                 = '$descripcion';
      \$config['aplicacion']['version_mayor']               = '$version_mayor';
      \$config['aplicacion']['version_menor']               = '$version_menor';
      \$config['aplicacion']['mostrar_encabezado_gobierno'] = $mostrar_encabezado_gobierno;
      \$config['aplicacion']['mostrar_logo']                = $mostrar_logo;
      \$config['aplicacion']['mostrar_titulo']              = $mostrar_titulo;
      \$config['aplicacion']['mostrar_portada']             = $mostrar_portada;
      \$config['aplicacion']['mostrar_fondos_al_azar']      = $mostrar_fondos_al_azar;
      \$config['aplicacion']['imagen_portada']              = '$imagen_portada';
      \$config['aplicacion']['imagen_fondo']                = '$imagen_fondo';
      \$config['aplicacion']['tema_bs']                     = '$tema_bs';
      \$config['aplicacion']['entorno']                     = '$entorno';
      \$config['aplicacion']['ancho_maximo']                = '$ancho_maximo';
FIN;
      $f = @fopen( $archivo_configuracion_app, 'w+' ); 
      if ( ! $f ) {
        die( 'Error al crear el archivo '.$archivo_configuracion_app .'<br>Aseg&uacute;rese de que el archivo existe y que tiene permisos de escritura.' );
      }
      file_put_contents( $archivo_configuracion_app , $datos);
      header('Location: '.$base_url );
      exit;
    }

    if( isset( $_POST ) && $this->input->post('password') ) {

      $archivo = $GLOBALS['application_folder'].'/config/_ap_database_auto.php';
      extract ( $_POST );

      $hoy = date("d-m-Y H:i.s");
      $datos = <<<FIN
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // creado automáticamente $hoy -4.30 ... jjy v2
    \$db['default']['hostname'] = '$host';
    \$db['default']['database'] = '$dbname';
    \$db['default']['username'] = '$user';
    \$db['default']['password'] = '$password';
FIN;
      file_put_contents( $archivo , $datos);

    }

    $estatus = "!";
    $estatus_cnx = "!";
    $estatus_tpl = "!"; // fijo mientras tanto! ... corregir .... v2 jjy <-- para el chequeo de la navegacion - pestañas y menus
    $estatus_app = "!";
    switch ( $version ){

      case 'v2':

        //intentaremos conectar a la base de datos .... jjy
        include( $GLOBALS['application_folder'].'/config/database.php' ); // se importan los datos de conexion de CodeIgniter
        extract( $db['default'] ); // se extraen los datos de conexion de CodeIgniter

        $cadena_cnx = "host=$hostname dbname=$database user=$username password=$password";
        $cnx = @pg_connect( $cadena_cnx );
        if ( ! $cnx ) {
          $this->reconfigurar_datos_conexion( $db['default'] );
        } else {
          $estatus_cnx = 'OK';
        }
        if ( $estatus_cnx == 'OK' ){ // solo si pasa la primera validacion !....
          //intentamos identificar las variables de configuracion de la aplicacion ... a ver si están por defecto!.... jjy v2
          //se cargan los datos específicos de la aplicación ... según el nombre del directorio RAIZ! ... jjy v2
          @include ( $archivo_configuracion_app ); // se importan los datos de conexion de CodeIgniter

          extract( $GLOBALS['config']['aplicacion'] );
          //echo $id;
          if ( ! isset( $id ) OR $id == 's/i' ){
            
            $GLOBALS['config']['aplicacion']['nombre_corto'] = $info_archivo['basename'];
            $this->reconfigurar_datos_plantilla( $GLOBALS['config']['aplicacion'] );
          } else {
            $estatus_app = 'OK'; // Todo en orden !! .... FALTA verificar datos de la aplicacion! ... desde otro archivo! _ap_config_auto.php ... jjy v2
          }
        }
        //verificamos info de pestanas (nav) .... jjy v2
        include( $GLOBALS['application_folder'].'/config/_ap_nav_auto.php' ); // se importan los datos de conexion de CodeIgniter

        if( ! isset( $arr_navegacion ) && count ( $this->tabs ) == 0  ){
          //prp( $this, 1 );
          $this->reconfigurar_datos_aplicacion('');
        } else {
          $estatus_tpl = "OK";
        }

        /*
        prp("GLOBALS ************************************************* \n");
        prp( $GLOBALS );
        prp("VARIABLES ************************************************* \n");
        prp( get_defined_vars() );
        prp("FUNCIONES ************************************************* \n");
        prp( get_defined_functions() );
        prp("CONSTANTES ************************************************* \n");
        prp( get_defined_constants() );*/
        break;
    }
    // si ambas verificacion arrojan OK ... entonces el estatus general es OK ! ... jjy v2
    if (
         $estatus_cnx == 'OK' 
      && $estatus_tpl == 'OK' // esta puesto fijo mientras tanto! .... corregir! ... v2 jjy
      && $estatus_app == 'OK' ){

      $estatus = 'OK';
    }
    return $estatus;
  }
  
  function reconfigurar_datos_conexion( $variables, $llamada_interna = '' ){
    $llamada_interna = (trim($llamada_interna!=''))?'_' . $llamada_interna: '';
    $this->load->view( 'conf/conf_conexion'.$llamada_interna.'_v', $variables );
  }
  
  function reconfigurar_datos_plantilla( $variables, $llamada_interna = '' ){
    $llamada_interna = (trim($llamada_interna!=''))?'_' . $llamada_interna: '';
    $this->load->view( 'conf/conf_plantilla'. $llamada_interna .'_v' , $variables );
  }

  function reconfigurar_datos_aplicacion( $variables, $llamada_interna = '' ){

    // se carga el archivo de configuracion si existe!!!
    $base_url = $GLOBALS['config']['base_url'];
    $info_archivo = pathinfo( $_SERVER["SERVER_NAME"] . $base_url );
    $archivo_configuracion_nav = $_SERVER['DOCUMENT_ROOT'] . '/' . $info_archivo['basename'] .'/' . $GLOBALS['application_folder'] . '/config/_ap_nav_auto.php';

    if ( $variables == 'g'){

      extract( $_POST );

      $detalle_nav = "";
      foreach( $id_modulo as $i => $id ){

        if ( trim($id) != "" ){
          $detalle_nav .= "\t\t\$arr_navegacion['pestanas']['".$id."'] = '".$titulo_modulo[$i]."';\n";
          $detalle_nav .= "\t\t\$arr_navegacion['url_enlaces']['".$id."'] = '".$enlace[$i]."';\n";
          $detalle_nav .= "\t\t\$arr_navegacion['sub_menus']['".$id."']['tipo'] = '".$menu_izquierda[$i]."';\n";
        }
      }
      $hoy = date("d-m-Y H:i.s");
      $salida =<<<FIN
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // creado automáticamente $hoy -4.30 ... jjy v2
$detalle_nav

FIN;
      file_put_contents( $archivo_configuracion_nav, $salida );

      $this->index();

    } else { // !g ... diferente de guardar .. jjy v2

      /**  
        Definición de las pestañas del menú de navegación principal .... jjy ***********
      **/
      $directorio = opendir( realpath( '.' ) ); //ruta actual
      while ( $id = readdir( $directorio ) ) {
          if ( is_dir( $id ) && substr( trim( strtolower( $id ) ) , 0, 7) === 'modulo_' ) {
              if ( ! isset( $modulos [$id] ) ) {
                  $modulos [ $id ] = ucwords( str_replace( '_', ' ', $id ) );
              }
          }
      }
      $tabs = array(  // los que no son modulos .. sino links internos
        'pestanas' => array(
              ),
        //.. para enlaces específicos ... jjy
        'url_enlaces' => array( // los que no son modulos .. sino links internos
                //'inicio'            => site_url(), //**** se cambió por el nombre corto en el menú (fijo) ...  v2 jjy
              ),
      );
      $menus_izquierda_activos =  array( // se definen solo los modulos de menu-secundario-superior ó sin-menu
      );

      // se incorporan los módulos a la definición de pestañas .. jjy
      foreach( $modulos as $id_modulo => $titulo_pestana ) {

         $tabs['pestanas']    += array( $id_modulo => $titulo_pestana );
         $tabs['url_enlaces'] += array( $id_modulo => site_url().'s/cm/'.$id_modulo );
         if ( ! isset( $menus_izquierda_activos[$id_modulo] ) ) {
            $menus_izquierda_activos[$id_modulo] = array();
         }
      }
      // ****** Fin de la definición de pestañas .... jjy *********************

      //se cargan los datos específicos de la aplicación ... según el nombre del directorio RAIZ! ... jjy v2
      include ( $archivo_configuracion_nav ); // si no existe fallará .. pero será capturado por el @ ... jjy v2

      if( isset( $arr_navegacion ) ){
        
        $menus_izquierda_activos = $arr_navegacion['sub_menus']; // .... modificacion importante v2 jjy

        $variables = array(
          'modulos' => $modulos,
          'tabs' => $tabs,
          'menus_izquierda' => $menus_izquierda_activos,
        );

        $llamada_interna = (trim($llamada_interna!=''))?'_' . $llamada_interna: '';
        $this->load->view( 'conf/conf_aplicacion'. $llamada_interna .'_v' , $variables );

      } else {

        $this->tabs = $tabs;
      
        $detalle_nav = "";
        foreach( $modulos as $id_modulo => $titulo_pestana ) {

          $detalle_nav .= "\t\t\$arr_navegacion['pestanas']['".$id_modulo."'] = '".$titulo_pestana."';\n";

        }
        $detalle_nav .= "\t\t\$arr_navegacion['url_enlaces']=array();\n";
        //$detalle_nav .= "\t\t\$arr_navegacion['sub_menus']['tipo']=array();\n";
        
        $hoy = date("d-m-Y H:i.s");
        $datos = <<<FIN
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // creado automáticamente $hoy -4.30 ... jjy v2
$detalle_nav

FIN;
/**
    EJEMPLO DE DEFINICION DE LOS SUBMENUS! ........ incorporar ..... jjy v2!
**/
        /*    
          $arr_navegacion['sub_menus']['modulo_administracion']['opciones'] = array( 
            array( 'Configuraciones Generales',
                   site_url().'s/cm/modulo_administracion/configuraciones',
                   '_self' ), 
            array( 'Usuarios del sistema',
                   site_url().'modulo_administracion.php/s/listar',
                   'frame_modulo' ),
            array( 'Tablas Secundarias',
                   site_url().'modulo_administracion.php/ts/listar',
                   'frame_modulo' ),
            array( 'Tablas de Apoyo',
                   site_url().'modulo_administracion.php/t/listar',
                   'frame_modulo' ),
          );
        */
        file_put_contents( $archivo_configuracion_nav, $datos );

      }

    }

  } // fin !g

  function mostrar_dialogo_emergente( $tipo_comun, $parametros="" ){
    $archivo_vista = "";
    $archivo_vista = (strpos($tipo_comun,'/')===FALSE)?'comun/'.$tipo_comun.'_v':$tipo_comun;
    
    $variables['datos']=explode(':', $parametros);
    $this->load->view($archivo_vista, $variables);
  }

  function mostrar_dialogo_simple( $parametros ){
    $archivo_vista = "dialogo_simple_v";
    $variables['datos']=explode(':', $parametros);
    $this->load->view($archivo_vista, $variables);
  }

  function ejecutar_comando_git(){
    $archivo_vista = "desarrollo/dialogo_consola_git_v";
    $variables['titulo_dialogo'] = 'Consola GIT';
    $variables['post'] = $this->input->post();

    $this->load->view($archivo_vista, $variables); 
  }

}