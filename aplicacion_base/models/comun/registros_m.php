<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registros_m extends CI_Model {

  function __construct(){
       parent::__construct();
       $this->load->database();
  }
  public function listar_registros( $origen_datos, $parametros = array() ){
      $campos = '*';
      $orden = 'id';

      extract( $parametros );

      $this->db->select( $campos );
      $this->db->from( $origen_datos );
      $this->db->order_by( $orden );

      $datos = $this->db->get();
      $datos = $datos->result_array();

      //prp( $this->db->last_query() );
      return $datos;
  }
  public function eliminar_registro( $id_registro, $parametros ){
    $tabla_asociada      = '';
    $id                  = '';
    $condicion_adicional = '';

    extract( $parametros );    

    $salida = '';

    $sql = "DELETE from $tabla_asociada WHERE id = $id_registro;";
    $rs = $this->db->query( $sql );
    $salida = $rs;

    return $salida;
  }
  public function guardar_registro( $parametros ){
    $tabla_asociada      = '';
    $id                  = '';
    $condicion_adicional = array();

    $salida = array();

    extract( $parametros );

    //prp($tabla_asociada .'<hr>');
    //prp($parametros);

    if( trim( $tabla_asociada ) == '' ){

      $salida['mensaje'] = "No se definió el formulario correctamente (tabla_asociada)!";

    } else {

      foreach ( $parametros as $id_campo => $valor ) {
        // se excluyen los campos de control
        $prefijo = substr( trim( $id_campo ), 0, 7 );
        if( $prefijo == 'campo__' ) {
          // se asignan los valores a los campos que serán actualizados o insertados! ... jjy
          $id_campo_x = substr( $id_campo, 7 );
          $this->db->set( $id_campo_x, $valor );
        }
        if( trim( $id ) == '' && $prefijo == 'autoc__' ) {
          $id_autoc = substr( $id_campo, 7 );
          $this->db->set( $id_autoc, mt_rand(1000, 9999) ); // colocar valor aleatorio!!!! ... jjy
        }
      }
      if( trim( $id ) == '' ){
          $rs = $this->db->insert( $tabla_asociada );
          $id = $this->db->insert_id();

          if ( $id_autoc != '' ) { //Se regresa el autocodigo si se indico el CAMPO!!!!  ... jjy
            $this->db->where('id', $id);
            $auto_codigo = autocodigo( $tabla_asociada, $id );
            $this->db->update( $tabla_asociada, array( $id_autoc => $auto_codigo ) );
            $salida[ $id_autoc ] = $auto_codigo;
          }

        } else {
          $this->db->where('id', $id);
          if( count( $condicion_adicional ) > 0 ) {
            //se incorporan las condiciones adicionales
            $this->db->where( $condicion_adicional );
          }
          $rs = $this->db->update( $tabla_asociada );
      }
      //prp( $this->db->last_query() );
      $salida['id'] = $id;
      $salida['rs'] = $rs;
    }
    //prp( $this->db->last_query() );
    return $salida;
  }
  public function datos_tabla_secundaria( $tabla, $condiciones = "", $orden = "id", $limite = 0 ){
    /*$salida = array(
      array('id'=>0),
      array('a'=>'1111'),
      array('b'=>'2222'),
    );*/
    if( ( is_array($condiciones) && count($condiciones)>0 ) OR trim( $condiciones ) != "" ){
      // se espera el siguiente formato
      //    $condiciones = "name='Joe' AND status='boss' OR status='active'";
      $this->db->where( $condiciones );
    }
    if ( $limite > 0 ) $this->db->limit( $limite );
    if ( $orden != '' ) $this->db->order_by( $orden );

    $rs = $this->db->get($tabla);
    $salida = $rs->result_array();

    //prp( $this->db->last_query());

    return $salida;

  }
  public function extraer_solo_estructura( $tabla ){
    $salida = array();

    $this->db->limit( 1 );
    $rs = $this->db->get( $tabla );

    $salida = $rs->row_array();
    foreach ( $salida as $campo => $valor) {
      $salida[ $campo ] = NULL; // solo nos interesan los campos ... los valores los hacemos null .. jjy
    }

    return $salida;
  }
  public function extraer_registro_segun_id( $tabla, $id_registro ){
    $salida = array();

    $this->db->where( 'id', $id_registro );
    $rs = $this->db->get( $tabla );

    $salida = $rs->row_array();
    return $salida;
  }
  public function extraer_registros_segun_condicion( $tabla, $condiciones = array(), $parametros = array() ){
    $orden = '';
    extract( $parametros );
    $salida = array();
    if ( count( $condiciones ) > 0 ) {
      $this->db->where( $condiciones );
    }
    if( $orden != '' ) $this->db->order_by( $orden );

    $rs = $this->db->get( $tabla );

    $salida = $rs->result_array();

    //prp( $this->db->last_query() );
    return $salida;
  }
  public function codigo_segun_id ( $tabla, $campo_codigo, $id_registro ){
    $reg = $this->extraer_registro_segun_id( $tabla, $id_registro );
    return $reg[$campo_codigo];
  }
  public function proximo_codigo_segun_tabla( $tabla_asociada, $campo_id = 'id', $campo_codigo = '' ){
    $salida = "";
    $id = $this->db->insert_id( $tabla_asociada );
    $proximo_codigo = autocodigo( $tabla_asociada, $id );
    $salida = $proximo_codigo;
    return $salida;
  }
  public function eliminar_registros_segun_condicion( $tabla_asociada, $condiciones ){
    $salida = '';

    $sql = "DELETE from $tabla_asociada WHERE $condiciones ";
    $rs = $this->db->query( $sql );
    $salida = $rs;

    return $salida;
  }

}