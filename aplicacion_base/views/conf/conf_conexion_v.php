<?php if ( ! defined('BASEPATH')) exit('Que intentas hacer???!'); ?>
<?php $this->load->view('comun/encabezado_basico_v') ?>

  </head>

  <body>

    <div class="container">
      
      <div class='ancho-400 centrado'>

        <?=html_br('30px')?>
        <p class="alinear-centro">
          <h2>Configuración de la Conexión a la Base de Datos</h2>
          <br>
          <p>Los datos de conexión suministrados en los archivos de configuración no parecen ser correctos.</p>
        </p>

        <hr>
        <?=html_br('20px')?>

        <?=html_formulario_ini( 'f_conf' )?>

        <?php
          $parametros_comunes = array(
            'clases_adicionales_etiqueta' => 'ancho-200',
          );
        ?>
        <?php
           $parametros = $parametros_comunes + array( 'etiqueta' => 'Nombre del host:', 'valor_inicial' => $hostname ); ?>
        <?=html_input( 'host', 'texto', $parametros )?>

        <br>
        <?php 
          $parametros = $parametros_comunes + array( 'etiqueta' => 'Nombre de la BD:', 'valor_inicial' => $database ); ?>
        <?=html_input( 'dbname', 'texto', $parametros )?>

        <br>
        <?php 
          $parametros = $parametros_comunes + array( 'etiqueta' => 'Usuario:', 'valor_inicial' => $username ); ?>
        <?=html_input( 'user', 'texto', $parametros )?>

        <br>
        <?php 
          $parametros = $parametros_comunes + array( 'etiqueta' => 'Contraseña:', ); ?>
        <?=html_input( 'password', 'password', $parametros )?>
        
        <?=html_br('20px')?>

        <hr>
        <?php 
          $parametros = array( 
            'descripcion'        => 'Aceptar', 
            'icono'              => 'fa-asterisk', 
            'enlace'             => 'javascript:f_conf.submit();', 
            'clases_adicionales' => 'btn-primary centrado',
          ); 
        ?>
        <?=html_bs_boton( 'b_enviar', $parametros )?>

        <?=html_formulario_fin()?>

      </div>  

    </div> <?php // fin div container ... jjy v2 ?>

<?php /*************************************************/

$this->load->view( 'comun/pie_html_v' ); 

/*******************************************************/?>