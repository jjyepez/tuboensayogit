<?php if ( ! defined('BASEPATH')) exit('Que intentas hacer???!'); ?>
<?php $this->load->view('../../aplicacion_base/views/comun/encabezado_basico_v') ?>

  </head>

  <body>

    <div class="container">
      
      <div class='ancho-400 centrado'>

        <?=html_br('30px')?>
        <p class="alinear-centro">
          <h2>Configuración de la Aplicación</h2>
          <br>
          <p>Los datos de la aplicación parecen no haber sido definidos en los archivos de configuración.</p>
        </p>

        <hr>
        <?=html_br('20px')?>

        <?=html_formulario_ini( 'f_conf' )?>

        <?php
          $parametros_comunes = array(
            'clases_adicionales_etiqueta' => 'ancho-200',
          );
        ?>
        <?php
          $opciones_temas_bs = array( 'default'=>'default', 'INN_pv2_2014_1'=>'INN 2014-1', 'flatly'=>'flatly', 'cerulean'=>'cerulean', 'lumen'=>'lumen', 'simplex'=>'simplex', 'spacelab'=>'spacelab', 'united'=>'united' );

          $campos = array(
            'id'              => array( 'ID de la Aplicación', 'texto', $id ),
            'nombre_corto'    => array( 'Nombre Corto', 'texto', $nombre_corto ),
            'nombre_completo' => array( 'Nombre Completo', 'texto', $nombre_completo ),
            'descripcion'     => array( 'Descripción de la Aplicación', 'texto', $descripcion ),
            'version_mayor'   => array( 'Versión Mayor', 'texto', $version_mayor ),
            'version_menor'   => array( 'Versión Menor', 'texto', $version_menor ),
    
            'mostrar_encabezado_gobierno' =>  array( 'Mostrar Encabezado Gob.', 'checkbox', $mostrar_encabezado_gobierno ),
            
            'mostrar_logo'    => array( 'Mostrar Logo', 'checkbox', $mostrar_logo ),
            'mostrar_titulo'  => array( 'Mostrar Título', 'checkbox', $mostrar_titulo ),
            'mostrar_portada' => array( 'Mostrar Portada', 'checkbox', $mostrar_portada ),
            
            'mostrar_fondos_al_azar' => array( 'Mostrar Fondos al Azar', 'checkbox', $mostrar_fondos_al_azar ),
            'imagen_portada'         => array( 'Archivo imagen Portada', 'texto', $imagen_portada ),
            'imagen_fondo'           => array( 'Archivo imagen Fondo', 'texto', $imagen_fondo ),

            'tema_bs'          => array( 'Tema de Boostrap 3.0', 'lista', $tema_bs, $opciones_temas_bs ),
          );
        ?>
        <?php foreach ( $campos as $id_campo => $info_parametros ) { ?>
          <?php 
            $parametros = $parametros_comunes 
                        + array( 
                            'etiqueta'      => $info_parametros[0],
                            'valor_inicial' => $info_parametros[2],
                          ); 
            if ( isset( $info_parametros[3] ) ){
                $parametros += array(
                  'items' => $opciones_temas_bs,
                );
            }
          ?>
          <?=html_input( $id_campo, $info_parametros[1], $parametros )?>
          <br>
        <?php } ?>
        
        <?=html_br('20px')?>

        <?php 
          $parametros = array( 
            'descripcion'        => 'Aceptar', 
            'icono'              => 'fa-asterisk', 
            'enlace'             => 'javascript:f_conf.submit();', 
            'clases_adicionales' => 'btn-primary centrado',
          ); 
        ?>
        <?=html_bs_boton( 'b_enviar', $parametros )?>

        <?=html_formulario_fin()?>

      </div>  

    </div>


<?php /*************************************************/

$this->load->view( 'comun/pie_html_v' ); 

/*******************************************************/?>