<?php if ( ! defined('BASEPATH')) exit('Que intentas hacer???!'); ?>
<?php
  /**
    SE PREPARAN LOS DATOS EN BASE A $GLOBALS!  ... v2 jjy
    ESTA VISTA DEBE PASARSE AL MODULO DE ADMINISTRACION / CONFIGURACIONES !
  */
  //se cargan los datos específicos de la aplicación ... según el nombre del directorio RAIZ! ... jjy v2
  $info_archivo = pathinfo( $GLOBALS['config']['base_url'] );
  $base_url = $_SERVER['DOCUMENT_ROOT'] . '/' . $info_archivo[ 'basename' ];

  // **** ARCHIVO DE CONFIGURACION 
  $archivo_configuracion_app = $base_url . '/' . $info_archivo[ 'basename' ] . '.conf';

  @include ( $archivo_configuracion_app ); // si no existe fallará .. pero será capturado por el @ ... jjy v2

  extract($config['aplicacion']);
  /**
   -------------------- fin
  */

  $html_contenido_general = ""; // apoyo a componentes ... jjy v2
?>
<?php $this->load->view('../../aplicacion_base/views/comun/encabezado_basico_v') ?>

  </head>
  <body>

    <div class="frame-container">
      
      <div class='ancho-full'>

        <?=html_br('15px')?>

        <p class="alinear-centro">
          <h3>Configuración de la Aplicación <span class="negrita"><?=$nombre_completo?></span></h3>
          
          <?=html_br('10px')?>
          
          <p>Los datos de la aplicación ser&aacute;n actualizados en los archivos de configuración.</p>
        </p>

        <hr>

        <?=html_br('10px')?>

        <?php
          $parametros = array(
            'destino' => '_top',
            'metodo' => 'POST',
            'accion' => base_url() . '/s/reconfigurar_datos_aplicacion/g', 
            'clases_adicionales' => 'formulario-tabla',
          );
        ?>
        <?=html_formulario_ini( 'f_conf', $parametros )?>
        
        <?php 

          /*prp( $modulos );
          prp( $tabs );
          prp( $menus_izquierda );*/

          $data = array();
          $c = 0;
          foreach ($tabs['pestanas'] as $id_modulo => $titulo) {
            $data[$c] = array( 
              'id_modulo'      => $id_modulo,
              'titulo_modulo'  => $titulo,
              'menu_izquierda' => ( isset( $menus_izquierda[ $id_modulo ]) && isset ($menus_izquierda[ $id_modulo ][0]) ) ? $menus_izquierda[ $id_modulo ][0] : 'sin-menu',
              'enlace'         => $tabs['url_enlaces'][ $id_modulo ],
              'sub_menus'      => 'sm',
            );
            $c++;
          }

          $anchos_columnas = array(
            'id_modulo' => '150px',
            'titulo_modulo' => '200px',
            'menu_izquierda' => '200px',
            'enlace' => '300px',
            'sub_menus' => '200px',
          );
          $parametros = array(
            'ancho_columnas' => $anchos_columnas,
          );

          $i = $c;
          //while ( $i++ <20 ){ $data[$i] = $data[0]; }

        ?>
        <?php //=html_grid_simple_plus_divs( 'grid_modulos', $data, $parametros ) ?>
        <?php //=html_br('20px')?>


        <?php
          $editable = TRUE;
          $parametros_lista = array(
            'items' => array(
                'sin-menu' => 'Sin menú',
                'menu-secundario-superior' => 'Menú superior',
                'menu-izquierda' => 'Menú Lateral Izq.',
            ),
          );
          $parametros['componentes_columnas'] = array(
            'titulo_modulo' => array( 'texto' ),
            'menu_izquierda' => array( 'lista', $parametros_lista ),
            'enlace' => array( 'texto' ),
            'sub_menus' => array( 'boton', array( 'enlace' => 'javascript:document.parent.mostrar_popup(9999)' ) ),
          );
        ?>
        <?=html_grid_avanzado ( 'grid_modulos_av', $data, $parametros ) ?>


        <hr>
        <?php 
          $parametros = array( 
            'descripcion'        => 'Aceptar', 
            'icono'              => 'fa-check', 
            'enlace'             => 'javascript:f_conf.submit();', 
            'clases_adicionales' => 'btn-default alinear-izquierda',
          ); 
        ?>
        <?=html_bs_boton( 'b_enviar', $parametros )?>

        <?=html_formulario_fin()?>

      </div>  

    </div>

    <?=html_preparar_popup( $html_contenido_general )?>
    <?=$html_contenido_general?>

</body>
</html>