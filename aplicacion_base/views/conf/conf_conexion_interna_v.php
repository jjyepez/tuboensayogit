<?php if ( ! defined('BASEPATH')) exit('Que intentas hacer???!'); ?>
<?php
  /**
    SE PREPARAN LOS DATOS EN BASE A $GLOBALS!  ... v2 jjy
    ESTA VISTA DEBE PASARSE AL MODULO DE ADMINISTRACION / CONFIGURACIONES !
  */
  //se cargan los datos específicos de la aplicación ... según el nombre del directorio RAIZ! ... jjy v2
  $info_archivo = pathinfo( $GLOBALS['config']['base_url'] );
  $base_url = $_SERVER['DOCUMENT_ROOT'] . '/' . $info_archivo[ 'basename' ];
  
  // **** ARCHIVO DE CONFIGURACION 
  $archivo_configuracion_app = $base_url . '/aplicacion_base/config/_ap_database_auto' . '.php';

  @include ( $archivo_configuracion_app ); // si no existe fallará .. pero será capturado por el @ ... jjy v2

  extract($db['default']);
  /**
   -------------------- fin
  */
?>
<?php $this->load->view('comun/encabezado_basico_v') ?>

  </head>

  <body>

    <div class="frame-container">
      
      <div class='ancho-full'>

        <?=html_br('15px')?>

        <p class="alinear-centro">
          <h3>Configuración de la <span class="negrita">Conexión a la Base de Datos</span></h3>
          
          <?=html_br('10px')?>
          
          <p>Los datos de conexión ser&aacute;n actualizados en los archivos de configuraci&oacute;n.</p>
        </p>

        <hr>

        <?=html_br('10px')?>
        
        <?php
          $parametros = array(
            'destino' => '_top',
            'metodo' => 'POST',
            'accion' => '../../s/verificacion_general_plantilla/v2',
          );
        ?>
        <?=html_formulario_ini( 'f_conf', $parametros )?>

        <table><tr>
          <td>

            <?php
              $parametros_comunes = array(
                'clases_adicionales_etiqueta' => 'ancho-200',
              );
            ?>
            <?php
               $parametros = $parametros_comunes + array( 'etiqueta' => 'Nombre del host:', 'valor_inicial' => $hostname ); ?>
            <?=html_input( 'host', 'texto', $parametros )?>

            <br>
            <?php 
              $parametros = $parametros_comunes + array( 'etiqueta' => 'Nombre de la BD:', 'valor_inicial' => $database ); ?>
            <?=html_input( 'dbname', 'texto', $parametros )?>

            <br>
            <?php 
              $parametros = $parametros_comunes + array( 'etiqueta' => 'Usuario:', 'valor_inicial' => $username ); ?>
            <?=html_input( 'user', 'texto', $parametros )?>

            <br>
            <?php 
              $parametros = $parametros_comunes + array( 'etiqueta' => 'Contraseña:', ); ?>
            <?=html_input( 'password', 'password', $parametros )?>
            
            <?=html_br('20px')?>

            <hr>
            <?php 
              $parametros = array( 
                'descripcion'        => 'Aceptar', 
                'icono'              => 'fa-check', 
                'enlace'             => 'javascript:f_conf.submit();', 
                'clases_adicionales' => 'btn-default alinear-izquierda',
              ); 
            ?>
            <?=html_bs_boton( 'b_enviar', $parametros )?>
      
          </td>
        </tr></table>

        <?=html_formulario_fin()?>

      </div>  

    </div> <?php // fin div container ... jjy v2 ?>

<?php /*************************************************/

//$this->load->view( 'comun/pie_html_v' ); 

/*******************************************************/?>

</body>
</html>