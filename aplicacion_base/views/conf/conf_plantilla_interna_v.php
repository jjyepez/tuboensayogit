<?php if ( ! defined('BASEPATH')) exit('Que intentas hacer???!'); ?>
<?php
  /**
    SE PREPARAN LOS DATOS EN BASE A $GLOBALS!  ... v2 jjy
    ESTA VISTA DEBE PASARSE AL MODULO DE ADMINISTRACION / CONFIGURACIONES !
  */
  //se cargan los datos específicos de la aplicación ... según el nombre del directorio RAIZ! ... jjy v2
  $info_archivo = pathinfo( $GLOBALS['config']['base_url'] );
  $base_url = $_SERVER['DOCUMENT_ROOT'] . '/' . $info_archivo[ 'basename' ];
  
  // **** ARCHIVO DE CONFIGURACION 
  $archivo_configuracion_app = $base_url . '/' . $info_archivo[ 'basename' ] . '.conf';

  @include ( $archivo_configuracion_app ); // si no existe fallará .. pero será capturado por el @ ... jjy v2

  extract($config['aplicacion']);
  /**
   -------------------- fin
  */
?>
<?php $this->load->view('../../aplicacion_base/views/comun/encabezado_basico_v') ?>
<?php $html_general_componentes = ""; //almacenara los HTMLS de apoyo que vienen de los componentes, como el calendario, la calculadora, etc. ?>


  </head>

  <body>

    <div class="frame-container">
      
      <div class='ancho-full'>

        <?=html_br('15px')?>

        <p class="alinear-centro">
          <h3>Configuración de la <span class="negrita">Plantilla OSTI v2.x</span></h3>
          
          <?=html_br('10px')?>
          
          <p>Los datos de la aplicación ser&aacute;n actualizados en los archivos de configuración.</p>
        </p>

        <hr>

        <?=html_br('10px')?>

        <?php
          $parametros = array(
            'destino' => '_top',
            'metodo' => 'POST',
            'accion' => '../../s/verificacion_general_plantilla/v2',
          );
        ?>
        <?=html_formulario_ini( 'f_conf', $parametros )?>
        
        <table><tr>
          <td valign='top'>

            <?php
              $parametros_comunes = array(
                'clases_adicionales_etiqueta' => 'ancho-200',
              );
            ?>
            <?php
              $campos = array(
                'id'              => array( 'ID de la Aplicación', 'texto', $id ),
                'nombre_corto'    => array( 'Nombre Corto', 'texto', $nombre_corto ),
                'nombre_completo' => array( 'Nombre Completo', 'texto', $nombre_completo ),
                'descripcion'     => array( 'Descripción de la Aplicación', 'texto', $descripcion ),
                'version_mayor'   => array( 'Versión Mayor', 'texto', $version_mayor ),
                'version_menor'   => array( 'Versión Menor', 'texto', $version_menor ),
                'entorno'         => array( 'Entorno', 'lista', $entorno, array( 'items' => array( 'DESARROLLO' => 'En desarrollo', 'PRODUCCION' => 'En producción', 'PRUEBAS' => 'En pruebas', 'DEMO' => 'Demostración', 'SUSPENDIDO' => 'Suspención Temporal' ) ) ),

                'mensaje'         => array( '', 'oculto','Actualizado'),
              );
            ?>
            <?php foreach ( $campos as $id_campo => $info_parametros ) { ?>
              <?php 
                $parametros = $parametros_comunes 
                            + array( 
                                'etiqueta'      => $info_parametros[0],
                                'valor_inicial' => $info_parametros[2],
                              ); 
                if ( isset( $info_parametros[3] ) ) {
                  $parametros += $info_parametros[3];
                }
              ?>
              <?=html_input( $id_campo, $info_parametros[1], $parametros )?>
              <br>
            <?php } ?>
          </td>    

          <td>
            <?php
              $opciones_temas_bs = array( 'default'=>'default', 'INN_pv2_2014_1'=>'INN 2014-1', 'flatly'=>'flatly', 'cerulean'=>'cerulean', 'lumen'=>'lumen', 'simplex'=>'simplex', 'spacelab'=>'spacelab', 'united'=>'united' );

              $campos = array(
                'mostrar_encabezado_gobierno' =>  array( 'Mostrar Encabezado Gob.', 'checkbox', $mostrar_encabezado_gobierno ),
                
                'mostrar_logo'    => array( 'Mostrar Logo', 'checkbox', $mostrar_logo ),
                'mostrar_titulo'  => array( 'Mostrar Título', 'checkbox', $mostrar_titulo ),
                'mostrar_portada' => array( 'Mostrar Portada', 'checkbox', $mostrar_portada ),
                
                'mostrar_fondos_al_azar' => array( 'Mostrar Fondos al Azar', 'checkbox', $mostrar_fondos_al_azar ),
                'imagen_portada'         => array( 'Archivo imagen Portada', 'texto', $imagen_portada ),
                'imagen_fondo'           => array( 'Archivo imagen Fondo', 'texto', $imagen_fondo ),

                'tema_bs'          => array( 'Tema de Boostrap 3.0', 'lista', $tema_bs, $opciones_temas_bs ),

                'ancho_maximo'     => array( 'Ancho Máximo:', 'texto', $ancho_maximo ),
              );
            ?>
            <?php foreach ( $campos as $id_campo => $info_parametros ) { ?>
              <?php 
                $parametros = $parametros_comunes 
                            + array( 
                                'etiqueta'      => $info_parametros[0],
                                'valor_inicial' => $info_parametros[2],
                              ); 
                if ( isset( $info_parametros[3] ) ){
                  $parametros += array(
                    'items' => $opciones_temas_bs,
                  );
                }
              ?>
              <?=html_input( $id_campo, $info_parametros[1], $parametros, $html_general_componentes )?>
              <br>
            <?php } ?>

          </td>
        </tr></table>

        <hr>
        <?php 
          $parametros = array( 
            'descripcion'        => 'Aceptar', 
            'icono'              => 'fa-check', 
            'enlace'             => 'javascript:f_conf.submit();', 
            'clases_adicionales' => 'btn-default alinear-izquierda',
          ); 
        ?>
        <?=html_bs_boton( 'b_enviar', $parametros )?>

        <?=html_formulario_fin()?>

      </div>  

    </div>


<?=$html_general_componentes?>

<?php /*************************************************/

//$this->load->view( 'comun/pie_html_v' ); 

/*******************************************************/?>

</body>
</html>