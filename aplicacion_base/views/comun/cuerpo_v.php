<?php if ( ! defined('BASEPATH') ) exit('No direct script access allowed'); ?>
<?php
	
  if( ! isset( $GLOBALS['config']['sesion']['seccion_activa'] ) ){
      $GLOBALS['config']['sesion']['seccion_activa'] = "inicio";
	}

  $seccion    = $GLOBALS['config']['sesion']['seccion_activa'];
  $parametros['seccion'] = $seccion;
  
  $mostrar_scrollbar = "no";
    if ( isset( $mostrar_scrollbar ) ) $parametros['mostrar_scrollbar'] = $mostrar_scrollbar;

?>

<?php $this->load->view('modulo/modulo_v',$parametros) ?>
