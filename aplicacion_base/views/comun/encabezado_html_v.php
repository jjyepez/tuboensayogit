<?php if ( ! defined('BASEPATH') ) exit('No direct script access allowed'); ?>
<?php 

  $config = $GLOBALS['config'];

  $codigo_usuario = $_SESSION['sesion']['codigo_usuario']; // datos de la sesion!!!! ... jjy
  $cuh            = $_SESSION['sesion']['cuh'];
  $sesion_iniciada = FALSE; //fijo para poder avanzar ! ... jjy
  if ( sesion_activa ( $codigo_usuario, $cuh ) ){ 
    $sesion_iniciada = TRUE; //fijo para poder avanzar ! ... jjy
  } 

?>
<?php 
	if( ! isset( $config['sesion'] )
      OR ! isset( $config['sesion']['seccion_activa'] ) 
    ){
      $config['sesion']['seccion_activa'] = "inicio";
	}
	$seccion_activa = $config['sesion']['seccion_activa'];
?>

<?php $this->load->view('comun/encabezado_basico_v') // ********************* inclusión mínima requerida ... jjy v2 ************************/ ?>

<script>
  $(document).ready(function(){
    <?php 
      $html_fondos = "";

      $archivo_img_fondo = $config['aplicacion']['imagen_fondo'];
        if( strtolower( substr( trim( $archivo_img_fondo ), 0, 7 ) ) !== 'http://' ){  $archivo_img_fondo = base_url()."imgs/".$archivo_img_fondo; }
      $archivo_img_portada = $config['aplicacion']['imagen_portada'];
        if( strtolower( substr( trim( $archivo_img_portada ), 0, 7 ) ) !== 'http://' ){  $archivo_img_portada = base_url()."imgs/".$archivo_img_portada; }

      if ( $config ['aplicacion']['mostrar_fondos_al_azar'] === TRUE ){

        $archivo_img_azar = base_url() . "imgs/fondo-".round(mt_rand(1,9))."v2"; // ... modificado .. jjy v2

        $html_fondos = "
          $('.fondo-base-aplicacion, .img-portada').css({
            'background':'none',
            'background-image':'url(" . $archivo_img_azar .".jpg)',
            'background-position':'".round(mt_rand(1,1280))."px ".round(mt_rand(1,800))."px',
            'background-size':'1280px 800px'
          });\n";

      if( $config['aplicacion']['entorno'] === "DESARROLLO" ){
          $html_fondos .= "
          $('.fondo-base-aplicacion, .img-portada').attr({
            'title': '" . $archivo_img_azar . "',
          });\n";
        }

      } else {
        // algunas modificaciones ... jjy v2
        $html_fondos = "
          $('.fondo-base-aplicacion').css({
            'background-image':'url(".$archivo_img_fondo.")'
          });
          $('.img-portada').css({
            'background-image':'url(".$archivo_img_portada.")',
            'background-position':'center center',
          });\n";
      }
      echo $html_fondos;
    ?>
  });

  function menuContextual( obj ){
    switch ((obj.nodeName).toLowerCase()){
      case "input":
      case "textarea":
        //REVISAR ... no funciona aun
        return true;
        break;
      default:
        return ( <?=($config['aplicacion']['entorno']=='DESARROLLO')?'true':'false'?> );
    }
  }
</script>
<style>
    .contenedor-principal {top: 80px;} /* para tipo sin nungún menú o con el menu de la izquierda! ... */
</style>
</head>

<body id="body" oncontextmenu="javascript:return menuContextual(this);">

  <?php 
    $html_estilos = " style='top:115px;' ";
    if( $config['aplicacion']['mostrar_encabezado_gobierno'] === FALSE ){
      $html_estilos = " style='top: 45px;' ";
    }
  ?>
  <div class="fondo-base-aplicacion" <?=$html_estilos?> >

    <div class="fondo-sombra-base"></div>

  </div>

  <header>

  <?php
      $tipo_menu_secundario_activo = 'sin-menu';
      if ( isset( $config['sesion'] )
           && isset( $config['sesion']['modulo_activo'] )
           && isset( $this->tabs['sub_menus'][$config['sesion']['modulo_activo']] )
           && isset( $this->tabs['sub_menus'][$config['sesion']['modulo_activo']][0] )
           ){
             
        $menus_izquierda_activos = (array) $this->tabs['sub_menus'];
        $tipo_menu_secundario_activo = $menus_izquierda_activos[$config['sesion']['modulo_activo']][0];

      }
      ?>
  <?php if ( $config['aplicacion']['mostrar_encabezado_gobierno'] === TRUE ) { ?>

	<style>
    .contenedor-principal { top: 125px; }
	</style>

  <div class="container encabezado_gobierno">

		<div class="logo_gobierno seguido"></div>
		<div class="logo_especial seguido"></div>

	</div>	

<?php } // fin mostrar_encabezado_gobierno ... jjy v2?>

  <div class="contenedor-encabezado">

    <!-- Fixed navbar -->
    <nav>
    <div class="navbar navbar-default navbar-static-top" role="navigation">
      
      <div class="container contenedor-navbar">

        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <?php
            $nombre_app =  '<span class="negrita">'.$GLOBALS['config']['aplicacion']['nombre_corto'] . '</span> v'
                          .$GLOBALS['config']['aplicacion']['version_mayor'] . '.'
                          .$GLOBALS['config']['aplicacion']['version_menor'] ;
          ?>
          <a class="navbar-brand letra-condensada" href="<?=base_url()?>"><?=html_sangria('15px')?><i class="fa fa-home"></i><?=html_sangria('5px')?><?=$nombre_app?></a>
        </div>

        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <?php
              $html_cerrar_sesion = "";
              /**
               LA SESION ESTA ACTIVA O NO?
              **/
              if (! sesion_activa( $codigo_usuario, $cuh ) ){
                $pestanas=array(); // 'inicio' => $pestanas['inicio'] );
              } else {

                $info_usuario = informacion_usuario( $codigo_usuario );
                //prp( $info_usuario );

                $html_cerrar_sesion = ''
                              .'<ul class="nav navbar-nav navbar-right">'
                              .'<!-- // revisar ... jjy
                                 li><a href="javascript:toggleFullScreen();"><span class="fa-stack"
                                  ><i class="fa fa-square-o fa-stack-2x"></i
                                  ><i class="fa fa-expand fa-stack-1x"></i></span
                                ></a></li>
                                -->'
                              .'<li><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">'
                              .'<span class="fa fa-stack">'
                              .'<i class="fa fa-square fa-stack-2x color-gris-claro"></i>'
                              .'<i class="fa fa-user fa-stack-1x"></i>'
                              .'</span>'
                              .'<span class="letra-condensada"><span class="ocultar-en-960">&nbsp;<span class="negrita"> '.ucwords($info_usuario['nombres']).'</span> ('.ucwords($info_usuario['nombres']).') &nbsp;</span><b class="caret"></b></span>'
                              .'</a>'
                              .'  <ul class="dropdown-menu">
                                    <li><a href="'.site_url().'/s/cerrar_sesion"><i class="fa fa-lg fa-sign-out"></i> <small>Cerrar sesi&oacute;n</small></a></li>
                                    <li><a href="'.site_url().'/s/cambiar_clave"><i class="fa fa-lg fa-key"></i> <small>Cambiar contrase&ntilde;a</small></a></li>'
                              .'  </ul>'
                              .'</li></ul>';
              }

              // Creacion de los botones del NAVBAR !!! ..... jjy v2!

              //prp( $tabs, 1 );


              $parametros = array(
                'pestanas'          => $tabs['pestanas'],
                'enlaces'           => $tabs['url_enlaces'],
                'id_pestana_activa' => $seccion_activa,
              );
            ?>
            <?=html_bs_menu ('',$parametros)?>

          </ul>

          <?=$html_cerrar_sesion?>

        </div>
      </div>
    </div>

  </nav>
	</div>

  </header>

  <?php /**

    HERRAMIENTAS SOLO PARA EL ENTORNO DE DESARROLLO ...... jjy
  
  ***/ ?>
  <?php if ( $config ['aplicacion']['entorno'] === 'DESARROLLO' ){ 
      $ancho_pestana = 185;
      ?>
      <style>
        .DES_herramientas_desarrolador {
          position: fixed;
          right:-<?=$ancho_pestana?>px;
          top: 118px;
          width: <?=$ancho_pestana+50?>px;
        }
        .DES_herramientas_desarrolador .pestana_herramientas {
          border-radius: 12px 0 0 12px;
          position: absolute;
          top: 5px;
          left: 0;
          width: 50px;
          height: 3.6em;
        }
        .DES_herramientas_desarrolador .pestana_herramientas.scripts { top: 62px; }
        .DES_herramientas_desarrolador .pestana_herramientas.configuraciones { top: 118px; }
        .DES_herramientas_desarrolador .pestana_herramientas.git { top: 176px; }
        .DES_herramientas_desarrolador .contenido_herramientas {
          position: absolute;
          top: 0;
          right:0;
          width: <?=$ancho_pestana?>px;
          height: auto;
          padding: 10px;
          border-radius: 0 0 0 10px;
          min-height: 250px;
        }
        .DES_herramientas_desarrolador ul {
          list-style-type: none;
          padding-left: 0;
          margin: 0;
          list-style-position: inside;
        }
        .DES_herramientas_desarrolador a {
          color: white;
        }
        .DES_herramientas_desarrolador .color-fondo-negro{
          background-color: rgba(0,0,0,0.9);
        }
        .DES_herramientas_desarrolador h3 {
          margin-top: 0px;
          margin-bottom: -5px;
          font-size: 16px;
        }
      </style>

      <script>
        $(document).ready(function(){
          $('.DES_herramientas_desarrolador .enlaces a').attr({target:'_blank', color:'red !important'});

          $('.DES_herramientas_desarrolador').mouseover(function(){

            $('.DES_herramientas_desarrolador').removeClass("encima");

            if( ! $(this).hasClass("DES_mostrado") ) {
              $(this).animate({right: "0px" }, {queue: false}).addClass("DES_mostrado encima encima");
            }
          });
          $('.DES_herramientas_desarrolador').mouseout(function(){
            if( $(this).hasClass("DES_mostrado") ) {
              $(this).animate({right: '-<?=$ancho_pestana?>px'}, {queue: false}).removeClass("DES_mostrado");
            }
          })
        });
        function quitar_frame_modulo(){
          $('#frame_modulo').remove();
        }
      </script>

      <div class="DES_herramientas_desarrolador color-blanco enlaces">
        <div class="pestana_herramientas color-fondo-negro">
          <div class="fa fa-stack fa-2x" style="margin-left:-3px;">
            <i class="fa fa-square-o fa-stack-2x color-amarillo"></i>
            <i class="fa fa-magic fa-stack-1x"></i>
          </div>
        </div>
        <div class="contenido_herramientas color-blanco color-fondo-negro letra-condensada">
          <h3 class="color-blanco">Plantilla - OSTI v2.0b</h3>
          <small class="color-amarillo negrita">Enlaces de apoyo</small>
          <ul><hr>
          <?php
            foreach ( $config['aplicacion']['enlaces_apoyo'] as $info_enlace ) {
              $destino = "";
              $enlace = "javascript:void(0);";
              if ( isset( $info_enlace[1] ) ) {
                $enlace = $info_enlace[1];
                if ( isset( $info_enlace[2] ) ) {
                  $destino = " target='".$info_enlace[2]."' ";
                } else {
                  $destino = " target='_blank' ";
                }
              }
              echo "<li><a ".$destino." href=\"".$enlace."\">".$info_enlace[0]."</a></li>\n";
            }
          ?>
          </ul>
        </div>
      </div>

      <div class="DES_herramientas_desarrolador color-blanco scripts">
        <div class="pestana_herramientas color-fondo-negro scripts">
          <div class="fa fa-stack fa-2x" style="margin-left:-3px;">
            <i class="fa fa-square-o fa-stack-2x color-amarillo"></i>
            <i class="fa fa-puzzle-piece fa-stack-1x"></i>
          </div>
        </div>
        <div class="contenido_herramientas color-blanco color-fondo-negro letra-condensada">
          <h3 class="color-blanco">Plantilla - OSTI v2.0b</h3>
          <small class="color-amarillo negrita">Scripts de apoyo</small>
          <ul><hr>
          <?php
            foreach ( $config['aplicacion']['scripts_apoyo'] as $info_enlace ) {
              $destino = "";
              $enlace = "javascript:void(0);";
              if ( isset( $info_enlace[1] ) ) {
                $enlace = $info_enlace[1];
                if ( isset( $info_enlace[2] ) ) {
                  $destino = " target='".$info_enlace[2]."' ";
                } else {
                  $destino = " target='_blank' ";
                }
              }
              echo "<li><a ".$destino." href=\"".$enlace."\">".$info_enlace[0]."</a></li>\n";
            }
          ?>
          </ul>
        </div>
      </div>

      <div class="DES_herramientas_desarrolador color-blanco scripts">
        <div class="pestana_herramientas color-fondo-negro configuraciones">
          <div class="fa fa-stack fa-2x" style="margin-left:-3px;">
            <i class="fa fa-square-o fa-stack-2x color-amarillo"></i>
            <i class="fa fa-cog fa-stack-1x"></i>
          </div>
        </div>
        <div class="contenido_herramientas color-blanco color-fondo-negro letra-condensada">
          <h3 class="color-blanco">Plantilla - OSTI v2.0b</h3>
          <small class="color-amarillo negrita">Configuraciones</small>
          <ul><hr>
          <?php
            foreach ( $config['aplicacion']['configuraciones'] as $info_enlace ) {
              $destino = "";
              $enlace = "javascript:void(0);";
              if ( isset( $info_enlace[1] ) ) {
                $enlace = $info_enlace[1];
                if ( isset( $info_enlace[2] ) ) {
                  $destino = " target='".$info_enlace[2]."' ";
                } else {
                  $destino = " target='_blank' ";
                }
              }
              echo "<li><a ".$destino." href=\"".$enlace."\">".$info_enlace[0]."</a></li>\n";
            }
          ?>
          </ul>
        </div>
      </div>
      
      <div class="DES_herramientas_desarrolador color-blanco scripts">
        <div class="pestana_herramientas color-fondo-negro git">
          <div class="fa fa-stack fa-2x" style="margin-left:-3px;">
            <i class="fa fa-square-o fa-stack-2x color-amarillo"></i>
            <i class="fa fa-github fa-stack-1x"></i>
          </div>
        </div>
        <div class="contenido_herramientas color-blanco color-fondo-negro letra-condensada">
          <h3 class="color-blanco">Plantilla - OSTI v2.0b</h3>
          <small class="color-amarillo negrita">Comandos GIT</small> <small class="color-rojo">pruebas iniciales</small>
          <ul><hr>
          <?php
            foreach ( $config['aplicacion']['git'] as $info_enlace ) {
              $destino = "";
              $enlace = "javascript:void(0);";
              if ( isset( $info_enlace[1] ) ) {
                $enlace = $info_enlace[1];
                if ( isset( $info_enlace[2] ) ) {
                  $destino = " target='".$info_enlace[2]."' ";
                } else {
                  $destino = " target='_blank' ";
                }
              }
              echo "<li><a ".$destino." href=\"".$enlace."\">".$info_enlace[0]."</a></li>\n";
            }
          ?>
          </ul>
        </div>
      </div>

      <?php 
      unset($config['aplicacion']['enlaces_apoyo']);
      unset($config['aplicacion']['scripts_apoyo']);
      unset($config['aplicacion']['configuraciones']);

      $html_contenido_general = "";
        html_preparar_dialogo_emergente( '', $html_contenido_general );
        echo ( $html_contenido_general );
    } 
  /**
    FIN DE HERRAMIENTAS DE DESARROLLO ........ BUSCAR UN MEJOR LUGAR PARA COLOCAR ESTAS LINEAS ..... jjy
  **/?>

	<div class="container contenedor-principal">