<?php if ( ! defined('BASEPATH') ) exit('No direct script access allowed'); ?>
<?php

$base_url                  = $GLOBALS['config']['base_url'];
$info_archivo              = pathinfo( $_SERVER["SERVER_NAME"] . $base_url );
$archivo_configuracion_nav = $_SERVER['DOCUMENT_ROOT'] . '/' . $info_archivo['basename'] .'/' . $GLOBALS['application_folder'] . '/config/_ap_nav_auto.php';
// -- se cargan los datos específicos de la aplicación ... según el nombre del directorio RAIZ! ... jjy v2
include ( $archivo_configuracion_nav ); // si no existe fallará .. jjy v2

$menus_izquierda_activos = array();
if ( isset( $arr_navegacion['sub_menus'] ) ) {
	$menus_izquierda_activos = $arr_navegacion['sub_menus']; // .... modificacion importante v2 jjy
}

$tipo_menu    = ' sin-menu ';
$mostrar_menu = FALSE;

$seccion      = $GLOBALS['config']['sesion']['seccion_activa'];

if ( array_key_exists( $seccion, $menus_izquierda_activos ) ) {

	if ( isset( $menus_izquierda_activos [$seccion]['tipo'] ) ){

		$tipo_menu   = $menus_izquierda_activos [$seccion]['tipo'];

		if ( $tipo_menu != "sin-menu"
					&& isset( $menus_izquierda_activos[$seccion]['opciones'] ) 
					&& count( $menus_izquierda_activos [$seccion]['opciones'] ) > 0 ){

			$mostrar_menu = TRUE;

			if ( $tipo_menu == 'menu-secundario-superior' ){
				$estilo_menu = ' nav nav-pills ';
			} else {
				$estilo_menu = ' nav nav-pills nav-stacked';
			}

		}
	}
}

?>
<?php 

	if ( $mostrar_menu === TRUE ) { ?>

		<div class = "menu-secundario  <?=$tipo_menu?>">

			<ul class="<?=$estilo_menu?>">

				<?php 

					$vista_menu_izquirda_base =  $GLOBALS['config']['sesion']['modulo_activo'] . '/views/menu_izquierda_v';
					$url_menu_izquirda_base = realpath('.') . '/' . $vista_menu_izquirda_base . '.php';
					
					$html_menu = "";
					$i=0;
					foreach( $arr_navegacion['sub_menus'][$seccion]['opciones'] as $opcion_menu){
						$activo = (($i++)==0)?" class='activ-e activo' ":"";
						$html_menu .= "<li ".$activo.">"
												. "<a href='".$opcion_menu[1]."' ";
						if( isset( $opcion_menu[2] ) ) {
							$html_menu .= " target='". $opcion_menu[2]."' ";
						}
						$html_menu .=">"
												. $opcion_menu[0]
												. "</a>"
												. "</li>";
					} 
				?>

				<?=$html_menu?>

			</ul>

		</div><?php 

	} 

?>