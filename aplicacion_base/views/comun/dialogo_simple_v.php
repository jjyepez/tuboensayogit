  <?php if ( ! isset( $titulo_dialogo ) ) $titulo_dialogo = "_titulo"; ?>
  <h2 class="titulo-seccion normal seguido"><?=$titulo_dialogo?></h2>
  <div class="cerrar-velo-blanco flotado-derecha">
    <a href="javascript:cerrar_velo();"><i class="fa fa-share-square-o fa-flip-horizontal"></i></a>
  </div>
  <hr>

  <?php if ( ! isset( $instrucciones ) ) $instrucciones = "_instrucciones"; ?>
  <?=html_etiqueta( $instrucciones )?>

  <?=html_br('7px')?>

  <?php
    if( ! isset( $contenido_html ) ){
      $contenido_html = "<i class='fa fa-3x fa-cog fa-spin'></i>";
    }
  ?>
  <?=$contenido_html?>
