
  <?php if ( ! isset( $titulo_dialogo ) ) $titulo_dialogo = "_titulo"; ?>
  <h2 class="titulo-seccion normal seguido"><?=$titulo_dialogo?></h2>

  <div class="cerrar-velo-blanco flotado-derecha">
    <a href="javascript:cerrar_velo();"><i class="fa fa-share-square-o fa-flip-horizontal"></i></a>
  </div>

  <hr>

  <?php if ( ! isset( $instrucciones ) ) $instrucciones = "_instrucciones"; ?>
  <?=html_etiqueta( $instrucciones )?>

  <?=html_br('7px')?>
  
  <?php
    if( ! isset( $columnas ) ) { $columnas = array(); }
    if( ! isset( $formato_columnas ) ) { $formato_columnas = array(); }
    if( ! isset( $ancho_columnas ) ) { $ancho_columnas = array(); }
    $parametros = array(
      'columnas'         => $columnas,
      'formato_columnas' => $formato_columnas,
      'ancho_columnas'   => $ancho_columnas,
    );
  ?>

