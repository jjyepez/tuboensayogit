<!DOCTYPE html>
<html lang="es">
  <head>
    <title><?=$GLOBALS['config']['aplicacion']['nombre_corto']?></title>
    <meta charset="UTF8"/>

    <link rel="stylesheet" type="text/css" href="<?=base_url().'css/fuentes_libres.css'?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/font-awesome/4.0.3/css/font-awesome.min.css"> 
    <link rel="stylesheet" href="<?=base_url()?>libs/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>libs/bootstrap/3.1.1/css/sticky-footer.css">

    <link rel="stylesheet" href="<?=base_url()?>libs/bootstrap/3.1.1/themes/<?=$GLOBALS['config']['aplicacion']['tema_bs']?>/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url().'css/estilos.css'?>">
    <link rel="stylesheet" href="<?=base_url()?>css/_ap_css_auto.php">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>aplicacion_base/css/estilos_propios.css">

    <link rel="stylesheet" media="screen" href="<?=base_url()?>libs/jq-scroll/css/jquery.classyscroll.css"></script>
    <link rel="stylesheet" media="screen" href="<?=base_url()?>libs/jq-scroll/css/documentation.css"></script>

    <link rel="shortcut icon" href="<?=base_url()?>imgs/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=base_url()?>imgs/favicon.ico" type="image/x-icon">    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=base_url()?>libs/jquery/1.11.0/jquery.min.js"></script>
