	</div>

   <footer id="footer">
    <?=html_br('50px')?>
	 </footer>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?=base_url()?>libs/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <script src="<?=base_url()?>libs/jq-formato/jquery.meio.mask.js"></script>

  <script src="<?=base_url()?>libs/jq-scroll/js/jquery.classyscroll.js"></script>
  <script src="<?=base_url()?>libs/jq-scroll/js/jquery.mousewheel.js"></script>

  <script src="<?=base_url()?>libs/jq-jsonsuggest/jquery.jsonsuggest-2.min.js"></script>
  
  <script src="<?=base_url()?>js/scripts.js"> //la buena práctica sugiere que los scripts deben ir al final del documento </script>

</body>
</html>
