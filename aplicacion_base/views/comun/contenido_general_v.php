<?php if ( ! defined('BASEPATH') ) exit('No direct script access allowed'); ?>
<?php

	$menus_izquierda_activos = array();
	$tipo_menu_secundario    = 'sin-menu'; // por defecto ... jjy

	if ( isset( $tabs['sub_menus'][ $seccion ] ) ) {
	
		$menus_izquierda_activos = $tabs['sub_menus'][ $seccion ];
		
		if ( isset( $tabs['sub_menus'][ $seccion ]['tipo'] ) ) {
		
			$tipo_menu_secundario = $tabs['sub_menus'][ $seccion ]['tipo'];
		
		}
	}

?>
<?php

	//variables que se pasan a la vista!
	$variables['secciona_activa']      = $seccion;
	$variables['tipo_menu_secundario'] = $tipo_menu_secundario;

	// -- ENCABEZADO
	$this->load->view( 'comun/encabezado_html_v' );

	?><div class="contenido-general"><?php

		$this->load->view( 'comun/menu_izquierda_v', $variables );

		?><div class = "cuerpo cuerpo-<?=$tipo_menu_secundario?>" ><?php // CUERPO

			$this->load->view( $vista_principal );

		?></div>

	</div> <!-- fin contenedor -->

	<?php
	// -- PIE
	$this->load->view( 'comun/pie_html_v' );

