<?php global $config ?>

	</div>

   <footer id="footer">
	
      <div class="barra-estado">
         <div class="container">

            <div class="alinear-centro color-gris">
               <a rel="license" href="http://creativecommons.org/licenses/by-nc/3.0/deed.es"><img class="alinear-abajo" alt="Licencia Creative Commons" style="border-width:0" src='<?=base_url()?>/imgs/cc_logo.png' /></a><?=html_sangria('10px')?><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Plantilla-OSTI (<?=$config['aplicacion']['plantilla_osti']?>)</span>
               <span class="hidden-xs"><span class="hidden-sm"> por <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">sistemas@inn.gob.ve</span></span> se encuentra bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es">Licencia CC BY-NC-SA 3.0</a>.</span>
            </div>

         </div>

      </div>

   </footer>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?=base_url()?>libs/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <script src="<?=base_url()?>js/scripts.js"> //la buena práctica sugiere que los scripts deben ir al final del documento </script>

  <?php if( $config['aplicacion']['entorno'] == 'DESARROLLO'){ ?>

    <script src="<?=base_url()?>js/scripts_desarrollo.js">// funciones solo para DESARROLLO ... jjy v2</script>

  <?php } ?>

</body>
</html>
