      <!-- CALENDARIO SIMPLE .. de apoyo al componente que use la funcion especial de calendario ... !! ... jjy v2 -->
      <!-- ----------------------------------- -->
      <?php
        //preparando todo ! ... jjy v2

        $fecha_inicial = date("Y-m-d");
        $id_input_resultado = "";
        $formato = "d/m/Y";
        if( isset( $datos ) && is_array( $datos ) ){
          if( isset( $datos[2] ) ) $formato = $datos[2];
          if( isset( $datos[1] ) ) $id_input_resultado = $datos[1];
          $datos = $datos[0];
        }

        if( isset( $datos ) && date( "Y-m-d", strtotime( $datos ) ) == $datos ){
          $fecha_inicial = $datos;
        } else {
          $id_input_resultado = $datos;
        }
        $fecha = $fecha_inicial;
        $fecha_base = date_parse_from_format("Y-m-d", $fecha);
          $mes = $fecha_base["month"];
          $ano = $fecha_base["year"];
        $fecha_base_dia_1 = $ano.'-'.$mes.'-01';
        $fecha_proximo_mes = date( "Y-m-d", strtotime( $fecha_base_dia_1 . " next month" ));        
        $fecha_anterior_mes = date( "Y-m-d", strtotime( $fecha_base_dia_1 . " previous month" ));

        $html_scripts = "";
      ?>

      <!-- ----------------------------------- -->
      <div id="calendario_comun" class="calendario-simple" style="display:table">
        <?php
          $parametros = array(
            //'valor_inicial' => date("Y-m-d"),
            'tags_contenedor' => '<div style="display:inline-block; margin:5px;"><div style="border:1px solid #ccc;">|</div></div>',
          );
        ?>
        <?php /*$parametros['fecha']=$fecha_anterior_mes; ?>
        <?=html_simple_calendario2( $parametros ) */?>

        <?php 
          $parametros['fecha']         = $fecha_inicial; 
          $parametros['valor_inicial'] = $fecha_inicial; 
          if ( $formato != "" ) $parametros['formato'] = $formato; 
        ?>
        <?=html_simple_calendario2( $id_input_resultado, $parametros, $html_scripts ) ?>

        <?php /* $parametros['fecha']=$fecha_proximo_mes; ?>
        <?=html_simple_calendario2( $parametros ) */ ?>

      </div>

      <?php

        function html_simple_calendario2( $id_input_resultado='', $parametros=array(), &$html_scripts="" ){
          $fecha           = date("Y-m-d");
          $tags_contenedor = array("<div>","</div>");
          $enlace          = array( 'javascript:seleccionar_fecha("{@fecha_dia}", "'.$id_input_resultado.'");' );
          $destino         = "";
          $formato         = "d-m-Y";
          $valor_inicial   = $fecha;

          extract( $parametros );

          if( isset( $enlace[1] ) ) $destino = $enlace[1];
          if( isset( $enlace[0] ) ) $enlace  = $enlace[0];
          $esta_fecha = $fecha;
          $meses           = array( '','enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre' );
          $dias            = array( 'lunes','martes','miércoles','jueves','viernes','sábado', 'domingo' );
          $dias_abreviados = array( 'l','m','x','j','v','s','d' );
          
          $html_salida = "";
          
          $fecha_base = date_parse_from_format("Y-m-d", $esta_fecha);
            $ano = $fecha_base["year"];
            $mes = $fecha_base["month"];
            $dia = $fecha_base["day"];
            $u_dia = date("t", strtotime($esta_fecha));

          $fecha_base_dia_1 = $fecha; //$ano.'-'.$mes.'-01';
          $fecha_proximo_mes = date( "Y-m-d", strtotime( $fecha_base_dia_1 . " next month" ));        
          $fecha_anterior_mes = date( "Y-m-d", strtotime( $fecha_base_dia_1 . " previous month" ));
          $fecha_proximo_ano = date( "Y-m-d", strtotime( $fecha_base_dia_1 . " next year" ));        
          $fecha_anterior_ano = date( "Y-m-d", strtotime( $fecha_base_dia_1 . " previous year" ));
          
          $arreglo_dias=array();
          $este_mes = $mes;
          $este_ano = $ano;
          $dia_ini = 1; // domingo
          $dia_fin = $u_dia;

          for( $este_dia=$dia_ini;$este_dia<=$dia_fin;$este_dia++){
            $esta_fecha=$este_ano.'-'.$este_mes.'-'.$este_dia;
            $esta_semana = date("W", strtotime($esta_fecha));
            $este_dia_semana = date("w", strtotime($esta_fecha));

            // ajuste para que domingo sea 6 y no 0
            if( $este_dia_semana == 0 ){
              $este_dia_semana=6;
            } else {
              $este_dia_semana--;
            }

            if( $esta_semana == 53 ) $esta_semana = 1;
            $esta_semana = formato_ceros( $esta_semana, 2 );

            if( ! isset( $arreglo_dias[$esta_semana] ) ){
              $arreglo_dias[$esta_semana] = array_fill_keys( $dias_abreviados, 0 );
            }
            $arreglo_dias[$esta_semana][$dias_abreviados[$este_dia_semana]] = $este_dia;
          } 
          
          $tags = ( is_array( $tags_contenedor ) ) ? $tags_contenedor : explode("|", $tags_contenedor );
          
          $html_salida = $tags[0];
          $html_salida .= '<table class="html-lista mes"><tr class="encabezado_mes">
            <th><a href="javascript:mostrar_calendario(\''.$fecha_anterior_ano.'\', \'calendario_comun\',\''.$id_input_resultado.'\')"><i class="fa fa-angle-double-left"></i></a> <a href="javascript:mostrar_calendario(\''.$fecha_anterior_mes.'\', \'calendario_comun\',\''.$id_input_resultado.'\')"><i class="fa fa-angle-left"></i></a></th>
            <th colspan="5"> '.$meses[$este_mes].' '.$este_ano.'</th>
            <th><a href="javascript:mostrar_calendario(\''.$fecha_proximo_mes.'\', \'calendario_comun\',\''.$id_input_resultado.'\')"><i class="fa fa-angle-right"></i></a> <a href="javascript:mostrar_calendario(\''.$fecha_proximo_ano.'\', \'calendario_comun\',\''.$id_input_resultado.'\')"><i class="fa fa-angle-double-right"></i></a></th>
            </tr>';
          $html_salida .= '<tr class="encabezado_dias">';
          foreach ($dias_abreviados as $dia ) {
            $html_salida .= "<th>".$dia."</th>";
          }
          $html_salida .= '</tr>';

          foreach ($arreglo_dias as $esta_semana => $dias_esta_semana ) {
            $html_salida .= "<tr class='fila-registro seleccionable'>";
            
            foreach( $dias_esta_semana as $este_dia_semana => $este_dia_n) {
              if( $este_dia_n == 0 ){ $este_dia_n = ''; }
              
              $fecha_este_dia = date("Y-m-d", strtotime($este_ano.'-'.$este_mes.'-'.$este_dia_n));

              $clase_valor_inicial = "";
              if( $fecha_este_dia == $valor_inicial ){
                $clase_valor_inicial = " class='fecha-activa' ";
              }
              $fecha_con_formato = date( $formato, strtotime( $este_ano.'-'.$este_mes.'-'.$este_dia_n ) );
              $enlace_clic = str_replace( '{@fecha_dia}', $fecha_con_formato, $enlace );
              $html_salida .= "<td ".$clase_valor_inicial."><a href='".$enlace_clic."' target='".$destino."'>".$este_dia_n."</a></td>";
            }

            $html_salida .= "</tr>";
          }

          $html_salida .= '<tr><td colspan="6">'.$valor_inicial.'</td><td><i></i></td></tr>';          

          $html_salida .= '</table>';
          $html_salida .= $tags[1];

          $html_scripts .= "";

          return $html_salida;
        }

      ?>

      <?=$html_scripts?>