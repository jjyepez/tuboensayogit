  <?php if ( ! isset( $titulo_dialogo ) ) $titulo_dialogo = "_titulo"; ?>
  <h2 class="titulo-seccion normal seguido"><?=$titulo_dialogo?></h2>
  <div class="seguido cerrar-velo-blanco">
    <a href="javascript:cerrar_velo();"><i class="fa fa-times"></i></a>
  </div>
  <?=html_hr('br')?>

  <?php
    
    //prp( $post );

    $comando = '';
    $comando_git = '';

    extract( $post );

    $comando_aux = explode('|', $comando );
    $comando = $comando_aux[0];

    switch ( $comando ) {
      case 'consola':
        if( isset( $comando_aux[1] ) ) $comando_git = $comando_aux[1];
        break;
      default:
        $instrucciones = 'No se recibió un comando GIT válido ...';
        break;
    }
    
    $html_salida = '';

    if( trim( $comando_git ) != '' ){

      $instrucciones =  $comando_git;
      $salida_consola = shell_exec( $comando_git );
      $salida_consola = htmlentities( $salida_consola );
      $contenido_html = "
        <div>
        <pre readonly='readonly' class='consola' style='width:100%;height:290px;'
        >".$salida_consola."</pre>
        </div>
      ";
      $html_salida .= html_br('5px');

      $html_salida .= html_formulario_ini( 'f_consola_git', array('accion' => 'javascript:void(0);' ) );
      $parametros = array(
        'valor_inicial' => $instrucciones,
        'clases' => ' consola ',
        'estilos' => ' width: 485px !important;padding: 4px;margin-right: 5px;',
        'parametros_html' => 'autofocus',
      );
      $html_salida .= html_input( 'linea_comando', 'texto', $parametros );
      $parametros = array(
      );
      $parametros = array(
        'icono' => 'fa-cog',
        'texto' => 'ejecutar',
        'tipo' => 'submit',
        'enlace' => 'javascript:ejecutar_comando_actual();',
        'clases_adicionales' => 'btn-default btn-sm seguido',
      );
      $html_salida .= html_bs_boton( 'b_ejecutar', $parametros );
      $html_salida .= html_formulario_fin();

      $html_salida .= html_br('10px');

    } else  {

      $contenido_html = "
        <div style='position:absolute;display:table;width:100%;height:75%;text-align:center;'>
          <div style='display:table-cell;vertical-align:middle;'>
            <i class='fa fa-3x fa-cog fa-spin'></i>
          </div>
        </div>
      ";

    }

  ?>

  <?=$html_salida?>

  <?=$contenido_html?>

  <script type="text/javascript">
    function ejecutar_comando_actual(){
      var comando = $('#linea_comando').val();
      //alert ( "comando_git('consola|"+comando+"');");
      comando_git('consola|'+comando);
    }
  </script>