<?php header('Content-Type: text/css'); ?>
<?php
  // el siguiente codigo fue ofuscado para mejor entendimiento del css ....  jjy v2
  $script = $_SERVER['SCRIPT_FILENAME']; $base_url = $script; $base_folder = str_replace( $_SERVER['DOCUMENT_ROOT'] . '/', '', $base_url ); $base_folder_aux = explode('/', $base_folder ); $nombre_app = $base_folder_aux[0]; $info_archivo = pathinfo( $base_folder ); $base_url = $_SERVER['DOCUMENT_ROOT'] . '/'. $nombre_app; $archivo_configuracion_app = $base_url .'/'. $nombre_app . '.conf';
  define('BASEPATH', $base_url); include ( $archivo_configuracion_app ); $ancho_maximo = ''; extract($config['aplicacion']); $salida = ""; $a_max  ='650'; extract( $_REQUEST ); $ancho_maximo = ( $ancho_maximo == '' ) ? $a_max : $ancho_maximo; $a_max = intval( $ancho_maximo ); $unidad = 'px'; $ancho_maximo_con_menu = $ancho_maximo * 0.92 ; 
  if( trim( (string) intval( trim( $ancho_maximo ) ) ) == trim( $ancho_maximo ) ){ $ancho_maximo .= $unidad; } else { $unidad = trim( str_replace( trim( $a_max ), '', $ancho_maximo ) ); }
  $mitad_ancho_maximo = ($a_max / 2) . $unidad; $ancho_maximo_con_menu = $ancho_maximo_con_menu . $unidad;

?>
<?php

$salida =<<<FIN

  /* contenido css auto generado ... jjy  -- SOLO POR SER DATOS CALCULADOS ... tipo less! ... */

  .container {
    max-width: $ancho_maximo !important;
    width:     $ancho_maximo !important;
    padding:   0;
  }
  .fondo-sombra-base {
    margin-left: -$mitad_ancho_maximo !important;
    width:       $ancho_maximo !important;
  }
  .contenedor-principal {
    margin-left: -$mitad_ancho_maximo !important;
  }
  @media (min-width: 768px) {
    .container > .navbar-header, 
    .container-fluid > .navbar-header, 
    .container > .navbar-collapse, 
    .container-fluid > .navbar-collapse{
      margin: auto 0;
    }
  }
  @media (max-width: 767px) {
    .fondo-sombra-base{
      width: 100% !important;
      margin-left: -50% !important;
    }
    .container{
      width: $ancho_maximo !important;
    }
  }

FIN

?>
<?=$salida?>