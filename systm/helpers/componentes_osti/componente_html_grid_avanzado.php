<?php
  //crea un grid simple basado en html .. jjy !!!!2014!!
  function html_grid_avanzado ( $id, $arreglo_datos, $parametros=array(), &$html_apoyo_salida="" ) {
    //# @parametro array $parametros Un arreglo asociativo parametro => valor, con algunos o varios de los siguientes: >>//
      $columnas           = array();
      $formato_columnas   = array(); //
      $ancho_columnas     = array(); //
      $ancho_grid         = '100%;';
      $alto_grid          = '215px';
      $alto_fila          = '2.5em';
      $mensaje_automatico = TRUE;
      $permitir_nuevo     = TRUE;
      $mostrar_acciones   = FALSE; //// REVISAR BIEN!
      $iconos_accion      = array( 'mostrar', 'eliminar', 'guardar' ); // opcionales ... jjy
      $enlace_mostrar     = 'javascript:alert("mostrar")';
      $enlace_imprimir    = 'javascript:alert("imprimir")';
      $enlace_editar      = 'javascript:alert("editar")';
      $enlace_eliminar    = 'javascript:alert("eliminar")';
      $enlace_guardar     = 'javascript:alert("guardar")';
    //<<
    extract( $parametros );

    //se hace una conversion del arreglo por si viene como obj_result de postgres .. jjy
    if( ! is_array( $arreglo_datos ) ) $arreglo_datos = ( array ) $arreglo_datos;

    $html_salida   = "";
    $html_acciones = "";

    if( count( $columnas ) == 0 ){
      $c = 0;
      foreach ($arreglo_datos[0] as $campo => $valor) {
        $columnas += array( $campo => ucfirst( str_replace('_', ' ', $campo ) ) );
        $c++;
      }
    }
    if( count( $ancho_columnas ) == 0 ){
      $c = 0;
      foreach ($arreglo_datos[0] as $campo => $valor) {
        $ancho_columnas += array( $campo => intval( 100 / count( $arreglo_datos[0] ) ) .'%' );
        $c++;
      }
    }
    if ( count( $columnas ) > 0 ){ 

      $html_salida = "<div 
          class='html_grid_avanzado' 
          style='width: ".$ancho_grid."'
          ><div 
            style = 'display:block;' 
            id    = 'encabezados_$id' 
            name  = 'encabezados_$id' 
            class = 'html-grid html-grid_plus encabezado'
            ><div 
              style = 'display:inline-block; position:relative; width: 100%;'
              >";

        $alineacion_texto = "alinear-izquierda"; //solo la primera columna

        $html_salida .= '<div class="contenedor-celda-grid_plus puntero">'.html_sangria('5px').'</div>'; // puntero! ... v2 jjy

        foreach ( $columnas as $campo => $titulo_columna ){

          $ancho_definido = "";
          if ( isset( $ancho_columnas[ $campo ] ) ) {

            $ancho_definido = "width:". $ancho_columnas[ $campo ];
          }
          
          $html_salida .= "<div 
              style = 'position: relative; display: inline-block; " .$ancho_definido. "' 
              class = 'alinear-centro'
              >"
            . "<div 
                    class = 'contenedor-celda-grid_plus ".$alineacion_texto. "'
                    style = '". $ancho_definido . "; '
                    >"
            . '' . $titulo_columna
            . "</div>"
          . "</div>"; // Div que contiene los ENCABEZADOS fijos!!! .... jjy

          $alineacion_texto = ""; //solo la primera
        }

        $html_salida .= "</div>"
          ."</div>"
          ."<div 
              class = 'contenedor-grid contenedor-grid_plus area-desplazable' 
              style = 'border-top:0; height: ".$alto_grid."'
              >"; //DIV QUE HACE EL SCROLLING DEL CONTENIDO DEL GRID!!!!!!!!!!!!!!! ...jjy
    }

    $html_salida   .= "<div 
        style = 'display:block;' 
        id    = '$id' 
        name  = '$id' 
        class = 'html-grid html-grid_plus'
        >";

    $n_registros =count($arreglo_datos);
    $fila        =0;
    $n_campos    =0;

    if ( count ( $columnas ) == 0 && count( $arreglo_datos ) > 0 ) {

      if ( count ( $arreglo_datos ) == 0 ) {

        $columnas = array('error' => 'No se hasn definido columnas a mostrar y el arreglo de datos está vacío');

      } else {

        foreach ( $arreglo_datos[0] as $campo => $valor ) {

          $columnas[$campo] = $campo;

        }
      }
    }

    //  !!!! Se remplaza en la version plus!!!!!! por el div del contenedor scrolleable ... jjy
    if( count( $columnas ) > 0 ){

      foreach( $columnas as $campo => $titulo ){
        $mostrar_col[$campo]=$titulo;

        $n_campos++;
        if(isset($mostrar_col[$campo])) {
          $titulo=$mostrar_col[$campo];
        }
      }
    }
    
    $html="";
    $mensaje="";
    $tipo_mensaje="";

    if($n_registros>0){
      foreach($arreglo_datos as $reg){
        $fila++;

        $par_o_impar=(($fila%2)==0)?'par':'impar';
        $registro_editable = '';

        $html_campos_hidden="";
        $html_salida.= "<div 
            style = 'display:block; position:relative; height: $alto_fila' 
            class = 'fila-registro seleccionable {$registro_editable}'
            >";

        //MAGIA para reordenar los campos de acuerdo a las columnas suministradas!!!!
        // elegancia ... copiada de la web ! ... jjy
        $reg = array_replace( array_flip( array_keys( $mostrar_col ) ), $reg );       

        $html_salida .= "<div class='contenedor-celda-grid_plus puntero'>".html_sangria('5px')."</div>";

        foreach($reg as $campo=>$valor){

          $ancho_definido = "";
          if ( isset( $ancho_columnas[ $campo ] ) ) {

            $ancho_definido = "width:". $ancho_columnas[ $campo ];

          }

          if( isset( $mostrar_col[$campo] ) ) {

            if ( isset ( $campos[$campo] ) ) { //si viene con formato

              $valor = str_replace( '{@valor}', $valor, $campos[$campo] );

            }
            if ( isset ( $formato_columnas[$campo] ) ) { //si viene con formato

              $valor = $formato_columnas[$campo];

              foreach($reg as $campo_x => $valor_x){

                $valor = str_replace( '{@'.$campo_x.'}', $valor_x, $valor);

              }
            }

            if( isset( $componentes_columnas[$campo] ) ){ 
            // -- es componente ... jjy v2
              $html_salida.= "<div class='{$par_o_impar} seguido contenedor-componentes-celda-grid_plus'
                                   style = '".$ancho_definido."'>";
              $html_salida.= "<div class='contenedor-celda-grid_plus' 
                                   style = '".$ancho_definido."'>";

              $tipo_componente = $componentes_columnas[$campo][0];
              $parametros = array();
              if ( isset( $componentes_columnas[$campo][1] ) ) {
                $parametros      = $componentes_columnas[$campo][1];
              }
              // Tremendo FUME! deox! ... v2 jjy
              $parametros += array(
                'estilos_adicionales_contenedor_input' => 'position:relative;width:100%;height:100%;',
                'clases_adicionales'                   => $par_o_impar . ' seleccionable ',
                'valor_inicial'                        => $valor,
              );

              $html_salida .= html_input($campo.'_'.$fila, $tipo_componente, $parametros, $html_apoyo_salida );
              /**
               AQUI SE MUESTRA EL CONTENIDO (componente) DE CADA CELDA!!!!! ...jjy
               **/
              $html_salida.= "</div></div>";

            } else { 
            // -- no es componente

              $html_salida.= "<div class='{$par_o_impar} seguido contenedor-componentes-celda-grid_plus'
                style = '".$ancho_definido."'
                ><div 
                    class = 'contenedor-celda-grid_plus'
                    >";
              /**
               AQUI SE MUESTRA EL CONTENIDO DE CADA CELDA!!!!! ...jjy
               **/

              if( strpos( $valor, '<' ) === FALSE 
                  && strpos( $valor, '>' ) === FALSE ){
                $html_salida .= html_input($campo . '_'.$fila, 'oculto', array( 'valor_inicial' => $valor ) );
                $valor = "<p style='padding-left:5px;' title='".$valor."'>".$valor."</p>";
              }
              $html_salida.= $valor; 
              $html_salida.= "</div></div>";

            } // -- fin componentes_columnas


          } else {

            if( strpos( $valor, '<' ) === FALSE 
                && strpos( $valor, '>' ) === FALSE ){
              $html_campos_hidden.= "<input id='{$campo}_".$fila."' name='{$campo}_".$fila."' type='hidden' class='ancho-full' value='{$valor}' rel='$fila' />\n";
            }
          }
          
        }

        // iconos de accion! .... v2 jjy
        $html_acciones = "";
        if ( $mostrar_acciones === TRUE ) {

          $enlace_accion['mostrar']  = $enlace_mostrar; // se reinician al valor original en cada vuelta de registro ... jjy
          $enlace_accion['eliminar'] = $enlace_eliminar;
          $enlace_accion['imprimir'] = $enlace_imprimir;
          $enlace_accion['editar']   = $enlace_editar;
          $enlace_accion['guardar']  = $enlace_guardar;

          foreach( $reg as $campo_aux => $valor_aux ){
            $enlace_accion['mostrar']  = str_replace('{@'.$campo_aux.'}', $valor_aux, $enlace_accion['mostrar']);
            $enlace_accion['eliminar'] = str_replace('{@'.$campo_aux.'}', $valor_aux, $enlace_accion['eliminar']);
            $enlace_accion['imprimir'] = str_replace('{@'.$campo_aux.'}', $valor_aux, $enlace_accion['imprimir']);
            $enlace_accion['editar']   = str_replace('{@'.$campo_aux.'}', $valor_aux, $enlace_accion['editar']);
            $enlace_accion['guardar']  = str_replace('{@'.$campo_aux.'}', $valor_aux, $enlace_accion['guardar']);
          }

          $html_acciones = "<table>
            <tr><td class='contenedor-iconos-accion ancho-60'>
            <span class='contenedor-input'>";
          $p = 0;
          if ( array_search( 'imprimir', $iconos_accion ) != "" ) { $html_acciones .= "<a title='Imprimir este registro' id='icono_imprimir'  name='icono_imprimir' href='".$enlace_accion['imprimir']."'><i style='left: {$p}px;'  class='icono-accion-izquierda fa fa-print'></i></a>"; $p+=20;}
          if ( array_search( 'editar', $iconos_accion )   != "" ) {   $html_acciones .= "<a title='Editar' id='icono_editar'    name='icono_editar'   href='".$enlace_accion['editar']."'><i style='left: {$p}px;' class='icono-accion-izquierda fa fa-pencil'></i></a>"; $p+=20;}
          if ( array_search( 'eliminar', $iconos_accion ) != "" ) { $html_acciones .= "<a title='Eliminar' id='icono_eliminar'  name='icono_eliminar' href='".$enlace_accion['eliminar']."'><i  style='left: {$p}px;' class='icono-accion-izquierda fa fa-trash-o'></i></a>"; $p+=20;}
          if ( array_search( 'guardar', $iconos_accion )  != "" ) { $html_acciones .= "<a title='guardar' id='icono_guardar'  name='icono_guardar' href='".$enlace_accion['guardar']."'><i  style='left: {$p}px;' class='icono-accion-izquierda fa fa-save'></i></a>"; $p+=20;}
          
          $html_acciones .= "</span></td></tr></table>";

          $html_salida.= '<div 
              style = "display:inline-block"
              class = "alinear-centro" 
              style = "padding:0px !important;"
              >'
              . $html_acciones 
              .
          '</div>'; //REVISAR!!!

        } // iconos accion ---- v2 jjy

        $html_campos_hidden.="<input class='registro_editado' id='registro_editado[]' name='registro_editado[]' type='hidden' rel='$fila'/>";

        $html_salida.= '<div 
            class = "invisible"
            >'
            . $html_campos_hidden
            . '</div>';

        $html_salida.= "
        </div>\n";
      }

    } else {

      if ( $mensaje_automatico ){ // se recibe por parámetro ... jjy

        $mensaje="La tabla está vacia actualmente.";
        $tipo_mensaje="advertencia";

      }
    }
    
    $reg=array();

    if(isset($campos)){

      foreach($campos as $campo=>$formato){

        if( !is_integer($campo) ) {

          $reg[$campo] = "";

        } else {

          $reg[$formato] = "";

        }
      }
    }

    //se muestran una fila vacía al final para un nuevo registro .. jjy
    $html="";

    $html_salida.= "</div>&nbsp;<br>\n";
    $html_salida.= "</div>"; // en prueba ....
    
    if ( $permitir_nuevo ){
      $html_salida .= "
      <div>
        <div 
            style = 'display:block;' 
            id    = 'pie_$id' 
            name  = 'pie_$id' 
            class = 'html-grid html-grid_plus pie'
            ><div 
              style = 'display:inline-block; position:relative; width: 100%;'
              >Estatus:
        </div></div></div>";
    }
    $html_salida.= "</div>\n"; // en prueba ....

    $html_salida.= $html;
    
    $html_salida.=html_mensaje('',$mensaje,$tipo_mensaje);

    if( strpos( $html_apoyo_salida, "$('.fila-registro').on('mouseover'" ) === FALSE ){ // se verifica que no exista el bloque
      $html_apoyo_salida .= "
      <script>
        $(document).ready(function(){
          $('.fila-registro').on('mouseover', function(){
            $(this).children().find('.solo-visible-hover').removeClass('invisible');
          })
          .on('mouseout', function(){
            $(this).children().find('.solo-visible-hover').addClass('invisible');
          });
        });
      </script>
      ";
    }
    return $html_salida;
  }
