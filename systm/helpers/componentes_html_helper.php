<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//# @helper Componentes-OSTI 2.1.1b - Feb 2014
/**
 * Componentes-OSTI 2.1.1b - Feb 2014
 * Helper con los componentes html para la plantilla-osti 2.0b
 * 
 * @author jjyepez <jyepez@inn.gob.ve> + Equipo OSTI-INN desarrollo de sistemas!
 *
 * @package componentes-osti
 * @version 2.1.1b
 * @license http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es
 * 
 */

// como parte del uso de los componentes OSTI, se fija la zona horaria por defecto en Caracas! ... debe reubicarse! ... v2 jjy
date_default_timezone_set("America/Caracas");

if( ! function_exists('html_input')) {

	function html_input ( $id, $tipo_elemento = "texto", $parametros = array(), &$html_apoyo_salida = "" ) {
		//# @descripcion Componente que genera una salida HTML normalizada de diversos elementos tipo <INPUT>
		//# @parametro string $id El ID que se le asignará al elemento <INPUT> resultante
		//# @parametro string $tipo_elemento El tipo de elemento que generará el componente. Puede ser uno de los siguientes: >>//
			#   texto ... text
			#
			#   texto_funcion_especial ... texto_funciones_especiales ... combo
			#
			#   contador
			#
			#   contraseña ... clave ... password
			#
			#   fecha ... date
			#
			#   calendario
			#
			#   lista ... select
			#
			#   opcionmultiple
			#
			#   archivo ... file ... subirarchivo ... subir_archivo
			#
			#   imagen ... image ... img
			#
			#   texto_largo ... memo ... textolargo ... textarea
			#
			#   texto_enriquecido ... editor_html ... texto_wysiwyg ... wysiwyg
			#
			#   buleano ... boolean ... checkbox
			#
			#   oculto ... escondido ... hidden
			#
			#   boton
			#
			#<br>
		//<<<
		//# @nota El valor debe ser mayor que 0
		//# @parametro array $parametros Los diferentes parámetros que se esperan y sus valores por omisión >>//
			$solo_lectura           = FALSE;
			$editable               = TRUE;
			$etiqueta               = "";
			$etiqueta_con_for       = TRUE;
			$valor_inicial          = "";
			$clases                 = "componente";
			$estilos                = "";
			$estilos_solo_lectura   = "";
			$parametros_html        = "";
			$parametros_adicionales = array(); // variables modificadoras del componente (para ciertos componentes) .. jjy v2
			$mascara 								= '';
			$clases_adicionales     = "ancho-150";
			$clases_adicionales_contenedor_input  = "";
			$estilos_adicionales_contenedor_input = "";
			$texto_inicial          = "";
			$valor_inicial          = "";
			$info_ayuda             = "";
			$enlace_ENTER           = '';
			$items                  =  array( "0" => "seleccione" ); // para compontes tipo lista o de opciones multiples ... jjy
		//<<<
		//se extraen las variables pasadas por parámetros .. ns
			extract($parametros);
		//se inicializan las variables .. ns
			$html_salida	      = "";
		//-----------------------------------------

		/* $html_apoyo_salida    = ""; // variable para retornar bloques de html de apoyo a los componentes de vuelta hacia la vista! ... jjy v2 */

		if ( $enlace_ENTER != "" ){
			$parametros_html .= ' onenter = "'.$enlace_ENTER.'" ';
		}
		if ( $solo_lectura ){
			$parametros_html    .= ' readonly = "readonly" ';
		}

		if (  $editable === FALSE 
			    && ! in_array( $tipo_elemento, 
							array( 
							 	'opcionmultiple', 
							 	'opciones_multiples', 
							 	'seleccionmultiple', 
							 	'checkbox', 
							 	'buleano', 
							 	'boolean' 
							) 
						) 
		) { 
			// si no es editabe y no es un checkbox! .... o sus derivados ! ... OJO verificar con radiobuttons! ... jjy
			$clases        = "solo_lectura";
			if(trim($estilos_solo_lectura)!=''){ $estilos = $estilos_solo_lectura; }
			$valor_inicial = ( trim ($valor_inicial) == "" )? "&nbsp;" : $valor_inicial;
			$html_salida   = "<div rel='$id' class='$clases $clases_adicionales' style='$estilos' $parametros_html >$valor_inicial</div><input id='$id' name='$id' type='hidden' value='$valor_inicial' $parametros_html >";

		} else {

			switch ($tipo_elemento) {
				case "boton":
					$parametros = array(
						'icono' => ( isset( $icono ) ) ? $icono : 'fa-location-arrow',
						'clases_adicionales' => 'btn-sm ancho-full',
						'enlace' => $enlace,
					);
					$html_salida = html_bs_boton( $id, $parametros );
					break;
				case "texto":
				case "text":
					$html_salida = "<div class='contenedor-input $clases_adicionales_contenedor_input' style='$estilos_adicionales_contenedor_input'>";
					$html_salida .= "<input alt='$mascara' oncontextmenu='javascript:return menuContextual(this);' id='$id' name='$id' type='text' class='$clases $clases_adicionales' style='$estilos' value='$valor_inicial' $parametros_html >";
					$html_salida .= "</div>";
					break;
				case "autocompletar":
					$html_salida = html_input( $id, 'texto', $parametros, $html_apoyo_salida ); // temporalmente! REVISAR ... jjy v2
					/*$url_json = "";
					$enlace = "";
					extract($parametros);
					//$html_salida = "<div class='contenedor-input $clases_adicionales_contenedor_input' style='$estilos_adicionales_contenedor_input'>";
					$html_salida .= "<input oncontextmenu='javascript:return menuContextual(this);' id='$id' name='$id' type='text' class='$clases $clases_adicionales' style='$estilos' value='$valor_inicial' $parametros_html >";
					//$html_salida .= "</div>";
					if( strpos( $html_apoyo_salida, 'function callback(item) {' ) === FALSE ) {
						$html_apoyo_salida .= "
						<script>
							function callback(item) {
								alert('You selected \'' + item.text + '\'');
							}
							$(document).ready(function() {
								$('input#".$id."').jsonSuggest({url:'".$url_json."', minCharacters: 2});
							});
						</script>
						";
					}*/
					break;
				case "texto_funcion_especial":
				case "texto_funciones_especiales":
				case "combo":
					$clases_adicionales_icono             = "";
					$n_fd = 0;
					$n_fi = 0;
					$html_funciones = "";
					foreach ( $funciones_especiales as $detalle_funcion ){
						$clase_icono_accion_base = "icono-accion-derecha";
						$id_funcion    = $detalle_funcion [0];
						$icono_funcion = $detalle_funcion [1];

						//***** compatibilidad con bs_3 ... jjy v2
						$icono_funcion = str_replace(' icon-', ' fa fa-', ' ' . $icono_funcion . ' ');

						if ( $clase_icono_accion_base == "icono-accion-izquierda" ) {
							$margen_izquierdo = 6 + ( $n_fi * 20 ) . 'px';
							$n_fi++;
						} else {
							$margen_derecho = 6 + ( ( $n_fd + 1 ) * 20 ) . 'px';
							$n_fd++;
						}
						//detalle_funcion[2] --- se reciben parametros adicionales!
						$enlace             ="";
						$clase_icono_accion = "";
						if ( isset( $detalle_funcion [2] ) ) { 
							extract ( $detalle_funcion [2] ); 
						}
						$clase_icono_accion .= ' '.$clase_icono_accion_base;
						if ( trim( $enlace ) != "" ) { $html_funciones .= "<a id = '".$id_funcion."' name = '".$id_funcion."' href='$enlace' >"; }
						$html_funciones .= "<i class='".$icono_funcion." $clase_icono_accion $clases_adicionales_icono' ";
						if ( $clase_icono_accion_base == "icono-accion-izquierda" &&  $n_fi > 0) {
							$html_funciones .= " style='margin-right: ".$margen_izquierdo."' ";
						} else if ( $clase_icono_accion_base == "icono-accion-derecha" &&  $n_fd > 0) {
							$html_funciones .= " style='margin-left: -".$margen_derecho."' ";
						}
						$html_funciones .= "></i>";

						if ( trim( $enlace ) != "" ) { $html_funciones .= "</a>"; }
						$html_funciones .= "\n";
					}
					$padding_derecho   = 4 + ( $n_fd * 20 ) . 'px';
					$padding_izquierdo = 4 + ( $n_fi * 20 ) . 'px';
					$estilos     .= " padding-right: " . $padding_derecho ."; ";
					$estilos     .= " padding-left: " . $padding_izquierdo . "; ";
					$html_salida  = "<div class='contenedor-input $clases_adicionales_contenedor_input' style='$estilos_adicionales_contenedor_input'>"
											 . "<input id='$id' name='$id' type='text' class='$clases $clases_adicionales' style='$estilos' value='$valor_inicial' $parametros_html >";
					$html_salida .= $html_funciones;
					$html_salida .= "</div>";
					break;
				case "contador":
					$formato = "";
					$factor = 1;
					$valor_minimo = 'null';
					$valor_maximo = 'null';
					$estilos_adicionales = "";
					extract( $parametros_adicionales );

					if(! isset($enlace_incrementar) )	$enlace_incrementar = "javascript:void(0)";
					if(! isset($enlace_disminuir) ) $enlace_disminuir     = "javascript:void(0)";
					$estilos .= "padding-right: 18px;";
					$html_funciones = "<div class='icono-accion-derecha contenedor-micro-iconos'>"
														."<a class='micro-boton-arriba' href='$enlace_incrementar' 	onmousedown='javascript:incrementar_contador( $(this), \"$id\", ".$valor_maximo.", \"".$formato."\", ".$factor." )'><i class='fa fa-chevron-up'></i></a><br>"
														."<a class='micro-boton-abajo' 	href='$enlace_disminuir' 		onmousedown='javascript:disminuir_contador( $(this), \"$id\", ".$valor_minimo.", \"".$formato."\", ".$factor." )'><i class='fa fa-chevron-down'></i></a></div>";
					$html_salida = "<div class='contenedor-input' style='".$estilos_adicionales."'>"
								 . "<input id='$id' name='$id' type='text' class='$clases $clases_adicionales' style='$estilos' value='$valor_inicial' $parametros_html >";
					$html_salida .= $html_funciones;
					$html_salida .= "</div>";

					if( strpos( $html_apoyo_salida, "function incrementar_contador" )  === false ) { // evita duplicar el script si ya existe ... jjy v2
						$html_apoyo_salida .= "\n".'<script type="text/javascript" language="javascript"><!--'."\n".'
							function incrementar_contador( $obj_contador, id, valor_maximo, formato, factor ){
								formato = formato || ""; factor = factor || 1;
							  $input = $obj_contador.parent().siblings(\'input[type="text"]\');
							  var valor = Number( $input.val() );
							  if ( ( valor_maximo != 0 && ! valor_maximo ) || valor < valor_maximo ){
							    valor = valor + factor;
							    if ( formato != "" ){ valor = (formato + "" + valor).slice( -(formato.length) ); }
							    $input.val( valor );
							}}
							function disminuir_contador( $obj_contador, id, valor_minimo, formato, factor ){
								formato = formato || ""; factor = factor || 1;
							  $input = $obj_contador.parent().siblings(\'input[type="text"]\');
							  var valor = Number( $input.val() );
							  if ( ( valor_minimo != 0 && ! valor_minimo ) || valor > valor_minimo ){
							    valor = valor - factor;
							    if ( formato != "" ){ valor = (formato + "" + valor).slice( -(formato.length) ); }
							    $input.val( valor );
							}}'."\n--></script>\n";
					}
					break;
				case "contraseña":
				case "clave":
				case "password":
					$html_salida = "<input id='$id' name='$id' type='password' class='$clases $clases_adicionales' style='$estilos' value='$valor_inicial' $parametros_html >";
					break;
				case 'calculadora':
					$estilos_adicionales_contenedor_input = '';
					$clases_adicionales_contenedor_input = '';
					$clases_adicionales_icono = '';
					extract($parametros);
				  $parametros = array(
						'clases'                               => $clases,
						'clases_adicionales'                   => $clases_adicionales,
						'estilos_adicionales_contenedor_input' => $estilos_adicionales_contenedor_input,
						'clases_adicionales_contenedor_input'  => $clases_adicionales_contenedor_input,
						'clases_adicionales_icono'  					 => $clases_adicionales_icono,
						'estilos'                              => $estilos,
						'editable'                             => $editable,
						'parametros_html'                      => "placeholder = '0.00' $parametros_html ",
						'funciones_especiales' => array(
								array( 'accion_' . $id, 'fa fa-th', array( 'enlace' => 'javascript:calculadora("'.$id.'");', 'clases_adicionales_icono' => $clases_adicionales_icono ) ),
							),
						'valor_inicial' 											 => $valor_inicial,
						);
					$html_salida = html_input( $id, 'texto_funcion_especial', $parametros );
					if( strpos( $html_apoyo_salida, '.calculadora-simple {') === FALSE ){
						$html_apoyo_salida .= html_calculadora_simple( FALSE );
					}
					break;
				case "fecha":
				case "date":
					$parametros['etiqueta'] = '';
						$parametros['parametros_html'] = "placeholder='Día' maxlength='2'";
						$parametros['estilos'] = 'width:50px !important;';
						$parametros['parametros_adicionales'] = array( 'valor_minimo' => 1, 'valor_maximo' => 31 );
					$html_salida = html_input($id.'_dia','contador',$parametros) . html_sangria('7px');
						$meses=array("01"=>"Enero","02"=>"Febrero","03"=>"Marzo","04"=>"Abril","05"=>"Mayo","06"=>"Junio","07"=>"Julio","08"=>"Agosto","09"=>"Septiembre","10"=>"Octubre","11"=>"Noviembre","12"=>"Diciembre",);
						$parametros['items']=$meses;
						$parametros['texto_inicial'] = "Mes ...";
						$parametros['estilos'] = 'width:100px !important;';
						$parametros['parametros_html'] = "placeholder='Mes'";
					$html_salida.= html_input($id.'_mes','lista',$parametros) . html_sangria('7px');
						$parametros['texto_inicial'] = "";
						$parametros['estilos'] = 'width:50px !important;';
						$parametros['parametros_html'] = "placeholder='Año' maxlength='4'";
					$html_salida.=html_input($id.'_ano','texto',$parametros);
					break;
				case "hora": // .. v2 jjy
				case "time":
					$parametros['etiqueta'] = '';
						$parametros['parametros_html'] = "placeholder='hh' maxlength='2'";
						$parametros['estilos'] = 'width:50px !important;';
						$parametros['parametros_adicionales'] = array('valor_minimo' => '1', 'valor_maximo' => '12', 'formato' => '00' ); // atencion con este formato! se deben usar formatos validos para sprintf!! ... jjy v2
					$html_salida =html_input($id.'_hora','contador',$parametros, $html_apoyo_salida) . html_sangria('7px');
						$parametros['texto_inicial'] = "";
						$parametros['estilos'] = 'width:50px !important;';
						$parametros['parametros_html'] = "placeholder='mm' maxlength='4'";
						$parametros['parametros_adicionales'] = array('valor_minimo' => '0', 'valor_maximo' => '59', 'formato' => '00' ); // atencion con este formato! se deben usar formatos validos para sprintf!! ... jjy v2
					$html_salida.=html_input($id.'_minutos','contador',$parametros, $html_apoyo_salida) . html_sangria('7px');
						$ampm=array("AM"=>"AM","PM"=>"PM",);
						$parametros['items']=$ampm;
						$parametros['valor_inicial'] = "PM";
						$parametros['estilos'] = 'width:55px !important;';
						$parametros['parametros_html'] = "placeholder='AM'";
					$html_salida.= html_input($id.'_ampm','lista',$parametros);
					break;
				case "calendario":
					$parametros = array(
						'estilos'	=> $estilos,
						'editable'										=> $editable,
						'parametros_html'             => " $parametros_html ",
						'funciones_especiales'        => array(
								array( 'accion_' . $id, 'fa fa-calendar', array( 'enlace' => 'javascript:mostrar_dialogo_emergente("calendario_1","'.$id.'");' ) ),
							),
						'valor_inicial' => $valor_inicial,
						);
					$html_salida = html_input( $id, 'texto_funcion_especial', $parametros );
					if( strpos( $html_apoyo_salida, 'function mostrar_calendario( fecha_inicial') === FALSE ){
						$html_apoyo_salida .="
						<script>
	            function mostrar_calendario( fecha_inicial, id_calendario, id_input_resultado ){
	              $.ajax({
	                'url': '".site_url()."/../s/mostrar_dialogo_emergente/calendario_1/'+fecha_inicial+':'+id_input_resultado,
	                'context': document,
	              }).done(function(data){
	                $('#'+id_calendario).html(data);
	              });
	            }
	            function seleccionar_fecha( fecha, id_input ){
	            	$('#'+id_input).val(fecha);
	            	cerrar_velo();
	            }
	           </script>";
	         }
					break;
				case "reloj":
					$parametros = array(
						'estilos'	=> $estilos,
						'editable'										=> $editable,
						'parametros_html'             => "placeholder = 'hh:mm AM/PM' $parametros_html ",
						'funciones_especiales'        => array(
								array( 'accion_' . $id, 'fa fa-clock-o', array( 'enlace' => 'javascript:alert("reloj");' ) ),
							),
						'valor_inicial' => $valor_inicial,
						);
					$html_salida = html_input( $id, 'texto_funcion_especial', $parametros );
					break;
				case "lista":
				case "select":
					$html_salida = "<div class='contenedor-input $clases_adicionales_contenedor_input' style='$estilos_adicionales_contenedor_input'>";
          $html_salida .= "<select id='$id' name='$id' class='$clases $clases_adicionales' style='padding: 3px; $estilos' $parametros_html >";
          if( trim( $valor_inicial ) === ""&& trim( $texto_inicial ) !== "" ) $valor_inicial = $texto_inicial;
          $html_salida.="<option value='"
                  .(($valor_inicial!=="")?$valor_inicial:"")
                  ."'>";
          $html_salida.=(($texto_inicial!=="")?$texto_inicial:"");
          $html_salida.="</option>";
          foreach($items as $valor=>$texto){
                  if(trim($valor)===""&&$texto=="") $valor=$texto;
                  $html_salida.="<option value='$valor'";
                  if(trim($valor_inicial)==(trim($valor))){
                          $html_salida.=" selected ";
                  }
                  $html_salida.=">";
                  $html_salida.=$texto;
                  $html_salida.="</option>";
          }
          $html_salida.="</select>";
          $html_salida.="</div>";
          break;
				case "opcionmultiple":
				case "seleccionmultiple":
				case "opciones_multiples":
					if ( ! isset( $parametros['clases_adicionales']) ) { $clases_adicionales = ''; }
					if( ! isset( $clases_adicionales_checkbox ) ){
						$clases .= " alinear-medio ";	
					} else{
						$clases .= " ".$clases_adicionales_checkbox." ";
					}
					$html_salida="<div class='seguido $clases '>\n";
					$i=0;
					foreach($items as $valor=>$texto){
						$i++;	
						$clases_adicionales = str_replace ('{@i}', $i, $clases_adicionales );

						if(trim($valor)==="") $valor=$texto;
						$html_salida.="<label style='white-space:nowrap;'>"
									 			."<input class='$clases $clases_adicionales' id='$id' name='$id' type='checkbox' value='$valor' style='margin:0px;margin-right:5px;' ";
						if ( isset( $valor_inicial[$valor] ) ) { $html_salida.= $valor_inicial[$valor]; } //CLEVER!!!!!! jjy
						if ( ! $editable ) { $html_salida.= ' readonly="readonly" disabled '; }
						$html_salida.= " >";
						$parametros['estilos'] = 'padding:0;margin-right: 7px;';
						$parametros['clases_adicionales'] = $clases_adicionales . ' seguido ';
						$html_salida.=html_etiqueta($texto,'',$parametros).'</label>';
					}
					$html_salida.="</div>";
					break;
				case "opcionsimple":
				case "seleccionsimple":
				case "opciones_simples":
					//$parametros['clases'] = "";
					if ( ! isset( $parametros['clases_adicionales']) ) { $clases_adicionales = ''; }
					$clases .= " alinear-medio ";
					$html_salida="<div class='seguido $clases'>\n";
					foreach($items as $valor=>$texto){
						if(trim($valor)==="") $valor=$texto;
						$html_salida.="<label style='white-space:nowrap;'>"
									 			."<input id='$id' name='$id' type='radio' value='$valor' style='margin:0px;margin-right:5px;' $parametros_html >";
						$parametros['estilos'] = 'padding:0;margin-right: 7px;';
						$html_salida.=html_etiqueta($texto,'',$parametros).'</label>';
					}
					$html_salida.="</div>";
					break;
				case "archivo":
				case "file":
				case "subirarchivo":
				case "subir_archivo":
					$html_salida="<input id='$id' name='$id' type='file' class='$clases' style='$estilos' $parametros_html >";
					break;
				case "imagen":
				case "image":
				case "img":
					$html_salida="<img src='$url_imagen' id='$id' name='$id' class='$clases' style='$estilos' $parametros_html >";
					break;
				case "texto_largo":
				case "memo":
				case "textolargo":
				case "textarea":
					$html_salida="<textarea id='$id' name='$id' type='password' class='$clases' style='$estilos' $parametros_html >$valor_inicial</textarea>";
					break;
				case "texto_enriquecido":
				case "editor_html":
				case "texto_wysiwyg":
				case "wysiwyg":
					if ( ! isset( $parametros['clases_adicionales']) ) { $clases_adicionales = ''; }
					// requiere!!! $html_includes = '<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>';
					$html_salida  = "<textarea id='$id' name='$id' type='password' class='$clases $clases_adicionales' style='$estilos' $parametros_html >$valor_inicial</textarea>";
					$html_apoyo_salida .= "<script>tinymce.init({selector:'#$id'});</script>";
					break;
				case "buleano":
				case "boolean":
				case "checkbox":
					$marcado=0;
					if ( ! isset( $parametros['clases_adicionales']) ) { $clases_adicionales = ''; }
					if ( ! isset( $parametros['valor']) ) { $valor = '1'; } // ... jjy v2
					// en la sig. linea fue cambiado "$valor_inicial" por $valor << que es lo correcto! .... jjy v2
					$html_salida = "<div class='contenedor-input $clases_adicionales_contenedor_input' style='$estilos_adicionales_contenedor_input'>";
					$html_salida.= "<i ";
					if ( ! $editable ) { $html_salida.= ' readonly="readonly" disabled '; }

					$html_salida.= " $parametros_html class='buleano fa fa-";
					if ( $valor_inicial == $valor ) { $html_salida.= 'check-'; $marcado=1;} // ... jjy v2
					$html_salida.="square-o fa-lg'></i>";
					$html_salida.= html_input($id, 'oculto', array('valor_inicial'=>$marcado));
					$html_salida.="</div>";
					$tipo_elemento = 'checkbox';

					if( strpos( $html_apoyo_salida, '$(this).toggleClass("fa-check-square-o")') === FALSE ){
						$html_apoyo_salida .= "
						<script>
							$(document).ready(function(){
								$('.buleano[readonly]').addClass('discreto');
								$('.buleano').not('[readonly]').on('click',function(){
									$(this).siblings().closest('.oculto').val(Math.abs(parseInt($(this).siblings().closest('.oculto').val())-1));
									$(this).toggleClass(\"fa-check-square-o\").toggleClass(\"fa-square-o\");
								});
							});
						</script>";
					}
					break;
				case "oculto":
				case "escondido":
				case "hidden":
			  	$clases = "oculto";
					$html_salida="<input id='$id' name='$id' type='hidden' class='$clases $clases_adicionales' style='$estilos' value='$valor_inicial' $parametros_html >";
					break;
				default:
					$clases="";
					$html_salida="<input id='$id' name='$id' type='$tipo_elemento' class='$clases' style='$estilos' $parametros_html >";
					break;
			}
		} // fin if de editable

		if ( trim( $etiqueta != "" ) ){
			$parametros = array();
			if ( isset( $clases_adicionales_etiqueta ) ) {
				$parametros = array( 'clases_adicionales' => $clases_adicionales_etiqueta );
			}
			$id_for = ( $etiqueta_con_for == TRUE ) ? $id : '';
			if( $tipo_elemento != 'checkbox' ) {
				$html_salida = html_etiqueta( $etiqueta, $id_for, $parametros ) .html_sangria('7px'). $html_salida;
			} else {
				$html_salida .= html_sangria('7px') . html_etiqueta( $etiqueta, $id_for, $parametros );
			}
		}
		if ( trim( $info_ayuda != "" ) ){
			$parametros   = array();
			$html_salida .= "&nbsp;&nbsp;<i title='$info_ayuda' class='fa fa-question-circle icono-info-ayuda'></i>"; // mod jjy v2
		}
		// verificar la siguiente línea ... el unico objetivo es que la apariencia sea igual en todos los navegadores! .. jjy v2
		$html_salida = str_replace( "\t", "", str_replace( "\n", "",  str_replace( ">  <", "><", str_replace( "> <", "><", $html_salida ) ) ) );
		// fin depuracion html ---- v2 jjy!
		return $html_salida."";
		/*# @ejemplo
				<?php
					$parametros = array(
						'clases'  => 'negrita',
						'estilos' => 'color:red;',
					);
				?>
				<?=html_etiqueta( 'Campo:', 'id_campo', $parametros )?>
		#*/
		/*# @salida
				<label for='id_campo' class='negrita' style='color:red;'>Campo:</label>
		#*/

	}
}

if( ! function_exists('html_etiqueta')) {

	function html_etiqueta ( $texto = "", $for = "", $parametros = array() ) {
		/** 
		 * @author jjyepez <jyepez@inn.gob.ve>
		 */
		//# @descripcion Genera el código en HTML para una etiqueta
		//# @parametro string $texto Texto de la etiqueta
		//# @parametro string $for ID del componente html_input asociado
		//# @parametro array $parametros Los diferentes parámetros que se esperan y sus valores por omisión >>//
		$clases             = ""; // puede ser cualquier clase o conjunto de clases validas 
		$estilos            = "";
		$id                 = "";
		$clases_adicionales = "";  
		//<<
		// se extraen las variables pasadas por parámetros .. ns
		extract($parametros);
		//se inicializan las variables .. ns
		$html_salida				= "";
		//-----------------------------------------//

		$html_salida = "<label id='$id' name='$id' for='$for' class='$clases $clases_adicionales' style='$estilos'>$texto</label>";

		return $html_salida;
		/*# @ejemplo
				<?php
					$parametros = array(
						'clases'  => 'negrita',
						'estilos' => 'color:red;',
					);
				?>
				<?=html_etiqueta( 'Campo:', 'id_campo', $parametros )?>
		#*/
		/*# @salida
				<label for='id_campo' class='negrita' style='color:red;'>Campo:</label>
		#*/
	}
}

if ( ! function_exists( 'html_lista_datos' ) ){
	/**
	*	Esta función crea una lista-grid simple basada en html
	*/
	function html_lista_datos ( $id, $arreglo_datos, $parametros=array() ) {
		/// atencion ..... debe normalizarse esta funcion para extraer los parametros !!!!!! ..... jjy 
		$mostrar_paginacion = FALSE; // aun en etapa muy experimiental .... no usar!! ... jjy
		$mostrar_acciones   = TRUE; 
		$paginacion = array();
		$n_registro_actual = 1;
		$icono_puntero = '';
		$columnas = array();
		$enlace_mostrar  = 'javascript:alert("mostrar")';
		$enlace_imprimir = 'javascript:alert("imprimir")';
		$enlace_editar   = 'javascript:alert("editar")';
		$enlace_eliminar = 'javascript:alert("eliminar")';

		extract( $parametros );

		//se hace una conversion del arreglo por si viene como obj_result de postgres .. jjy
		if( ! is_array( $arreglo_datos ) ) $arreglo_datos = ( array ) $arreglo_datos;

		$html_salida ="";
		$html_salida = "<table id='$id' name='$id' class='html-lista'>";

		$n_registros = count( $arreglo_datos );
		$fila        = 0;
		$n_campos    = 0;

		if ( count( $columnas ) == 0 && count( $arreglo_datos ) > 0 ) {
			if ( count ( $arreglo_datos ) == 0 ) {
				$columnas = array('error' => 'No se han definido columnas a mostrar y el arreglo de datos está vacío');
			} else {
				//prp($arreglo_datos,1);
				foreach ( $arreglo_datos[0] as $campo => $valor ) {
					$columnas[$campo] = ucwords( str_replace( '_', ' ', $campo ) );
				}
			}
		}
		$html_salida.= "<tr class='cabecera_lista'>\n";
		$html_salida.= "<th class='columna-apuntador-lista'></th>\n";

		// se espera que en el arreglo $columnas venga cada nombre de campo de la tabla con su título asociado
		foreach( $columnas as $campo => $titulo ){

			$mostrar_col[$campo] = $titulo;

			//se muestran las cabeceras de las columnas .. jjy
			//sólo la primera vez
			$n_campos++;
			if(isset($mostrar_col[$campo])) {
				$titulo=$mostrar_col[$campo];
				$html_salida.= "<th";
				if ( $n_campos == 1 ) $html_salida .= " class = 'alinear-izquierda' "; else $html_salida .= " class = 'alinear-centro' ";
				$html_salida.=">".$titulo."</th>";
			}
		}
		if ( $mostrar_acciones === TRUE ){ // iconos de accion ....  jjy
			$html_salida .= "<th class='columna-acciones alinear-abajo'>";

			/* 
			******** OJO ...... ESTA PAGINACION MEJOR DEBERIA HACERSE EN UNA FILA AL FINAL DE LA LISTA .... EVALUAR .... jjy
			**/
			if( $mostrar_paginacion === TRUE ){
				$total_registros_orig = count($arreglo_datos);
				$total_registros = count($arreglo_datos);

				$n_registro_actual = $paginacion[1];
				//prp( $n_registro_actual .' de ' . count($arreglo_datos) );
				for( $k=0;$k<$n_registro_actual;$k++){
					unset( $arreglo_datos[$k] );
					//prp($arreglo_datos);
				}
				//prp( $n_registro_actual .' de '. count($arreglo_datos) );
				$n_registros = count( $arreglo_datos );

				$parametros_paginacion = array(
						'enlace'               => './',
						'total_registros'      => $total_registros, // extraer de "datos" !
						'registros_por_pagina' => 12,
					  );
				if( $paginacion != array() ){ // parametros generales del componente ...
					$parametros_paginacion['enlace']                 = $paginacion[2];
					$parametros_paginacion['registros_por_pagina']   = $paginacion[0];
					$parametros_paginacion['parametros_adicionales'] = array( 'display_pages' => FALSE );
					//$parametros_paginacion['total_registros']    = $paginacion[1];
				}
				$html_salida .= '<div style="position:absolute;margin-top:-50px;">'
						.html_paginacion( 'id_paginacion_lista_general', $parametros_paginacion )
						.'</div>';
			}	

			$html_salida .= "</th>\n";
		}

		$html_salida .= "<td class='invisible'></td>\n";
		$html_salida .= "</tr>\n";

		$html="";
		$mensaje="";
		$tipo_mensaje="";

		if( $n_registros > 0 ){

			//prp($arreglo_datos);

			foreach( $arreglo_datos as $reg ){ 

				//MAGIA para reordenar los campos de acuerdo a las columnas suministradas!!!!
				// elegancia ... copiada de la web ! ... jjy
				$reg = array_replace( array_flip( array_keys( $mostrar_col ) ), $reg );				

				//prp($reg,1);

				if ($mostrar_paginacion == TRUE){
					if ( $fila >= $parametros_paginacion['registros_por_pagina'] ){
						break;
					}
				}

				/**
				AQUI INICIA EL RECORRIDO DE LOS REGISTROS PARA MOSTRAR LAS FILAS ........ jjy *** 
				**/
				$fila++;
				$id_registro = $fila;
				if ( isset( $reg['id'] ) ){
					$id_registro = $reg['id'];
				}

				$par_o_impar=(($fila%2)==0)?'par':'impar';
				$html_campos_hidden="";
				$html_salida.= "<tr class='fila-registro' rel='".$id_registro."'>";

				$n_campo = 0;
				$html_acciones = "";
				if ( $mostrar_acciones === TRUE ) {

					$enlace_accion['mostrar']  = $enlace_mostrar; // se reinician al valor original en cada vuelta de registro ... jjy
					$enlace_accion['eliminar'] = $enlace_eliminar;
					$enlace_accion['imprimir'] = $enlace_imprimir;
					$enlace_accion['editar']   = $enlace_editar;

					foreach( $reg as $campo_aux => $valor_aux ){
						$enlace_accion['mostrar']  = str_replace('{@'.$campo_aux.'}', $valor_aux, $enlace_accion['mostrar']);
						$enlace_accion['eliminar'] = str_replace('{@'.$campo_aux.'}', $valor_aux, $enlace_accion['eliminar']);
						$enlace_accion['imprimir'] = str_replace('{@'.$campo_aux.'}', $valor_aux, $enlace_accion['imprimir']);
						$enlace_accion['editar']   = str_replace('{@'.$campo_aux.'}', $valor_aux, $enlace_accion['editar']);
					}

					$html_acciones = "
						<td class='contenedor-iconos-accion ancho-60'>
					  <span class='contenedor-input'>&nbsp;";
					$p = 0;
					if ( array_search( 'imprimir', $iconos_accion ) != "" ) { $html_acciones .= "<a title='Imprimir este registro' id='icono_imprimir' 	name='icono_imprimir' href='".$enlace_accion['imprimir']."'><i style='left: {$p}px;' 	class='icono-accion-izquierda fa fa-print'></i></a>"; $p+=20;}
					if ( array_search( 'editar', $iconos_accion ) != "" ) {   $html_acciones .= "<a title='Editar' id='icono_editar' 		name='icono_editar' 	href='".$enlace_accion['editar']."'><i style='left: {$p}px;' class='icono-accion-izquierda fa fa-pencil'></i></a>"; $p+=20;}
					if ( array_search( 'eliminar', $iconos_accion ) != "" ) { $html_acciones .= "<a title='Eliminar' id='icono_eliminar' 	name='icono_eliminar' href='".$enlace_accion['eliminar']."'><i 	style='left: {$p}px;' class='icono-accion-izquierda fa fa-trash-o'></i></a>"; $p+=20;}
					
					$html_acciones .= "&nbsp;</span>\n</td>\n";
				}
				$html_salida .= "
					<td style='width:24px;' class='contenedor-iconos-accion'>
						<span class='contenedor-input'>&nbsp;";
							
				if ( $mostrar_acciones === TRUE ) { $html_salida .= "<a id='icono_mostrar' name='icono_mostrar' href='".$enlace_accion['mostrar']."'>"; }

				if ( trim($icono_puntero) != "" ){
					$html_salida .= "<img class='puntero_lista' src='".base_url()."/imgs/$icono_puntero'/>";
				} else {
					$html_salida .= "<i style='left: 0px;' class='icono-accion-izquierda fa fa-caret-right'></i>";
				}

				if ( $mostrar_acciones === TRUE ) { $html_salida .= "</a>"; }

				$html_salida .= "</span>\n</td>";

				/*
				!!!!! A PARITR DE ACA SE RECORREN UNO A UNO TODOS LOS CAMPOS=>VALORES DE CADA REGISTRO PARA ARMAR LAS COLUMNAS !!!! ... jjy
				*/
				foreach( $reg as $campo=>$valor ){

					if ( isset( $mostrar_col[$campo] ) ) {
						$n_campo++;

						$html_salida .= "\n<td ";
						if ( $mostrar_acciones === TRUE ) {
							$html_salida .= "onclick='javascript:document.location.href=\"".$enlace_accion['mostrar']."\";' ";
						}
						if ( $n_campo != 1 ) {
							$html_salida .= " class='alinear-centro' ";
						}
						$html_salida .= ">";
						if ( isset ( $campos[$campo] ) ) { //si viene con formato ... jjy
							$valor = str_replace( '{@valor}', $valor, $campos[$campo] );
						}
						$html_salida.= $valor;
						$html_salida.= "</td>";
					} else {
						$html_campos_hidden .= "<input id='{$campo}[]' name='{$campo}[]' type='hidden' class='ancho-full' value='{$valor}' rel='$fila' />\n";
					}
				}
				
				$html_campos_hidden.="<input id='nf' name='nf' type='hidden' rel='$fila'/>";
				$html_salida.= '<td class="invisible">'.$html_campos_hidden.'</td>';
				$html_salida.= $html_acciones;
				$html_salida.= "</tr>\n";
			}

		} else {

			$mensaje="La tabla está vacía actualmente.";
			$tipo_mensaje="advertencia";
			
		}
		$reg=array();
		if(isset($parametros['campos'])){
			foreach($parametros['campos'] as $campo=>$formato){
				if( !is_integer($campo) ) {
					$reg[$campo] = "";
				} else {
					$reg[$formato] = "";
				}
			}
		}
		if ( isset( $parametros['codigo_tabla'] ) ) {
			$reg['codigo_tabla']=$parametros['codigo_tabla'];
		}

		//se muestran una fila vacía al final para un nuevo registro .. jjy
		$html="";

		/* 
		******** OJO ...... ESTA PAGINACION MEJOR DEBERIA HACERSE EN UNA FILA AL FINAL DE LA LISTA .... EVALUAR .... jjy
		**/
		if( $mostrar_paginacion === TRUE ){
			$total_registros = $total_registros_orig;

			$n_registro_actual = $paginacion[1];
			//prp( $n_registro_actual .' de ' . count($arreglo_datos) );
			for( $k=0;$k<=$n_registro_actual;$k++){
				unset( $arreglo_datos[$k] );
				//prp($arreglo_datos);
			}
			//prp( $n_registro_actual .' de '. count($arreglo_datos) );
			$n_registros = count( $arreglo_datos );

			$parametros_paginacion = array(
					'enlace'               => './',
					'total_registros'      => $total_registros, // extraer de "datos" !
					'registros_por_pagina' => 12,
				  );
			if( $paginacion != array() ){ // parametros generales del componente ...
				$parametros_paginacion['enlace']               = $paginacion[2];
				$parametros_paginacion['registros_por_pagina'] = $paginacion[0];
				//$parametros_paginacion['total_registros']    = $paginacion[1];
			}

			$html_pag = html_paginacion( 'id_paginacion_lista_general', $parametros_paginacion );
			if( trim( $html_pag ) != '' ){
				$html_pag = "<tr><td colspan = '".($n_campos+2)."' align='center'>"
										.'<div>'.$html_pag.'</div><hr>'
										."</td></tr>";
			}
			//prp( $html_pag );
			$html .= $html_pag;
		}	

		$html_salida.= $html;
		$html_salida.= "</table>\n";
		$html_salida.=html_mensaje('',$mensaje,$tipo_mensaje);

		$enlace_mostrar = "#"; $enlace_imprimir = "#"; $enlace_editar = "#"; $enlace_eliminar = "#";
		if ( isset ( $parametros['enlace_mostrar_registro'] ) ){ $enlace_mostrar = $parametros['enlace_mostrar_registro']; }
		if ( isset ( $parametros['enlace_imprimir_registro'] ) ){ $enlace_imprimir = $parametros['enlace_imprimir_registro']; }		
		if ( isset ( $parametros['enlace_editar_registro'] ) ){ $enlace_editar = $parametros['enlace_editar_registro']; }		
		if ( isset ( $parametros['enlace_eliminar_registro'] ) ){ $enlace_eliminar = $parametros['enlace_eliminar_registro']; }		
		$script = "
			<script>
				$(document).ready(function(){
					$('tr.fila-registro .contenedor-input').hide();
					$('tr.fila-registro').mouseover( function(){ $(this).find('.contenedor-input').show(); });
					$('tr.fila-registro').mouseout ( function(){ $(this).find('.contenedor-input').hide(); });
				});
			</script>";
		$html_salida.=$script;

		return $html_salida;
	}

}

if( ! function_exists( 'html_formulario_ini' ) ) {
	function html_formulario_ini( $id, $parametros = array() ){
		//# @descripcion Componente que genera una salida HTML normalizada de diversos elementos tipo <INPUT>
		//# @parametro string $id El ID que se le asignará al elemento <INPUT> resultante
		//# @parametro array $parametros Los diferentes parámetros que se esperan y sus valores por omisión >>//
			$destino        = '_self';
			$accion         = '';
			$enlace         = '';
			$metodo         = 'POST';
			$enctype        = '';
			$tabla_asociada = '';
			$clases_adicionales = '';
		//<<<
		extract( $parametros );

		if ( trim( $enlace ) != "" && trim( $accion ) == "" ) $accion = $enlace;
		if ( trim( $accion ) != "" && trim( $enlace ) == "" ) $enlace = $accion;
		$html_salida = "<form "
				. " class = 'formulario $clases_adicionales' "
				. " id = '$id' name='$id' "
				. " target = '$destino' "
				. " action = '$enlace' "
				. " method = '$metodo' "
				. " enctype = '$enctype' "
				. ">\n";
		$html_salida .= html_input( 'tabla_asociada', 'oculto', array( 'valor_inicial' => $tabla_asociada ) );
		return $html_salida;
	}
}

if( ! function_exists( 'html_formulario_fin' ) ) {
	function html_formulario_fin( $parametros = array() ){
		$html_salida = "";
		$html_salida .= "</form>\n";
		return $html_salida;
	}
}
if ( ! function_exists( 'html_br' ) ) {
	function html_br ( $espacio_vertical = '5px' ) {
		return "<hr class='br-espacio-fijo sin-margenes' style='height:".$espacio_vertical." '>\n";
	}
}
if ( ! function_exists( 'html_hr' ) ) {
	function html_hr ( $espacio_vertical = '1px', $parametros = array() ) {
		$br = '';
		if( $espacio_vertical === 'br' ){
			$espacio_vertical = '1px';
			$br = html_br();
		}
		$estilos = 'border-width:'.$espacio_vertical.';'; // estilo fijo ... jjy
			if( $espacio_vertical == '1px'){
				$estilos .= 'border-top:0;';
			}
		$estilos_adicionales = 'border-color: #ccc;';
		extract( $parametros );

		return $br."<hr class='hr-alto-fijo sin-margenes' style='".$estilos." ".$estilos_adicionales."'>".$br."\n";
	}
}

if( ! function_exists('extraer_info_tabla')) {

	function extraer_info_tabla($tabla="",$parametros=array()) {

		//se instancia la clase Controller para poder extraer datos de la bd .. ns!
		$CI = & get_instance();
		//se inicializan variables .. ns
        $data_salida=array();
        //se extrae la informacion sobre los campos de la tabla .. ns
        if( ! table_exists($tabla)){
			$data_salida['campos']	=array();
        } else {
			$campos_arr            =$CI->db->field_data($tabla);	
			$data_salida['campos'] =$campos_arr;
	    }
	    //se cierra la conexion .. ns
        $CI->db->close();
        return $data_salida;
	}

	//version extendida basada n la tabla information_schema.columns de postgres .. ns
	function extraer_info_tabla_ext($tabla="",$parametros=array()) {

		//se instancia la clase Controller para poder extraer datos de la bd .. ns!
		$CI = & get_instance();
		//se inicializan variables .. ns
        $data_salida=array();
        //se extrae la informacion sobre los campos de la tabla .. ns
        $info_tabla=explode('.',$tabla);
        $CI->db->from('information_schema.columns');
        $esquema="public"; //por defecto .. ns
        if(isset($info_tabla[1])) {
        	$esquema=$info_tabla[0];
        	$tabla 	=$info_tabla[1];
        } else {
        	$tabla 	=$info_tabla[0];
        }
		$CI->db->where('table_schema',$esquema);
        $CI->db->where('table_name',$tabla);
        $CI->db->order_by('ordinal_position ASC');

        $rs=$CI->db->get();
        $campos_arr=$rs->result_array();
	        $data_salida['campos']  =$campos_arr;
        
	    //se cierra la conexion .. ns
        $CI->db->close();
        return $data_salida;
	}
}

if( ! function_exists('html_lista_segun_tabla')) {

	function html_lista_segun_tabla($id, $parametros=array()){ 

		//se instancia la clase Controller para poder extraer datos de la bd .. ns!
		$CI = & get_instance();
		//se inicializan variables .. ns
        $html_salida="";
		//Parámetros esperados ... jjy
		$clases="";
		$estilos="";
		$texto_por_defecto="-- Seleccione --";
		$valor_por_defecto="0";
		$texto_seleccionado="";
		$valor_seleccionado="";
		$datos['campos']=array();
		$datos['datos']=array();
		// los parametros correctos remplazaran a los inicializados en "" .. jjy
		extract($parametros);
		//
		valores_lista_segun_tabla($parametros);

        $html_salida="<select class='$clases' style='$estilos' id='$id' name='$id'>\n";
        	if($texto_por_defecto!=""){
		        $html_sub1="<option value='$valor_por_defecto' ";
		        if($valor_por_defecto==$valor_seleccionado){
		        	$html_sub1.=" selected='selected' ";
		        }
		        $html_sub2=">$texto_por_defecto</option>";
		        $html_salida.=$html_sub1.$html_sub2;
		    }

        foreach ($datos['datos'] as $reg) {
			foreach($reg as $campo=>$valor){
                if($campo==$campo_lista_valor){
                    $html_sub1="<option value='$valor' ";
					if($valor_seleccionado!="" && $valor==$valor_seleccionado){
			        	$html_sub1.=" selected='selected' ";
			        }
                }
		         //|| ($texto_seleccionado!="" && $valor==$texto_seleccionado))
                if($campo==$campo_lista_texto){
                	if($texto_seleccionado!="" && $valor==$texto_seleccionado){
		        		$html_sub1.=" selected='selected' ";
		        	}
                    $html_sub2=">$valor</option>\n";
                }
            }
            $html_salida.=$html_sub1.$html_sub2;
        }
        $html_salida.="</select>\n";

        return $html_salida;
	}
}

if( ! function_exists('valores_lista_segun_tabla')) {

	function valores_lista_segun_tabla ($tabla,$parametros=array()){

		//se instancia la clase controller! .. ns
		$CI = & get_instance();
        //parametros esperados ... jjy
        $texto_por_defecto  ="";
        $valor_por_defecto  ="";
        $valor_seleccionado ="";
        $tabla_origen       =$tabla;
        $condicion_filtro   ="";
        $orden_salida       ="";
        $consulta_sql_origen="";
        $campo_lista_valor  ="id_o_codigo"; //pasar valor correcto .. ns
        $campo_lista_texto  ="descripcion"; //pasar valor correcto .. ns
        //se extraen los parámetros .. ns
        extract($parametros);
        //se prepara la extracción de los datos .. ns
        $CI->db->select($campo_lista_valor.','.$campo_lista_texto);
        $CI->db->from($tabla_origen);
        if(isset($condicion_filtro)&&$condicion_filtro!="") $CI->db->where($condicion_filtro);
        if(isset($orden_salida)&&$orden_salida!="") $CI->db->order_by($orden_salida);
        $rs = $CI->db->get();
        //
        $resultado['campos']=$rs->list_fields();
        $resultado['datos']=$rs->result();
        //
        $CI->db->close();
        return $resultado;
    }
}

if ( ! function_exists( 'html_campo_celda_con_etiqueta' ) ) {

	function html_campo_celda_con_etiqueta($id = '', $parametros = array()){

		$tipo 	  = "texto";
		$etiqueta = "etiqueta";
		$placeholder = '';
		extract($parametros);
		$salida = '';
		$salida = '<table width="100%"><tr>'
					.'<td><label style="white-space:nowrap;" for="'.$id.'">'.$etiqueta.'</label></td>'
					.'<td width="100%">';
		switch ($tipo){
			case "fecha":
				if ( $placeholder == '' ) $placeholder = 'dd/mm/aaaa';
				$html_campo = '<input placeholder="'.$placeholder.'" type="text" style="width:100%" id="'.$id.'" name="'.$id.'"/>';
			break;

			case "list-a_radios":
				//print_r($parametros);
				$html_campo = '<select>';
				foreach ($valores as $id => $datos) {
					//$html_campo .= '<input id="'.$id.'" name="'.$id.'" type="radio" value="'.$datos[1].'"><label for="'.$id.'">'.$datos[0]."</label>\n";
					$html_campo .= '<option value="'.$datos[1].'">'.$datos[0]."</option>\n";
				}
				$html_campo .= "</select>\n";
				//$html_campo = '<input placeholder="'.$placeholder.'" type="text" style="width:100%" id="'.$id.'" name="'.$id.'"/>';
			break;

			case "texto":
			default:
				$html_campo = '<input placeholder="'.$placeholder.'"" type="text" style="width:100%" id="'.$id.'" name="'.$id.'"/>';
			break;
		}
		$salida .= $html_campo;
		$salida .= '</td>'
				  .'</tr></table>';
		return $salida;

	}

}

if ( ! function_exists( 'html_tabla_formulario' ) ) {

	function html_tabla_formulario( $id = '', $parametros = array() ){

		extract( $parametros );
		$salida = '';

		$salida .= "<div id='$id' name '$id'>\n";
		$salida .= "<table width='100%'>\n";
		
		foreach ($filas as $fila) {
			$salida .= "<tr>\n";

			foreach ( $fila as $indice => $contenido ) {

				$tipo = 'texto';
				if ( is_array( $contenido ) && $indice != '_titulo' && $indice != '_span') {
					$tipo = $contenido [1];
					$contenido = $contenido [0];
				}

				switch ( $indice ) {
					case '_titulo': 
						$salida .= '<th ';
						if (isset ( $contenido['_span'] ) ) {
							$salida .= 'colspan="'.$contenido['_span'].'"';
						}
						$salida .= '>'.$contenido['_texto']."</th>\n";
					break;

					case '_span':
						$salida .= '<td ';
						foreach ( $contenido as $sub_indice => $sub_contenido ) {

							switch ( $sub_indice ) {
								case '_columnas':
									$salida .= 'colspan="'.$sub_contenido.'">';
									break;
								
								default:

									if ( is_array( $sub_contenido ) ) {

										$valores = array();
										
										$etiqueta = $sub_contenido[0];
										$tipo = $sub_contenido[1];
										if ( isset( $sub_contenido [2] ) ){
											$valores = $sub_contenido [2];
										}

										$sub_parametros = array( 'id'       => $indice,
																 'tipo' 	=> $tipo,
																 'etiqueta' => $etiqueta,
																 'valores'	=> $valores,
															); 
										$salida .= html_campo_celda_con_etiqueta( '', $sub_parametros );

									} else {

										$salida .= '<span>';
										$sub_parametros = array( 'id'       => $sub_indice,
																 'tipo' 	=> $tipo,
																 'etiqueta' => $sub_contenido ); 
										$salida .= html_campo_celda_con_etiqueta( '', $sub_parametros );
										$salida .= '</span>';
									}
									break;
							}
						}
						$salida .= "</td>\n";
					break;

					default:
					
						$salida .= '<td>';
						if ( is_array( $contenido ) ) {

							$etiqueta = $contenido[0];
							$tipo = $contenido[1];
							$valores = $contenido [2];

							$sub_parametros = array( 'id'       => $indice,
													 'tipo' 	=> $tipo,
													 'etiqueta' => $etiqueta,
													 'valores'	=> $valores,
												); 
							$salida .= html_campo_celda_con_etiqueta( '', $sub_parametros );

						} else {

							$sub_parametros = array( 'id'       => $indice,
													 'tipo' 	=> $tipo,
													 'etiqueta' => $contenido ); 
							$salida .= html_campo_celda_con_etiqueta( '', $sub_parametros );
						}
						$salida .= "</td>\n";
					break;
				}
			}
			$salida .= "</tr>\n";
		}
		$salida .= "</table>\n";
		$salida .= "</div>\n";
		
//		echo "<pre>", print_r($parametros);

		return $salida;
	}

}

if ( ! function_exists( 'html_enlace' ) ) {

	function html_enlace ($enlace = 'javascript:void(0);', $parametros = array() ) {
		$id               = '';
		$texto            = $enlace;
		$destino          = '_self';
		$tooltip          = '';
		$clase            = '';
		$clase_adicional  = '';
		$estilo_adicional ='';

		extract( $parametros );

		$html_salida = "";

		$html_salida  .= ""
									.  "<a id = '$id' name = '$id' "
									.  "	 href = '$enlace' "
									.  "	 target = '$destino' "
									.  "	 title = '$tooltip' "
									.  "	 class = '$clase $clase_adicional' "
									.  "	 style = '$estilo_adicional'>"
									.  $texto
									.  "</a>\n";

		return $html_salida;
	}
}

if ( ! function_exists( 'html_enlace_boton' ) ) {

	function html_enlace_boton ($id = '', $parametros = array() ) {
		$clase_iconos           = 'glyphicons';
		$descripcion            = '';
		$tipo                   = 'boton_icono';
		$icono                  = '';
		$enlace                 = "javascript:alert('definir enlace');";
		$destino                = '_self';
		$posicion_icono         = '';
		$tooltip                = '';
		$clase                  = 'fa btn-default btn btn-sm sin-outline';
		$clase_adicional        = '';
		$clases_adicionales     = '';
		$clase_adicional_icono  = 'fa fa-large';
		$estilo_adicional       ='';
		$estilo_adicional_icono ='';
		$alinear_icono          = "";
		$parametros_html 				= "";

		extract( $parametros );

		//retro-compatibilidad con bootsrap 2
		$icono = str_replace( ' icon-', ' fa fa-',' '.$icono.' ' );

		if ( trim( $clase_adicional == '' && trim( $clases_adicionales ) != '' ) ) $clase_adicional = $clases_adicionales; // retro-compatibilidad !!! ... jjy v2

		if ( trim( $tooltip == '' && trim ( $descripcion ) != '' ) ) $tooltip = $descripcion;
		if ( trim( $tooltip == '' ) ) $tooltip = $enlace;
		if ( trim( $tooltip == '_nada' ) ) $tooltip = '';

		if ( trim( $posicion_icono !== '') ) {
			$coordenadas        = explode(' ', trim( $posicion_icono ) );
			$coordenadas_salida = $coordenadas;
			foreach ( $coordenadas as $i => $valor) {
				if ( strpos( $valor, 'px' ) === FALSE ) {
					$coordenadas_salida[$i] = (string) 
						( ( ( intval( $valor ) - 1 ) * 20 ) * (-1) ). 'px';
				}
			}
			$posicion_icono = implode( ' ', $coordenadas_salida );
		}

		$salida = '';

		if ( $tipo == 'boton_icono' && trim( $icono ) != '') {
			
			$salida = '<a class=" '.$clase.' '.$clase_adicional.'" ' 
						.'id="' . $id . '" name="' . $id . '" '
						.'style="'.$estilo_adicional.'" '
						.'href="'.$enlace.'" '
						.'title="'.$tooltip.'" '
						.'target="'.$destino.'" '
						. $parametros_html . ' >';

			$icono_boton = "";
			if( $icono != '_nada' && trim( $icono ) != '' ){
				$icono_boton = '<i class="'.$icono.' '.$clase_adicional_icono.'" style="'
					.$estilo_adicional_icono.'"></i>';

				if ( trim ( $descripcion ) !== '' ){
					if ( trim ( $alinear_icono ) == 'derecha' ) {
						$salida .= $descripcion . '&nbsp;&nbsp;' . $icono_boton;
					} else {
						$salida .= $icono_boton . '&nbsp;&nbsp;' . $descripcion;
					}
				} else {
					$salida .= $icono_boton;
				}

			} else {
				$salida .= $descripcion;
			}

			$salida .= '</a> ';

		} elseif ( $tipo == 'solo_icono' && trim( $icono ) != '') {
			if ( trim ( $descripcion ) !== '' ){
				$clase .= ' icono-con-texto';
			}
			$salida .= '<a style="'.$estilo_adicional.'" '
					. 'class="a-sin-borde seguido '.$clase.'" '
					. 'href="' 		. $enlace 	. '" '
					. 'target="' 	. $destino 	. '" '
					. 'title="' 	. $tooltip 	. '"'
					. '><div class = "' . $tipo . " " . $clase_adicional
					. '" id="' . $id . '" name="' . $id . '">'
					. '<div class="icono ' . $icono . '" '
					. 'style="background-position:' . $posicion_icono . ';">'
					. '</div>'
					. '</div></a> '."\n";

		} elseif ( $tipo == 'boton_glyphicon' && ( trim( $icono ) != '' OR $posicion_icono != '' ) ) {

			$salida = '<a class="btn sin-outline icono-con-texto'.$clase_adicional.'" ' 
							. 'style="'.$estilo_adicional.'" '
						  . 'href="'.$enlace.'" title="'.$tooltip.'" target="'.$destino.'">'
							. '<div class="icono '
							. $clase_iconos . ' ' 
							. $icono . '" '
							. 'style="background-position:' . $posicion_icono . ';">'
							. '</div>';

			if ( trim ( $descripcion ) !== '' ){
				$salida .= "&nbsp;<div class='descripcion-boton'>$descripcion</div>";
			}
			$salida .= '</div></a>&nbsp;';
		}

		return $salida;
	}
}


if ( ! function_exists( 'html_paginacion' ) ){

	//crea los links de paginacion según los parámetros dados ... aplica para cualquier vista ... jjy
	function html_paginacion ( $id = '', $parametros = array() ) {
		
		//valores por omisión ... jjy
		$enlace               = '#';
		$total_registros      = 10;
		$registros_por_pagina = 5;
		//parametros propios del helper de paginacion !!!
		$parametros_adicionales['display_pages'] = TRUE;
		$parametros_adicionales['full_tag_open'] = '';

		// extracción de parámetros para sobreescribir valores por omisión ... jjy
		extract ( $parametros );
		extract ( $parametros_adicionales );

		//prp( $parametros_adicionales );

		// validación de parámetros ... jjy
		$enlace = ( substr( trim( $enlace ), -1 ) == '/')
				  ? substr( trim( $enlace ), 0, -1)
				  : trim( $enlace );

		// variable de salida de la función ... jjy
		$salida = "";

		$CI =& get_instance();
		$CI->load->library('pagination');

		$config = array(
					//'full_tag_open'  => $parametros_adicionales['full_tag_open'],
					'base_url'       => $enlace,
					'total_rows'     => $total_registros,
					'per_page'       => $registros_por_pagina,
					'use_page_numbers'   => FALSE, // debe tomar el reg no la pagina !!
					'page_query_string' => FALSE,
					'display_pages' => $display_pages, // mostrar numeros de pag o no ... jjy
					// la página actual debe venir siempre como último parámetro de la uri ... jjy
					'uri_segment'    => count ( explode('/', str_replace ( site_url(), '', $enlace ) ) ),
					// el parametro anterior debe ser revisado y probado para muchos otros casos .... jjy
					//'full_tag_close' => '</div></div>',
					'num_links'      => 3, //round ( $total_registros / $registros_por_pagina ),

					'first_link'		=> '<i class="icon fa fa-double-angle-left"></i>', //html_enlace_boton('pagina-primero', array('enlace' => '', 'icono'=>'fa fa-double-angle-left', 'clase_adicional_icono' => 'fa fa-small', 'estilo_adicional' => 'padding:1px 5px;')),
					'last_link'			=> '<i class="icon fa fa-double-angle-right"></i>', //html_enlace_boton('pagina-final', array('enlace' => '', 'icono'=>'fa fa-double-angle-right', 'clase_adicional_icono' => 'fa fa-small', 'estilo_adicional' => 'padding:1px 5px;', 'tooltip'=> $total_registros )),

					'prev_link'			=> '<i class="icon fa fa-angle-left"></i>', //html_enlace_boton('pagina-anterior', array('enlace' => '', 'icono'=>'fa fa-chevron-left', 'clase_adicional_icono' => 'fa fa-small', 'estilo_adicional' => 'padding:1px 5px;', 'tooltip'=> $total_registros )),
					'next_link'			=> '... <i class="icon fa fa-angle-right"></i>', //html_enlace_boton('pagina-siguiente', array('enlace' => '', 'icono'=>'fa fa-chevron-right', 'clase_adicional_icono' => 'fa fa-small', 'estilo_adicional' => 'padding:1px 5px;')),
				);

		$CI->pagination->initialize($config); // aplica los parametros ... jjy
		
		$salida = $CI->pagination->create_links();

		/*$salida = "<div class='btn-group'> "
		. html_enlace_boton('pagina-anterior', array('icono'=>'fa fa-chevron-left', 'clase_adicional_icono' => 'fa fa-small', 'estilo_adicional' => 'padding:1px 5px;', 'tooltip'=> $total_registros ))
		. html_enlace_boton('pagina-siguiente', array('icono'=>'fa fa-chevron-right', 'clase_adicional_icono' => 'fa fa-small', 'estilo_adicional' => 'padding:1px 5px;'))
		. "</div>"
		. $salida;*/

		return $salida;
	}
}

if ( ! function_exists( 'html_grid_simple' ) ){

	//crea un grid simple basado en html .. jjy
	function html_grid_simple ( $id, $arreglo_datos, $parametros=array() ) {
		$mostrar_paginacion = FALSE;
		$columnas           = array();
		$formato_columnas   = array(); //
		$codigo_tabla       = "";
		$editable           = FALSE;
		$nuevo_registro     = FALSE;
		$mostrar_acciones   = FALSE;

		extract( $parametros );

		//se hace una conversion del arreglo por si viene como obj_result de postgres .. jjy
		if(!is_array($arreglo_datos)) $arreglo_datos=(array) $arreglo_datos;

		$html_salida   = "";
		$html_acciones = "";
		$html_salida   = "<table id='$id' name='$id' class='html-grid'>";

		$n_registros =count($arreglo_datos);
		$fila        =0;
		$n_campos    =0;

		if ( count ( $columnas ) == 0 && count( $arreglo_datos ) > 0 ) {
			if ( count ( $arreglo_datos ) == 0 ) {
				$columnas = array('error' => 'No se hasn definido columnas a mostrar y el arreglo de datos está vacío');
			} else {

				foreach ( $arreglo_datos[0] as $campo => $valor ) {
					$columnas[$campo] = $campo;
				}
			}
		}

		if( count( $columnas ) > 0 ){

			$html_salida.= "<tr>";
			$html_salida.= "<th></th>\n";
			foreach( $columnas as $campo => $titulo ){
				$mostrar_col[$campo]=$titulo;

				//se muestran las cabeceras de las columnas .. jjy
				//sólo la primera vez
				$n_campos++;
				if(isset($mostrar_col[$campo])) {
					$titulo=$mostrar_col[$campo];
					$html_salida.= "<th>".$titulo."</th>";
				}
			}
			$html_salida.= "<td class='invisible'></td>\n";
			$html_salida.= "<th style='width:0px;'></th>\n";
			$html_salida.= "</tr>\n";
		}

		$html="";
		$mensaje="";
		$tipo_mensaje="";

		if($n_registros>0){

			foreach($arreglo_datos as $reg){
				$fila++;

				$par_o_impar=(($fila%2)==0)?'par':'impar';
				$registro_editable=($editable==true)?'registro_editable':'';
				$html_campos_hidden="";
				$html_salida.= "<tr class='fila-registro seleccionable {$par_o_impar} {$registro_editable}'>";
				$html_salida.= '<td class="alinear-abajo sin-padding"></td>';
				foreach($reg as $campo=>$valor){
					if(isset($mostrar_col[$campo])) {
						$html_salida.= "\n<td>";
						if($editable) {
							$html_salida.= "<input class='{$par_o_impar} celda_grid' id='{$campo}[]' name='{$campo}[]' type='text' class='{$par_o_impar} ancho-full' value='$valor' title='$valor' rel='$fila'/>";
						} else {
							if ( isset ( $campos[$campo] ) ) { //si viene con formato
								$valor = str_replace( '{@valor}', $valor, $campos[$campo] );
							}
							if ( isset ( $formato_columnas[$campo] ) ) { //si viene con formato
								$valor = $formato_columnas[$campo];
								foreach($reg as $campo_x => $valor_x){
									$valor = str_replace( '{@'.$campo_x.'}', $valor_x, $valor);
								}
							}
							$html_salida.= $valor;
						}
						$html_salida.= "</td>";
					} else {
						$html_campos_hidden.= "<input id='{$campo}[]' name='{$campo}[]' type='hidden' class='ancho-full' value='{$valor}' rel='$fila' />\n";
					}
				}
				if ( $mostrar_acciones == TRUE ){
					$html_acciones="<i class='fa fa-plus-sign-alt'> <i class='fa fa-trash-o'> ";
				}

				$html_campos_hidden.="<input class='registro_editado' id='registro_editado[]' name='registro_editado[]' type='hidden' rel='$fila'/>";
				$html_salida.= '<td class="invisible">'.$html_campos_hidden.'</td>';
				$html_salida.= '<td class="alinear-centro" style=" width="15px"; padding:0px !important;">'.$html_acciones.'</td>';//REVISAR!!!
				$html_salida.= "</tr>\n";
			}

		} else {

			$mensaje="La tabla está vacia actualmente.";
			$tipo_mensaje="advertencia";
		}
		$reg=array();
		if(isset($campos)){
			foreach($campos as $campo=>$formato){
				if( !is_integer($campo) ) {
					$reg[$campo] = "";
				} else {
					$reg[$formato] = "";
				}
			}
		}
		$reg['codigo_tabla'] = $codigo_tabla;

		//se muestran una fila vacía al final para un nuevo registro .. jjy
		$html="";
		if($nuevo_registro){
			$html_campos_hidden="";
			$html= 	'<script>';
			$html.= "var reg_nuevo={$fila};";
			$html.= 'var tr_nuevo="';
			$html.=	"<tr class='registro_nuevo'><td></td>";

			foreach($reg as $campo=>$valor){
				$valor_campo_defecto="";
				$hay_campo_nuevo=isset($parametros[$campo.'_nuevo']);
				if($hay_campo_nuevo){
					$valor_campo_defecto=$parametros[$campo.'_nuevo'];
				} else {
					if(!isset($mostrar_col[$campo])){
						$valor_campo_defecto=$valor;
					}
				}
				if(isset($mostrar_col[$campo])) {
					$html.= "<td>"
						   ."<input class='celda_grid' id='{$campo}[]' name='{$campo}[]' type='text' class='ancho-full' value='{$valor_campo_defecto}'  rel='$fila'/>"
						   ."</td>";
				} else {
					$html_campos_hidden.= "<input id='{$campo}[]' name='{$campo}[]' type='hidden' class='ancho-full' value='{$valor_campo_defecto}'  rel='$fila'/>";

				}
			}
			$html_campos_hidden.="<input class='registro_editado' id='registro_editado[]' name='registro_editado[]' type='hidden' rel='$fila'/>";
			$html.= "<td class='invisible'>$html_campos_hidden</td>";
			$html.= "<td></td>";
			$html.= '</tr>";';
			$html.="\n</script>\n";
		}	

		$html_salida.= "</table>\n";
		$html_salida.= $html;
		$html_salida.=html_mensaje('',$mensaje,$tipo_mensaje);

		if($nuevo_registro){
			$html_salida.= "<a href='javascript:void(0);' class='link_registro_nuevo_grid' rel='$id'>Agregar registro nuevo</a>";
			$html_salida.= "&nbsp;<a href='javascript:void(0);' class='link_guardar_registro' rel='$id'>Guardar cambios</a>";
			$html_salida.= "&nbsp;<a href='javascript:void(0);' class='link_cancelar_edicion' rel='$id'>Cancelar</a>";
			$script="
				<script>
					$(document).ready(function(){
						$('#'+$(this).attr('rel')+' tr:even td input').addClass('par');
						$('.link_guardar_registro , .link_cancelar_edicion').hide();
						$('.link_cancelar_edicion').click(function(){
							$('#'+$(this).attr('rel')+' tr:last').remove();
							$('.link_guardar_registro[rel=\"'+$(this).attr('rel')+'\"], .link_cancelar_edicion[rel=\"'+$(this).attr('rel')+'\"]').hide();
							$('.link_registro_nuevo_grid[rel=\"'+$(this).attr('rel')+'\"]').show();
							reg_nuevo--;
						});
						$('.link_registro_nuevo_grid').click(function(){
							reg_nuevo++;
							$('#'+$(this).attr('rel')).append(tr_nuevo);
							$('#'+$(this).attr('rel')+' tr:last input').attr('rel',reg_nuevo);
							$('#'+$(this).attr('rel')+' tr:even td input').addClass('par');
							$(this).hide();
							$('.link_guardar_registro[rel=\"'+$(this).attr('rel')+'\"], .link_cancelar_edicion[rel=\"'+$(this).attr('rel')+'\"]').show();

							$('.celda_grid').change(function(){
								$('.registro_editado[rel=\"'+$(this).attr('rel')+'\"]').val('SI');
							});
						});
						$('.link_guardar_registro').click(function(){
							{$parametros['funcion_guardar']}();
						});
						$('.celda_grid').change(function(){
							$('.registro_editado[rel=\"'+$(this).attr('rel')+'\"]').val('SI');
						});
					});
				</script>";
			$html_salida.= $script;
		}

		return $html_salida;
	}

}

if ( ! function_exists( 'html_grid_simple_plus_divs' ) ){

	//crea un grid simple basado en html .. jjy !!!!2014!!
	function html_grid_simple_plus_divs ( $id, $arreglo_datos, $parametros=array() ) {

		$columnas           = array();
		$formato_columnas   = array(); //
		$ancho_columnas     = array(); //
		$ancho_grid					= '100%;';
		$alto_grid 					= '215px';
		$mensaje_automatico = true;

		extract( $parametros );

		//se hace una conversion del arreglo por si viene como obj_result de postgres .. jjy
		if( ! is_array( $arreglo_datos ) ) $arreglo_datos = ( array ) $arreglo_datos;

		$html_salida   = "";
		$html_acciones = "";

		if( count( $columnas ) == 0 ){
			$c = 0;
			foreach ($arreglo_datos[0] as $campo => $valor) {
				$columnas += array( $campo => ucfirst( str_replace('_', ' ', $campo ) ) );
				$c++;
			}
		}
		if( count( $ancho_columnas ) == 0 ){
			$c = 0;
			foreach ($arreglo_datos[0] as $campo => $valor) {
				$ancho_columnas += array( $campo => intval( 100 / count( $arreglo_datos[0] ) ) .'%' );
				$c++;
			}
		}

		if ( count( $columnas ) > 0 ){ 

			$html_salida = "<div style='width: ".$ancho_grid."'><div 
					style = 'display:block;' 
					id    = 'encabezados_$id' 
					name  = 'encabezados_$id' 
					class = 'html-grid html-grid_plus encabezado'
					><div 
						style = 'display:inline-block; position:relative; width: 100%;'
						>";

				$alineacion_texto = "alinear-izquierda"; //solo la primera columna

				foreach ( $columnas as $campo => $titulo_columna ){

					$ancho_definido = "";
					if ( isset( $ancho_columnas[ $campo ] ) ) {

						$ancho_definido = "width:". $ancho_columnas[ $campo ];
					}
					
					$html_salida .= "<div 
							style = 'position: relative; display: inline-block; " .$ancho_definido. "' 
							class = 'alinear-centro'
							>"
						. "<div 
										class = 'contenedor-celda-grid_plus ".$alineacion_texto. " '
										>"
						. '&nbsp;' . $titulo_columna
						. "</div>"
					. "</div>"; // Div que contiene los ENCABEZADOS fijos!!! .... jjy

					$alineacion_texto = ""; //solo la primera
				}

				$html_salida .= "</div>"
					."</div>"
					."<div 
							class = 'contenedor-grid contenedor-grid_plus area-desplazable'  
							style = 'border-top:0; height: ".$alto_grid."'
							>"; //DIV QUE HACE EL SCROLLING DEL CONTENIDO DEL GRID!!!!!!!!!!!!!!! ...jjy
		}

		$html_salida   .= "<div 
				style = 'display:block;' 
				id    = '$id' 
				name  = '$id' 
				class = 'html-grid html-grid_plus'
				>";

		$n_registros =count($arreglo_datos);
		$fila        =0;
		$n_campos    =0;

		if ( count ( $columnas ) == 0 && count( $arreglo_datos ) > 0 ) {

			if ( count ( $arreglo_datos ) == 0 ) {

				$columnas = array('error' => 'No se hasn definido columnas a mostrar y el arreglo de datos está vacío');

			} else {

				foreach ( $arreglo_datos[0] as $campo => $valor ) {

					$columnas[$campo] = $campo;

				}
			}
		}

		//  !!!! Se remplaza en la version plus!!!!!! por el div del contenedor scrolleable ... jjy
		if( count( $columnas ) > 0 ){

			foreach( $columnas as $campo => $titulo ){
				$mostrar_col[$campo]=$titulo;

				$n_campos++;
				if(isset($mostrar_col[$campo])) {
					$titulo=$mostrar_col[$campo];
				}
			}
		}
		
		$html="";
		$mensaje="";
		$tipo_mensaje="";

		if($n_registros>0){

			foreach($arreglo_datos as $reg){
				$fila++;

				$par_o_impar=(($fila%2)==0)?'par':'impar';
				$registro_editable = '';

				$html_campos_hidden="";
				$html_salida.= "<div 
						style = 'display:block; position:relative;' 
						class = 'fila-registro seleccionable {$registro_editable}'
						>";

				//MAGIA para reordenar los campos de acuerdo a las columnas suministradas!!!!
				// elegancia ... copiada de la web ! ... jjy
				$reg = array_replace( array_flip( array_keys( $mostrar_col ) ), $reg );				

				foreach($reg as $campo=>$valor){

					$ancho_definido = "";
					if ( isset( $ancho_columnas[ $campo ] ) ) {

						$ancho_definido = "width:". $ancho_columnas[ $campo ];

					}

					if( isset( $mostrar_col[$campo] ) ) {

						$html_salida.= "<div class='{$par_o_impar}'
								style = 'position: relative; display: inline-block;".$ancho_definido."'
								><div 
										class = 'contenedor-celda-grid_plus'
										s-tyle = '". $ancho_definido . "'
										>";

						if ( isset ( $campos[$campo] ) ) { //si viene con formato

							$valor = str_replace( '{@valor}', $valor, $campos[$campo] );

						}
						if ( isset ( $formato_columnas[$campo] ) ) { //si viene con formato

							$valor = $formato_columnas[$campo];

							foreach($reg as $campo_x => $valor_x){

								$valor = str_replace( '{@'.$campo_x.'}', $valor_x, $valor);

							}

						}
						/**
						 AQUI SE MUESTRA EL CONTENIDO DE CADA CELDA!!!!! ...jjy
						 **/

						 $valor = '<span title="'.$valor.'">'.$valor.'</span>';
						$html_salida.= '&nbsp;'.$valor; 

						$html_salida.= "</div>";

						//}

						$html_salida.= "</div>";

					} else {

						$html_campos_hidden.= "<input id='{$campo}[]' name='{$campo}[]' type='hidden' class='ancho-full' value='{$valor}' rel='$fila' />\n";

					}
				}

				$html_campos_hidden.="<input class='registro_editado' id='registro_editado[]' name='registro_editado[]' type='hidden' rel='$fila'/>";

				$html_salida.= '<div 
						class = "invisible"
						>'
						. $html_campos_hidden
						. '</div>';

				/*$html_salida.= '
				<div 
						style = "display:table-cell"
						class = "alinear-centro" 
						style = "width:15px; padding:0px !important;"
						>'
						. $html_acciones 
						.
				'</div>';//REVISAR!!!*/
				
				$html_salida.= "
				</div>\n";
			}

		} else {

			if ( $mensaje_automatico ){ // se recibe por parámetro ... jjy

				$mensaje="La tabla está vacia actualmente.";
				$tipo_mensaje="advertencia";

			}
		}
		$reg=array();

		if(isset($campos)){

			foreach($campos as $campo=>$formato){

				if( !is_integer($campo) ) {

					$reg[$campo] = "";

				} else {

					$reg[$formato] = "";

				}
			}
		}

		//se muestran una fila vacía al final para un nuevo registro .. jjy
		$html="";

		$html_salida.= "</div>&nbsp;<br>\n";
		
		$html_salida.= "</div>\n"; // en prueba ....
		
		$html_salida.= $html;
		
		$html_salida.=html_mensaje('',$mensaje,$tipo_mensaje);

		return $html_salida;
	}

}










if ( ! function_exists( 'html_grid_simple_plus_divs_prueba' ) ){ /// adaptación y mejoras por CARLOS VILORIA ... ! v2

	//crea un grid simple basado en html .. jjy !!!!2014!!
	function html_grid_simple_plus_divs_prueba ( $id, $arreglo_datos, $parametros=array() ) {

		$columnas         = array();
		$formato_columnas = array(); //
		$ancho_columnas   = array(); //
		$ancho_encabezado = '';
		$tipo_columna     = array();
		$mostrar_acciones = '';
		$altura_grid      = '215px';

		extract( $parametros );

		if($mostrar_acciones){
			$columnas['accion']='Acciones';
			$ancho_columnas['accion']='60px';
		}
		//prp($columnas);
		//prp($ancho_columnas,1);
		//se hace una conversion del arreglo por si viene como obj_result de postgres .. jjy
		if( ! is_array( $arreglo_datos ) ) $arreglo_datos = ( array ) $arreglo_datos;
		if( count($ancho_columnas) > 0 ){//se pregunta si viene el ancho_columnas para sumar los ancho y poner el ancho del div de manera correcta ...cv.
			$suma='';
			$conteo=0;//variable que sirve para saber cuantas columnas tengo y mutiplicarlo por 6  para agregrales los pinceles del padding
			foreach ($ancho_columnas as $ancho) {
				$suma='';
				$aux=str_split($ancho);
				foreach ($aux as $numero) {
					if(is_numeric($numero)){
						$suma.=$numero;
					}
				}
				$sumatoria[]=$suma;
				$conteo++;
			}			
			foreach ($sumatoria as $ancho) {
				$ancho_encabezado=$ancho_encabezado+$ancho;
			}
			//prp($conteo,1);
			$conteo=($conteo*6)+3;
			$ancho_encabezado=$ancho_encabezado+$conteo;
		}
		//prp($ancho_encabezado,1);

		$html_salida   = "";
		$html_acciones = "";

		if ( count( $columnas ) > 0 ){ 
			
			if($ancho_encabezado!='')$ancho_encabezado=$ancho_encabezado.'px';//esto es para asegurarnos que el ancho del encabezado se ajuste atumaticamente ...cv
				
				
			$html_salida = "
			
			<div 
					style = 'display:block; width:".$ancho_encabezado.";' 
					id    = 'encabezados_$id' 
					name  = 'encabezados_$id' 
					class = 'html-grid html-grid_plus encabezado_uno'
					>

				<div 
						style = 'display:table-row; '
						>

						<!--div 
								style = 'display:table-caption'
								>
						</div--> \n";

				$alineacion_texto = "alinear-izquierda"; //solo la primera columna

				foreach ( $columnas as $campo => $titulo_columna ){

					$ancho_definido = "";
					if ( isset( $ancho_columnas[ $campo ] ) ) {

						$ancho_definido = "width:". $ancho_columnas[ $campo ];
					}
					
					$html_salida .= "
					<div 
							style = 'display:table-cell;" .$ancho_definido. "' 
							class = 'alinear-centro'
							>"
						. "<div 
										class = 'contenedor-celda-grid_plus ".$alineacion_texto. "'
										style = '". $ancho_definido . "; '
										>			
					".$titulo_columna
						. "</div>"					
					. "</div>"; // Div que contiene los ENCABEZADOS fijos!!! .... jjy
					
					$alineacion_texto = ""; //solo la primera
				}

				$html_salida .= "
						</div>"
					."
						</div>"
					."
						<div 
							class = 'contenedor-grid contenedor-grid_plus' 
							style = 'border-top:0; height: ".$altura_grid."; width:".$ancho_encabezado."'
							>\n"; //DIV QUE HACE EL SCROLLING DEL CONTENIDO DEL GRID!!!!!!!!!!!!!!! ...jjy
		}

		$html_salida   .= "
		<div 
				style = 'display:block;' 
				id    = '$id' 
				name  = '$id' 
				class = 'html-grid html-grid_plus'
				>";

		$n_registros =count($arreglo_datos);
		$fila        =0;
		$n_campos    =0;

		if ( count ( $columnas ) == 0 && count( $arreglo_datos ) > 0 ) {

			if ( count ( $arreglo_datos ) == 0 ) {

				$columnas = array('error' => 'No se han definido columnas a mostrar y el arreglo de datos está vacío');

			} else {

				foreach ( $arreglo_datos[0] as $campo => $valor ) {

					$columnas[$campo] = $campo;

				}
			}
		}

		//  !!!! Se remplaza en la version plus!!!!!! por el div del contenedor scrolleable ... jjy
		if( count( $columnas ) > 0 ){

			foreach( $columnas as $campo => $titulo ){
				$mostrar_col[$campo]=$titulo;

				$n_campos++;
				if(isset($mostrar_col[$campo])) {
					$titulo=$mostrar_col[$campo];
				}
			}
		}
		
		$html="";
		$mensaje="";
		$tipo_mensaje="";

		if($n_registros>0){

			foreach($arreglo_datos as $reg){
				$fila++;

				$par_o_impar=(($fila%2)==0)?'par':'impar';
				$registro_editable = '';

				$html_campos_hidden="";
				$html_salida.= "
				<div  id='row_".$fila."'
						style = 'display:table-row' 
						class = 'fila-registro seleccionable {$par_o_impar} {$registro_editable}'
						>";

				/*$html_salida.= "
				<div 
						style = 'display:table-cell' 
						class = 'alinear-abajo sin-padding'
						>
				</div>";*/
				//prp($reg);
				foreach($reg as $campo=>$valor){

					$ancho_definido = "";
					if ( isset( $ancho_columnas[ $campo ] ) ) {

						$ancho_definido = "width:". $ancho_columnas[ $campo ];

					}

					if( isset( $mostrar_col[$campo] ) ) {

						$html_salida.= "\n
						<div 
								style = 'display:table-cell;'
								>
								<div 
										class = 'contenedor-celda-grid_plus'
										style = '". $ancho_definido . "'
										>";

						if ( isset ( $campos[$campo] ) ) { //si viene con formato

							$valor = str_replace( '{@valor}', $valor, $campos[$campo] );

						}
						if ( isset ( $formato_columnas[$campo] ) ) { //si viene con formato

							$valor = $formato_columnas[$campo];

							foreach($reg as $campo_x => $valor_x){

								$valor = str_replace( '{@'.$campo_x.'}', $valor_x, $valor);

							}

						}
						/**
						 AQUI SE MUESTRA EL CONTENIDO DE CADA CELDA!!!!! ...jjy
						 **/
						if(isset($tipo_columna[$campo])){
							extract($tipo_columna[$campo]);
							
							//prp($campo_oculto,1);
							$parametros_input['valor_inicial']=$valor;

							$parametros_input['clases_adicionales']=$campo.'_'.$fila;
							if($tipo_input=='texto_funcion_especial'){
								
							$parametros_input['funciones_especiales'][0][2]['enlace']='javascript:mostrar_dialogo_grid("'.$campo.'_'.$fila.'","'.$tabla_asociada_dialogo.'")';
							}
							//prp($parametros_input);
							$html_salida .=html_input( $campo.'[]', $tipo_input, $parametros_input );
							



							if(isset($campo_oculto) && $campo_oculto){
								$parametros=array(
									'valor_inicial'=>'',
									'parametros_html'=>'rel="'.$campo.'_'.$fila.'"',
									);
								//aqui verificamos si en el arreglo de datos existe un valor para el campo oculto;
								if(isset($reg["campo_oculto_".$campo.""]))$parametros['valor_inicial']=$reg["campo_oculto_".$campo.""];
								//prp($parametros,1);
								$campo_oculto=false;
								$html_salida.=html_input('oculto_'.$campo.'[]','oculto',$parametros);
							}
							$html_salida.= "
							</div>";
							$html_salida.= "
							</div>";
						}else{
							
							$html_salida.= $valor; 

							$html_salida.= "
							</div>";
							$html_salida.= "
							</div>";
						}
						//}

						

					} else {

						$html_campos_hidden.= "<input id='{$campo}[]' name='{$campo}[]' type='hidden' class='ancho-full' value='{$valor}' rel='$fila' />\n";

					}
				}

				$html_campos_hidden.="<input class='registro_editado' id='registro_editado[]' name='registro_editado[]' type='hidden' rel='$fila'/>";
				
				if($mostrar_acciones && count( $arreglo_datos ) > 0 ){// condicion para cuando vienen las acciones tru agregar al grid los botones
						/*$parametros = array(
				            'enlace'           => 'javascript:mas_grid();',		            
				            'icono'            => 'fa fa-expand-alt',
				            //'estilo_adicional' => 'width:5px; height:10px;',
				            //'descripcion'      => 'Guardar',
				            'clase_adicional'  => 'boton_mas_grid',
				            );*/
					$boton_mas='<a id="'.$fila.'"  name="'.$fila.'"  href="javascript:mas_grid();" title=""  class="boton_mas_grid fa fa-expand-alt"></a>';

				        //$boton_mas= html_enlace_boton($fila, $parametros );
				       
					$boton_menos='<a id="'.$fila.'"  name="'.$fila.'" href="javascript:menos_grid();" title="" class="fa fa-collapse-alt boton_menos_grid" ></a>';
						$html_salida.= '
						<div id="row_'.$fila.'" name="'.$id.'" style="display:table-cell; width:59px" >
						'.$boton_mas.' '.$boton_menos.'
						</div>';

						/*$html_salida.= '
						<div 
								style = "display:table-cell"
								class = "alinear-centro" 
								style = "width:15px; padding:0px !important;"
								>'
								. $html_acciones 
								.
						'</div>';//REVISAR!!!*/
						
					$html_salida.= "
					</div>\n";
				}else{
					$html_salida.= "
					</div>\n";
				}
			}

		} else {
			
				$html_salida.= "
				<div  id='row_1'
						style = 'display:table-row'
						class = 'fila-registro seleccionable impar'

						
						>";
			if($mostrar_acciones && count($tipo_columna > 0) ){
				foreach ($columnas as $nombre_campo=> $columnas) {
					if($nombre_campo==='accion')$ancho_columnas[$nombre_campo]='59px';
						
					$html_salida.= "\n
					<div 
							style = 'display:table-cell;'
							>
							<div 
									class = 'contenedor-celda-grid_plus'
									style = 'width:". $ancho_columnas[$nombre_campo]. ";'
									>";

					if(isset($tipo_columna[$nombre_campo])){
								extract($tipo_columna[$nombre_campo]);
								//prp($parametros_input);
								//prp($campo_oculto,1);

								$parametros_input['valor_inicial']='';
								$html_salida .=html_input( $nombre_campo.'[]', $tipo_input, $parametros_input );

								if(isset($campo_oculto) && $campo_oculto){
									$campo_oculto=false;
									$html_salida.=html_input('oculto_'.$nombre_campo.'[]','oculto');
								}
								$html_salida.= "
								</div>";
								$html_salida.= "
								</div>";

					}
				}
			}

				$boton_mas='<a id="1"  name="1"  href="javascript:mas_grid();" title=""  class="boton_mas_grid fa fa-expand-alt"></a>';

				        //$boton_mas= html_enlace_boton($fila, $parametros );
				       
					$boton_menos='<a id="1"  name="1" href="javascript:menos_grid();" title="" class="fa fa-collapse-alt boton_menos_grid" ></a>';
						$html_salida.= '
						<div id="row_1" name="'.$id.'" style="display:table-cell; width:59px;" >
						'.$boton_mas.' '.$boton_menos.'
						</div>';

						/*$html_salida.= '
						<div 
								style = "display:table-cell"
								class = "alinear-centro" 
								style = "width:15px; padding:0px !important;"
								>'
								. $html_acciones 
								.
						'</div>';//REVISAR!!!*/
						
					$html_salida.= "
					</div>\n";

			$mensaje="La tabla está vacia actualmente.";
			$tipo_mensaje="advertencia";
		}
		$reg=array();

		if(isset($campos)){

			foreach($campos as $campo=>$formato){

				if( !is_integer($campo) ) {

					$reg[$campo] = "";

				} else {

					$reg[$formato] = "";

				}
			}
		}

		//se muestran una fila vacía al final para un nuevo registro .. jjy
		$html="";

		$html_salida.= "</div></div></div>&nbsp;<br>\n";
		
		$html_salida.= "</div>\n"; // en prueba ....
		
		$html_salida.= $html;
		
		$html_salida.=html_mensaje('',$mensaje,$tipo_mensaje);

		return $html_salida;
	}

}





/** 

 Version PRO - Avanzada del Grid_simple_plus_divs! que permite el uso de componentes como celdas ... v2 jjy

 **/
if ( ! function_exists( 'html_grid_avanzado' ) ){

	require( $GLOBALS['system_path'].'helpers/componentes_osti/componente_html_grid_avanzado.php' );

}
/**

FIN grid Pro .... jjy v2

**/










if(!function_exists('html_mensaje')){

	function html_mensaje($id="",$mensaje="", $tipo="amarillo", $parametros=array()){
		$salida="";
		$tipo_bs = array( // ojo ... los alerts de bootstrap no me convencen! ... v2 jjy .. descartados por ahora
			'amarillo'    => 'warning',
			'advertencia' => 'warning',
			'azul'        => 'info',
			'info'        => 'info',
			'error'       => 'danger',
			'alerta'      => 'danger',
			'rojo'        => 'danger',
			'verde'       => 'success',
			'exito'       => 'success',
			'ok'       		=> 'success',
		);
		if($mensaje!=""){
			$salida='
			<div id="'.$id.'" name="'.$id.'" ' //	class="alert alert-'. $tipo_bs[ $tipo ] .' alert-dismissable 
				.'class="mensaje X-mensaje-'.$tipo.' alert-'. $tipo_bs[ $tipo ].' ">'
				.$mensaje.'
				<!--button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button-->
			</div>
			<script>
				setTimeout("desvanecer_mensaje()",3000);
				function desvanecer_mensaje(){$(".mensaje").fadeOut("fast");}
			</script>';
		}
		return $salida;
	}
}

if (!function_exists('html_pestanas')){

	function html_pestanas($id="", $parametros=array()){
		$deshabilitar_otras = FALSE;
		$html_salida     = "";
		$clases          = (isset($parametros['clases-pestanas']) && $parametros['clases-pestanas']!="")?$parametros['clases-pestanas']:"";
		$estilos         = (isset($parametros['estilo-pestanas']) && $parametros['estilo-pestanas']!="")?$parametros['estilo-pestanas']:"";
		$estilo_general  = (isset($parametros['estilo-general']) && $parametros['estilo-general']!="")?$parametros['estilo-general']:"";
		
		extract( $parametros );

		$seccion_activa  = $parametros['id_pestana_activa'];

		foreach($parametros['pestanas'] as $seccion => $titulo){
			eval('$tab_'.$seccion.' = "";');
		}
		eval('$tab_'.$seccion_activa.' = "active";');

		$html_salida.='<div id="'.$id.'" name="'.$id.'" class="X-contenedor-pestanas">
				<ul class="nav nav-tabs sm X-pestanas ancho-full" style="'.$estilo_general.'">
					&nbsp;';
					
					$estilo_id_tab = "";
					foreach ( $parametros['pestanas'] as $id_tab => $titulo_tab ) {
						
						eval ( '$estilo_id_tab = $tab_'.$id_tab.';' );

						$html_salida.='<li class="tab '.$clases.' '.$estilo_id_tab.'" style=" '.$estilos.' ">';
						
						if ( ! $deshabilitar_otras === TRUE OR $id_tab == 'inicio' ){
							$html_salida.='<a href="';
							$html_salida.=( ( isset ( $parametros ['enlaces'][$id_tab] ) ) 
									? $parametros ['enlaces'][$id_tab] 
									: "javascript:void(0);"
							);
							$html_salida.='" rel="'.$id_tab.'">';
						}

						$html_salida .= $titulo_tab;
						
						if ( ! $deshabilitar_otras === TRUE OR $id_tab == 'inicio' ){
							$html_salida .= '</a>';
						}

						$html_salida .= "</li>\n";
					}
		$html_salida.='</ul></div>';

		$html_salida .=	"\n
			<script>
				$(document).ready(function(){
					$('.pestanas li.tab a, .nav-tabs li.tab a').click(function(){
						$('.pestanas li.activo, .nav-tabs li.active').removeClass('activo active');
						$(this).parent().addClass('activo active');

						$('div.tab_activo').addClass('invisible').removeClass('tab_activo');
						$('div#'+$(this).attr('rel')).removeClass('invisible');
						$('div#'+$(this).attr('rel')).addClass('tab_activo');
					});
				});
			</script>
			\n";
		return $html_salida;
	}
}

if(!function_exists('html_lista_segun_tabla')){
	
	function html_lista_segun_tabla($id, $parametros=array()){
		//Parámetros esperados ... jjy
		$clases="";
		$estilos="";
		$texto_por_defecto="-- Seleccione --";
		$valor_por_defecto="0";
		$texto_seleccionado="";
		$valor_seleccionado="";

		// los parametros correctos remplazaran a los inicializados en "" .. jjy
		extract($parametros);

		$CI =& get_instance();
		$CI->load->model('comun/comun_m');

		$datos=$CI->comun_m->valores_lista_segun_tabla($parametros);

        $html_salida="<select class='$clases' style='$estilos' id='$id' name='$id'>\n";
        	if($texto_por_defecto!=""){
		        $html_sub1="<option value='$valor_por_defecto' ";
		        if($valor_por_defecto==$valor_seleccionado){
		        	$html_sub1.=" selected='selected' ";
		        }
		        $html_sub2=">$texto_por_defecto</option>";
		        $html_salida.=$html_sub1.$html_sub2;
		    }

        foreach ($datos['datos'] as $reg) {
			foreach($reg as $campo=>$valor){
                if($campo==$campo_lista_valor){
                    $html_sub1="<option value='$valor' ";
					if($valor_seleccionado!="" && $valor==$valor_seleccionado){
			        	$html_sub1.=" selected='selected' ";
			        }
                }
		         //|| ($texto_seleccionado!="" && $valor==$texto_seleccionado))
                if($campo==$campo_lista_texto){
                	if($texto_seleccionado!="" && $valor==$texto_seleccionado){
		        		$html_sub1.=" selected='selected' ";
		        	}
                    $html_sub2=">$valor</option>\n";
                }
            }
            $html_salida.=$html_sub1.$html_sub2;
        }
        $html_salida.="</select>\n";

        return $html_salida;
	}
}

if( ! function_exists('html_calculadora_simple')){
	// más simple .... IMPOSIBLE! ... fuente: http://www.javascriptkit.com/script/cut18.shtml ... adaptación: jjy
	function html_calculadora_simple( $visible = TRUE ){
		$clase_visible = ( $visible ) ? '' : 'invisible';
		$html =<<< fin
			<!-- CALCULADORA SIMPLE .. de apoyo al componente que use la funcion especial de calculadora ... !! ... jjy -->
			<!-- ----------------------------------- -->
			<style type="text/css">
				.calculadora-simple { border: 1px solid #ccc; position:absolute; z-index: 100; padding:5px; background-color: white !important;box-shadow: 3px 3px 20px rgba(0, 0, 0, 0.25);}
				.calculadora-simple input#i { width: 100px; }
				.calculadora-simple .b_num { width: 25px; cursor: pointer; }
				.calculadora-simple .b_num.b_ok_calc { width: 52px; }
				.calculadora-simple table, .calculadora-simple table td {	border: 0; background-color: white !important; }
				.calculadora-simple table td { align: center !important; }
			</style>
			<script type="text/javascript">
				function c_a(c)   { v = $('#i').val(); if(c!='.' || (c=='.' && ( v.indexOf('.')==-1) || v.substr(-1)!='.') ){ $('#i').val(v+c); } } 
				function rt_calc(){ $('#'+$('.calculadora-simple #txt_destino').val() ).val( c.i.value ); x_calc(); }
				function x_calc() { $('.calculadora-simple').addClass('invisible'); }
			</script>
			<!-- ----------------------------------- -->
			<div class="calculadora-simple
					 $clase_visible 
			">
			  <form name="c" id="c">
			  <table border=4>
				  <tr>
					  <td align="center">
						  <input placeholder="0.00" type="text" id="i" name="i">
					  </td>
					  </tr>
					  <tr>
					  <td align="center">
						  <button type="button" class='b_num' rel="1" onclick="c_a('1')">1</button>
							<button type="button" class='b_num' rel="2" onclick="c_a('2')">2</button>
							<button type="button" class='b_num' rel="3" onclick="c_a('3')">3</button>
							<button type="button" class='b_num' rel="+" onclick="c_a(' + ')">+</button>
							<br>
							<button type="button" class='b_num' rel="4" onclick="c_a('4')">4</button>
							<button type="button" class='b_num' rel="5" onclick="c_a('5')">5</button>
							<button type="button" class='b_num' rel="6" onclick="c_a('6')">6</button>
							<button type="button" class='b_num' rel="-" onclick="c_a(' - ')">-</button>
							<br>
							<button type="button" class='b_num' rel="7" onclick="c_a('7')">7</button>
							<button type="button" class='b_num' rel="8" onclick="c_a('8')">8</button>
							<button type="button" class='b_num' rel="9" onclick="c_a('9')">9</button>
							<button type="button" class='b_num' rel="x" onclick="c_a(' * ')">x</button>
							<br>
							<button type="button" class='b_num' rel="." onclick="c_a('.')">.</button>
							<button type="button" class='b_num' rel="0" onclick="c_a('0')">0</button>
							<button type="button" class='b_num' rel="=" onclick="$('#i').val( (eval($('#i').val())).toFixed(2) )">=</button>
							<button type="button" class='b_num' rel="/" onclick="c_a(' / ')">/</button>
						  <br>
						  <button type="button" class='b_num' rel="c" onclick="$('#i').val('')">c</button>
							<button type="button" class='b_num b_ok_calc' rel="ok" onclick="rt_calc()">ok</button>
							<button type="button" class='b_num' onclick="x_calc()"><i class="fa fa-share fa fa-flip-horizontal"></i></button>
						  <br>
						  <input type="hidden" name="txt_destino" id="txt_destino" value=""/>
					  </td>
				  </tr>
			  </table>
			  </form>
			</div>
			<!-- ----------------------------------- -->
			<script>
				function calculadora( id_obj_input ){
					if( $('.calculadora-simple').hasClass('invisible') ){
	    			var m_orig = $('#'+id_obj_input).val();
	    			$('.calculadora-simple #i').val( ( m_orig == 0 ) ? '' : m_orig );
	  				var obj_input = $('#'+id_obj_input)[0];
	 					var left = $(obj_input).offset().left;
	 					var top = $(obj_input).offset().top + $(obj_input).height() + 8;
	 					$('.calculadora-simple').css({'top' : top+'px', 'left': left});
	   				$('.calculadora-simple #txt_destino').val( id_obj_input );
	   				$('.calculadora-simple').removeClass('invisible');
	   			} else {
	   				$('.calculadora-simple').addClass('invisible');
	   			}
	   		}
   		</script>
fin;
		return $html;
	}
}


/**
 
 	COMPONENTES 2.0 ----- Feb 2014 ... jjy  v2

**/

if( ! function_exists( 'html_bs_menu' ) ){
	// botón basado en Boostrap 3.1.1 ... para componentes 2.0 ... jjy
	// también en FontAwesome 4.0.1
	function html_bs_menu( $id = '', $parametros = array() ){
		$html_salida = "";

		$pestanas         = array();
		$enlaces_pestanas = array();
		$id_pestana_activa = 'inicio';

		extract( $parametros );

		foreach ( $pestanas as $id_pestana => $titulo_pestana ) {
			$activo = "";
			if ( $id_pestana == $id_pestana_activa ){
				$activo = " class='active' ";
			}
			$html_salida .= '<li '.$activo.' id="'.$id_pestana.'" name="'.$id_pestana.'">';
			$html_salida .= '<a ';
			$html_salida .= 'href="'.$enlaces[ $id_pestana ];
			$html_salida .= '">';
			$html_salida .= $titulo_pestana;
			$html_salida .= '</a></li>';
		}

		return $html_salida;
	}
}

if( ! function_exists( 'html_bs_boton' ) ){
	// botón basado en Boostrap 3.1.1 ... para componentes 2.0 ... jjy
	// también en FontAwesome 4.0.1
	function html_bs_boton( $id = '', $parametros = array(), &$html_complemento_salida = '' ){
		//# @parametro array $parametros Un arreglo asociativo parametro => valor, con algunos o varios de los siguientes: >>//
		$apariencia               = 'normal'; // .. sombra-2d
		$efecto_sombra            = ''; // .. '' ó 'traslucida'
		$color_principal          = 'gris-claro';
		$clases_adicionales_icono = '';
		$estilos_adicionales_icono = '';
		$distancia_sombra         = '40';
		$inclinacion_sombra       = '1';
		$clases_adicionales_link  = '';
		$estilos                  = '';
		$estilos_adicionales      = '';
		$clases                   = '';
		$clases_adicionales       = '';
		$parametros_html          = '';
		$texto                    = 'Bot&oacute;n';
		$descripcion              = '';
		$enlace                   = array();
		$tipo                     = 'button';
		$icono                    = '';
		//<<
		extract( $parametros );

		//retro-compatibilidad ... jjy v2
		if ( trim( $texto ) == 'Bot&oacute;n' && trim( $descripcion ) != '' ) $texto = $descripcion;

		$html_salida  = '';
		$este_enlace  = '';
		$enlace_destino = '';

		switch ( $apariencia ) {

			case 'sombra-2d':
			case 'sombra-3d':
				$clases_adicionales .= ' '.$color_principal.' ';
				$clases = ' boton sombra_larga ';
				if ( $texto == 'Bot&oacute;n' ) $texto = '';

				if( count( $enlace ) > 0 ){
					$este_enlace  = ( isset( $enlace[0] ) ) ? $enlace[0] : "javascript:void(0);";
					$este_destino = ( isset( $enlace[1] ) ) ? $enlace[1] : "_self";
					$html_salida .= "<a title='".$descripcion."' "
												. " href='".$este_enlace."' "
												. " target='".$este_destino."' "
												. " class='".$clases_adicionales_link." seguido' "
												. " style='".$estilos_adicionales."' "
												." >";
				}

				$html_salida .= " "
					."<div class='".$clases." ".$clases_adicionales."' 
								 style='border-radius:".$redondear_esquinas."; '>";

				if( strpos( $icono, "fa-" ) !== FALSE ){
					$html_salida .= "<i class='fa $icono'></i>";
				} else {
					$html_salida .= "<img src='$icono'/>";
				}

				$html_salida .= "</div>"
					."<h4>$texto</h4>";
					if( trim( $descripcion ) != "" && trim( $descripcion ) != trim( $texto ) ) {
						$html_salida .= " <label>$descripcion</label>";
					}
				if( count( $enlace ) > 0 ){
					$html_salida .= "</a>";
				}

				if( strpos($html_complemento_salida, "('.sombra_larga i.fa').each(" ) === false ) {

					$html_complemento_salida .= " "
						."<script> 
							\$(document).ready(function(){
							  \$('.sombra_larga i.fa').each( function(){
						      \$(this).wrap('<div class=\"interno\"></div>');
						      \$fa_c = $(this).clone();
						      for(var i = 1; i < ".intval($distancia_sombra)."; i++){
						          \$fa_c = \$fa_c.clone()
						              .css({'left':i+'px','top':(i*".$inclinacion_sombra.")+'px'})
						              .addClass('sombra".((trim($efecto_sombra)!="")?"-".$efecto_sombra:"")."')
													.appendTo( $(this).closest('.interno') );
						      }";
									if( substr(trim($apariencia),-2)=="3d" ){
										$html_complemento_salida .= " 
											\$(this).closest('.boton.sombra_larga').addClass('tres_d');";
									}
									$html_complemento_salida .= " "
							."});";

							if( strpos( $icono, "fa-" ) === FALSE ){
								$html_complemento_salida .= " 
									\$('.sombra_larga img').parent().css('padding','0');";
							}
							$html_complemento_salida .= " 

						});
						</script>\n";

					if( $estilos_adicionales_icono != '' ){
					
						$html_complemento_salida .= " "
							."<style>
			  				#".$id.".sombra_larga i.fa:first-child{
			      			".$estilos_adicionales_icono.";
			  				}
								</style>"
							."\n";
					}
				}
				break;

			case 'normal':
			default:

				if( trim( $clases ) == '' ) $clases = 'btn';
				if( trim( $clases_adicionales ) == '' ) $clases_adicionales = 'btn-default' ;	
				if( count( $enlace ) > 0 ){
					$este_enlace  = ( isset( $enlace[0] ) ) ? $enlace[0] : "javascript:void(0);";
					$este_destino = ( isset( $enlace[1] ) ) ? $enlace[1] : "document";
				}

				$destino = ( is_array( $enlace ) ) ? ( ( isset( $enlace[1] ) ) ? $enlace[1] : 'document' ) : 'document' ;
				$enlace  = ( is_array( $enlace ) ) ? ( ( isset( $enlace[0] ) ) ? $enlace[0] : '' ): $enlace;

				$onclick = "onclick=\"".$enlace."\"";
				if( strpos( $enlace, 'javascript:' ) === FALSE ){
					$onclick = "onclick=\"javascript:".$destino.".location = '".$enlace."';\" ";
					// REVISAR ESTA FORMA DE HACER EL ENLACE !!!! .... esta rebuscada! ... jjy v2
				}


				$html_salida = " "
					."<button 
							".$onclick."
							class='$clases $clases_adicionales'
							type='$tipo'
					>"
					.( ( trim( $icono ) == "" ) ? "" : "<i class='fa $icono'></i> &nbsp;" )
					.$texto
					."</button>";

		}

		return $html_salida;
	}
}

if ( ! function_exists('html_preparar_dialogo_emergente') ){
	function html_preparar_dialogo_emergente ( $parametros, &$html_salida ) {
		if( strpos($html_salida, "function mostrar_dialogo_emergente(" ) === false ) {
			$html_salida .= "".'
			<script>
			  function mostrar_dialogo_emergente( tipo_dialogo, id_objeto_resultado ){
			  	var id_contenedor_dialogo = "contenedor_dialogo_ajax";
				  $("#" + id_contenedor_dialogo).html("");
				  $(".velo-blanco").removeClass("invisible");
			    url_ajax = "'.site_url().'/../s/mostrar_dialogo_emergente/" + tipo_dialogo+"/"+id_objeto_resultado;
				  $.ajax({
				    url: url_ajax, 
				    context: document.body,
				  }).done (
				    function( data ){
				      //document.getElementById( $( "#"+id_contenedor_dialogo+" .contenido" )[0] ).innerHTML = data;
				      $( "#"+id_contenedor_dialogo+" " )[0].innerHTML=data;
				      $("#" + id_contenedor_dialogo).removeClass("invisible");
				    }
				  );
			  }
				function cerrar_velo(){
				  $("#dialogo, #contenedor_dialogo_ajax .contenido").html("");
				  $(".velo-blanco").addClass("invisible");
				}
			</script>'."\n";
		}
		if( strpos( $html_salida, '<div id="contenedor_dialogo_ajax"' ) === FALSE ){

			if( strpos( $html_salida, '<div class="velo-blanco invisible">' ) !== FALSE ) { // ya tiene velo-blanco
				$html_salida = str_replace( '<div class="velo-blanco invisible">',
					'<div class="velo-blanco invisible">
					  <div id="contenedor_dialogo_ajax" name="contenedor_dialogo_ajax" class="invisible">'
					  . '<div class="contenido"><div class="icono-espera"><i class="discreto fa fa-sipn fa-cog"></i></div></div>
					  </div>',
				$html_salida);
			} else {
				$html_salida .= '<div class="velo-blanco invisible">
					  <div id="contenedor_dialogo_ajax" name="contenedor_dialogo_ajax" class="invisible">'
					  . '<div class="contenido"><div class="icono-espera"><i class="discreto fa fa-sipn fa-cog"></i></div></div>
					  </div>
					</div>';
			}
		}
	}
}

if ( ! function_exists('html_preparar_popup') ){ // ...... jjy v2 ... funcion nueva!
	function html_preparar_popup ( &$html_salida ) {
		if( strpos($html_salida, "function mostrar_popup(" ) === false ) {
			$html_salida .= "".'
			<script>
				function mostrar_popup( parametros ){
			    if( typeof parametros == "string"){
			      alert( parametros ); 
			      return 0; // solo para verificacion! ... corregir e integrar ... jjy v2
			    }
			    var titulo 	= ( parametros["titulo"] !==\'undefined\' ) ? parametros["titulo"] : "_titulo";
			    var texto  	= ( parametros["texto"] !==\'undefined\' ) ? parametros["texto"] : "_texto";
			    var tipo 		= ( parametros["tipo"] !==\'undefined\' ) ? parametros["tipo"] : "informacion";
			    var icono 	= ( parametros["icono"] !==\'undefined\' ) ? parametros["icono"] : "fa-info-circle";
			    var botones = ( parametros["botones"] !==\'undefined\' ) ? parametros["botones"] : "aceptar";
			    var ancho  	= ( parametros["ancho"] !==\'undefined\' ) ? parametros["ancho"] : "300";
			    var alto  	= ( parametros["alto"] !==\'undefined\' ) ? parametros["alto"] : "250";
			    var boton_cerrar = ( parametros["boton_cerrar"] !==\'undefined\' ) ? parametros["boton_cerrar"] : "true";
			    
			    var html_popup = ""
			                   + "<h2>"+titulo+"</h2>"
			                   + ( ( boton_cerrar ) ? "<a href=\'javascript:cerrar_velo();\' class=\'cerrar_dialogo\'><i class=\'fa fa-share-square-o fa-flip-horizontal\'></i></a>" : "" )
			                   + "<hr>"
			                   + "<table width=\'100%\'><tr>"
			                   + "<td><i class=\'simbolo_popup fa "+icono+" fa-3x\'></i></td>"
			                   + "<td style=\'white-space:normal;\'><p class=\'texto_popup\'>"+texto+"</p></td>"
			                   + "</tr></table>"
			                   + "<div class=\'pie_dialogo\'>"
			                   + "<button class=\'btn btn-sm btn-primary\'><i class=\'fa fa-check fa-lg\'></i> &nbsp;Aceptar</button>"
			                   + "'.html_sangria('5px').'"
			                   + "<button class=\'btn btn-default btn-sm\'><i class=\'fa fa-reply fa-lg\'></i> &nbsp;Cancelar</button>"
			                   + "</div>";
			    $("#contenedor_dialogo").html("").append(html_popup).css({\'width\':ancho+\'px\',\'height\':alto+\'px\'});
			    $(".velo-blanco").removeClass("invisible");
			    $("#contenedor_dialogo").removeClass("invisible");
			  }
				function cerrar_velo(){
				  $("#dialogo, #contenedor_dialogo_ajax .contenido").html("");
				  $(".velo-blanco").addClass("invisible");
				}
			</script>'."\n";
			//if( strpos($html_salida, '<div class="velo-blanco invisible">' ) === false ) {
				 $html_salida .= '
					<div class="velo-blanco invisible">
					  <div id="contenedor_dialogo" name="contenedor_dialogo" class="invisible">
					    <div class="icono-espera"><i class="fa fa-spin fa-repeat fa-2x"></i></div>
					  </div>
					</div>
				';
			//}
		}
	}
}
if ( ! function_exists('html_simple_calendario') ){ // ...... jjy v2 ... funcion nueva!
	function html_simple_calendario( $parametros=array(), &$html_scripts="" ){
	  $fecha           = date("Y-m-d");
	  $tags_contenedor = array("<div>","</div>");
	  $enlace          = array( 'javascript:alert("{@fecha_dia}");' );
	  $destino         = "";
	  $formato         = "d-m-Y";
	  $valor_inicial   = $fecha;

	  extract( $parametros );

	  if( isset( $enlace[1] ) ) $destino = $enlace[1];
	  if( isset( $enlace[0] ) ) $enlace  = $enlace[0];
	  $esta_fecha = $fecha;
	  $meses           = array( '','enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre' );
	  $dias            = array( 'lunes','martes','miércoles','jueves','viernes','sábado', 'domingo' );
	  $dias_abreviados = array( 'l','m','x','j','v','s','d' );
	  
	  $html_salida = "";
	  
	  $fecha_base = date_parse_from_format("Y-m-d", $esta_fecha);
	    $ano = $fecha_base["year"];
	    $mes = $fecha_base["month"];
	    $dia = $fecha_base["day"];
	    $u_dia = date("t", strtotime($esta_fecha));

	  $fecha_base_dia_1 = $fecha; //$ano.'-'.$mes.'-01';
	  $fecha_proximo_mes = date( "Y-m-d", strtotime( $fecha_base_dia_1 . " next month" ));        
	  $fecha_anterior_mes = date( "Y-m-d", strtotime( $fecha_base_dia_1 . " previous month" ));
	  $fecha_proximo_ano = date( "Y-m-d", strtotime( $fecha_base_dia_1 . " next year" ));        
	  $fecha_anterior_ano = date( "Y-m-d", strtotime( $fecha_base_dia_1 . " previous year" ));
	  
	  $arreglo_dias=array();
	  $este_mes = $mes;
	  $este_ano = $ano;
	  $dia_ini = 1; // domingo
	  $dia_fin = $u_dia;

	  for( $este_dia=$dia_ini;$este_dia<=$dia_fin;$este_dia++){
	    $esta_fecha=$este_ano.'-'.$este_mes.'-'.$este_dia;
	    $esta_semana = date("W", strtotime($esta_fecha));
	    $este_dia_semana = date("w", strtotime($esta_fecha));

	    // ajuste para que domingo sea 6 y no 0
	    if( $este_dia_semana == 0 ){
	      $este_dia_semana=6;
	    } else {
	      $este_dia_semana--;
	    }

	    if( $esta_semana == 53 ) $esta_semana = 1;
	    $esta_semana = formato_ceros( $esta_semana, 2 );

	    if( ! isset( $arreglo_dias[$esta_semana] ) ){
	      $arreglo_dias[$esta_semana] = array_fill_keys( $dias_abreviados, 0 );
	    }
	    $arreglo_dias[$esta_semana][$dias_abreviados[$este_dia_semana]] = $este_dia;
	  } 
	  
	  $tags = ( is_array( $tags_contenedor ) ) ? $tags_contenedor : explode("|", $tags_contenedor );
	  
	  $html_salida = $tags[0];
	  $html_salida .= '<table class="html-lista mes"><tr class="encabezado_mes">
	    <th><a href="javascript:mostrar_calendario(\''.$fecha_anterior_ano.'\', \'calendario_comun\')"><i class="fa fa-angle-double-left"></i></a> <a href="javascript:mostrar_calendario(\''.$fecha_anterior_mes.'\', \'calendario_comun\')"><i class="fa fa-angle-left"></i></a></th>
	    <th colspan="5"> '.$meses[$este_mes].' '.$este_ano.'</th>
	    <th><a href="javascript:mostrar_calendario(\''.$fecha_proximo_mes.'\', \'calendario_comun\')"><i class="fa fa-angle-right"></i></a> <a href="javascript:mostrar_calendario(\''.$fecha_proximo_ano.'\', \'calendario_comun\')"><i class="fa fa-angle-double-right"></i></a></th>
	    </tr>';
	  $html_salida .= '<tr class="encabezado_dias">';
	  foreach ($dias_abreviados as $dia ) {
	    $html_salida .= "<th>".$dia."</th>";
	  }
	  $html_salida .= '</tr>';

	  foreach ($arreglo_dias as $esta_semana => $dias_esta_semana ) {
	    $html_salida .= "<tr class='fila-registro seleccionable'>";
	    
	    foreach( $dias_esta_semana as $este_dia_semana => $este_dia_n) {
	      if( $este_dia_n == 0 ){ $este_dia_n = ''; }
	      
	      $fecha_este_dia = date("Y-m-d", strtotime($este_ano.'-'.$este_mes.'-'.$este_dia_n));

	      $clase_valor_inicial = "";
	      if( $fecha_este_dia == $valor_inicial ){
	        $clase_valor_inicial = " class='fecha-activa' ";
	      }

	      $enlace_clic = str_replace( '{@fecha_dia}', $este_ano.'-'.$este_mes.'-'.$este_dia_n, $enlace );
	      $html_salida .= "<td ".$clase_valor_inicial."><a href='".$enlace_clic."' target='".$destino."'>".$este_dia_n."</a></td>";
	    }

	    $html_salida .= "</tr>";
	  }

	  $html_salida .= '<tr><td colspan="6">'.$valor_inicial.'</td><td><i></i></td></tr>';          

	  $html_salida .= '</table>';
	  $html_salida .= $tags[1];

	  $html_scripts .= "";

	  return $html_salida;
	}
}


/* End of file componentes_html_helper.php */
/* Location: ./aplicacion_base/helpers/componentes_html_helper.php */
