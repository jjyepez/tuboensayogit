
  <?php if ( ! isset( $titulo_dialogo ) ) $titulo_dialogo = "_titulo"; ?>
  <h2 class="titulo-seccion normal seguido"><?=$titulo_dialogo?></h2>

  <div class="cerrar-velo-blanco flotado-derecha">
    <a href="javascript:cerrar_velo();"><i class="fa fa-share-square-o fa-flip-horizontal"></i></a>
  </div>

  <hr>

  <?php if ( ! isset( $instrucciones ) ) $instrucciones = "_instrucciones"; ?>
  <?=html_etiqueta( $instrucciones )?>

  <?=html_br('7px')?>
  
  <?php
    if( ! isset( $columnas ) ) { $columnas = array(); }
    if( ! isset( $formato_columnas ) ) { $formato_columnas = array(); }
    if( ! isset( $ancho_columnas ) ) { $ancho_columnas = array(); }
    $parametros = array(
      'columnas'         => $columnas,
      'formato_columnas' => $formato_columnas,
      'ancho_columnas'   => $ancho_columnas,
    );
  ?>

  <?php /*
    $html_grid = "";

    if ( ! isset( $tipo_dialogo ) ) {
      $tipo_dialogo = 'default';
    }
    switch ( $tipo_dialogo ) {
      case 'plus-divs':
        $html_grid =  html_grid_simple_plus_divs ( 'grid_' . $id_dialogo , $datos, $parametros );
        break;
      
      default:
        $html_grid .= '<div class="contenedor-grid alto-245">';
        $html_grid .= html_grid_simple ( 'grid_' . $id_dialogo , $datos, $parametros );
        $html_grid .= '</div>';
        break;
    }
  ?>
  <?=$html_grid?>

  <?=html_br('5px')?>

  <?php 
    $html_grid = "";

    switch ( $id_dialogo ) {
      case 'recetas': 
      // se debe REVISAR, MEJORAR y convertir en una funcionalidad generica para cualquier grid o lista de datos !!!!!!!!!!!!!! .... jjy ?>
        <?php
          if ( ! isset( $valores_adicionales ) ) {
            $valores_adicionales = "";
          }
          $parametros = array(
            'metodo' => 'POST',
            'enlace' => 'javascript:filtrar_dialogo( "'.$id_dialogo.'", armar_filtro_grid("'.$id_dialogo.'"), "'.$valores_adicionales.'");',
          );
          ?>
          <?=html_formulario_ini('f_filtro_grid_' . $id_dialogo, $parametros)?>

          <?php
            $parametros = array(
              'items' => $columnas,
              'clases_adicionales' => 'ancho-200',
            );
          ?>
          <?=html_input('l_columna_filtro','lista', $parametros)?>
          <?=html_sangria('3px')?>

          <?php
            $parametros = array(
              'items' => array(
                'ilike' => 'parecido a',
                //'<>'   => 'diferente a', .. puede confundir
                //'='    => 'igual a', .. puede confindir
                '>'    => 'mayor que',
                '<'    => 'menor que',
              ),
              'clases_adicionales' => 'ancho-50',
            );
          ?>
          <?=html_input('l_operador_filtro','lista', $parametros)?>
          <?=html_sangria('3px')?>

          <?php
            $id = 't_terminos_filtro';
            $parametros = array(
              'parametros_html' => 'placeholder="Términos de filtrado" ',
              'estilos' => 'width:250px;',
              'funciones_especiales' => array(
                array( $id.'_b1', 'icon-filter', array( 'enlace' => 'javascript:filtrar_dialogo( "'.$id_dialogo.'", armar_filtro_grid("'.$id_dialogo.'"), "'.$valores_adicionales.'" );' ) ),
              ),
            );
          ?>
          <?=html_input( $id, 'texto_funcion_especial', $parametros )?>

          <?=html_formulario_fin()?>
        <?php
        break;
    } */
  ?>






  
  <?php //$this->load->view('pie_comun_modulo_v') // no modificar esta linea ... ..jy ?>