<?php $this->load->view('encabezado_comun_modulo_v') ?>
</head><body>
<?php $html_general_componentes = ""; //almacenara los HTMLS de apoyo que vienen de los componentes, como el calendario, la calculadora, etc. ?>

	<h2 class = "titulo-seccion">M&oacute;dulo tipo Portal</h2>
	<?=html_hr()?>

  <div id="desplazable" class="area-desplazable" style="top:33px;">

    <?=html_br('10px')?>
    
    <center>

    <h2>Redes Sociales</h2>
    <p>Instrucciones de este módulo</p>
    <?=html_br('20px')?>
    
    <?php
      $parametros = array(
        'estilos_adicionales'      => 'max-width: 250px;',
        'color_principal'          => 'gris-claro',
        'redondear_esquinas'       => '0px',
        'icono'                    => base_url().'/imgs/logo_inn.png',
        'apariencia'               => 'sombra-2d',
        'clases_adicionales_link'  => 'thumbnail',
        'clases_adicionales_icono' => 'color-negro',
        'enlace'                   => array( 'http://inn.gob.ve' ), // forma correcta de pasar enlaces : array( 'http://url.com', "_frame" )!
        'texto'                    => 'Sección',
        'descripcion'              => 'Lorem Ipsun dolor sit amet<br>conrcectetum oblivio septum.',
      );
    ?>
    <?=html_bs_boton( 'b', $parametros, $html_general_componentes )?>

    <?php 
      $parametros['icono']           ='fa-cutlery';
      $parametros['color_principal'] ='amarillo';
      $parametros['enlace'] = array('javascript:popup_en_desarrollo()');
    ?>
    <?=html_bs_boton( 'b', $parametros, $html_general_componentes )?>
    
    <?php 
      $parametros['icono']           ='fa-cutlery';
      $parametros['color_principal'] ='azul-claro';
    ?>
    <?=html_bs_boton( 'b', $parametros, $html_general_componentes )?>

    <?=html_br()?>
    
    <?php $parametros['icono']='fa-stack-overflow'?>
    <?=html_bs_boton( 'b', $parametros, $html_general_componentes )?>

    <?php 
      $parametros['icono']='fa-crop';
      $parametros['color_principal'] ='rojo';
    ?>
    <?=html_bs_boton( 'b', $parametros, $html_general_componentes )?>

    <?php 
      $parametros['icono']           = 'fa-envelope';
      $parametros['color_principal'] = 'azul';
      $parametros['texto']           = 'Correo Institucional';
      $parametros['enlace']          = array( 'http://webmail.noesis.com.ve', 'parent.document' );
    ?>
    <?=html_bs_boton( 'b', $parametros, $html_general_componentes )?>

  </center>

</div>

<script type="text/javascript">
  function popup_en_desarrollo(){
    var parametros = {
      'texto'   :'Esta funcionalidad se encuentra<br>En Desarrollo.',
      'titulo'  :'En Desarrollo',
      'tipo'    :'advertencia',
      'icono'   : 'fa-cogs',
      'botones' :{
        'aceptar':'javascript:alert(1);',
        'cancelar':'javascript:cerrar_velo(2);',
      },
      'alto'    :250,
      'ancho'   :400,
      'boton_cerrar':true,
    }
    mostrar_popup( parametros );
  }
</script>

<?=html_preparar_popup( $html_general_componentes )?>

<?= trim( str_replace( "\t",'',$html_general_componentes ) )?>

<?php $this->load->view('pie_comun_modulo_v') ?>