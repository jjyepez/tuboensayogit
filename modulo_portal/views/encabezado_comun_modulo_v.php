<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset='utf-8'>

  <link rel="stylesheet" type="text/css" href="<?=base_url().'css/fuentes_libres.css'?>">

  <link rel="stylesheet" type="text/css" href="<?=base_url()?>libs/font-awesome/4.0.3/css/font-awesome.min.css"> 
  <link rel="stylesheet" href="<?=base_url()?>libs/bootstrap/3.1.1/css/bootstrap.min.css">
  <!--link rel="stylesheet" href="<?=base_url()?>libs/bootstrap/3.1.1/css/sticky-footer.css"-->

  <link rel="stylesheet" href="<?=base_url()?>libs/bootstrap/3.1.1/themes/<?=$GLOBALS['config']['aplicacion']['tema_bs']?>/bootstrap.css">

	<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/estilos.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?><?=$GLOBALS['config']['modulo']['id']?>/css/estilos_propios.css">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) ... v2 jjy -->
  <script src="<?=base_url()?>libs/jquery/1.11.0/jquery.min.js"></script>
