/**
 * Plantilla-OSTI 2.1b - Mayo 2014 (04-05-2014) ...
 * Estructura basada en CodeIgniter para la creaci�n de aplicaciones en el INN-CCS-VE
 * 
 * @author jjyepez - jyepez@inn.gob.ve + Equipo de desarrollo OSTI-INN
 *
 * @package plantilla-osti
 * @version 2.1b
 * @license http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es
 * 
 */
/* *******************************************************************
| Datos sobre la aplicaci�n ...
|
| NOVEDADES en la v2.1b 
|  - Lista para INTRANET INN (modulo tipo portal - demo)
|  - Implementados scroll personalizados
|  - Implementado formato en componentes
|  - Mejoras en los componentes 2.1.x
| 
| POR HACER
|  - Corregir manejo de sesiones de usuarios
|  - Mejorar el manejo de scrollbars para IFRAME de m�dulos
|  - etc, etc ... 
| 
|  --
|  Adelante!
|  jjy
|
/* ******************************************************************/