<?php $this->load->view('encabezado_propio_v') ?>

	<style>
		pre{
			padding:0;margin:0;
		} 
		.tabla-demo > tbody > tr > td {
			padding:10px 10px 5px 10px;
		}
	</style>

</head>
<?php //************************************* </head><body> ********************************* ?>
<body>

<?php /**
			OJO CON LA VARIABLE $html_general_componentes
**/ ?>
<?php $html_general_componentes = ""; //almacenara los HTMLS de apoyo que vienen de los componentes, como el calendario, la calculadora, etc. ?>

	<h2 class = "titulo-seccion">M&oacute;dulo Demostrativo</h2>
	<hr>

		<?php
			$parametros = array(
							'enlace'		 => site_url().'',
							'destino'		 => '_self',
							'tipo'           => 'boton_icono',
							'icono'          => 'icon-save icon-large ',
							'clases_adicionales' => 'btn-primary',
							'descripcion' => 'Guardar',
						);
		?>
		<?=html_enlace_boton( 'bt_buscar', $parametros )?>

		<?php
			$parametros = array(
							'enlace'		 => 'http://google.com',
							'destino'		 => '_self',
							'tipo'           => 'boton_icono',
							'icono'          => 'icon-reply icon-large',
							'descripcion'		 => 'Volver'
						);
		?>
		<?=html_enlace_boton( 'bt_volver', $parametros )?>

		<?=html_sangria('20px')?>
		<?=html_etiqueta('Demostraci&oacute;n del Uso de los Componentes')?>

		<div class="btn-group flotado-derecha">

			<?php
				$parametros = array(
								'enlace'		 => 'http://google.com',
								'destino'		 => '_self',
								'tipo'           => 'boton_icono',
								'icono'          => 'icon-chevron-left',
								'tooltip'		 => 'Ir al anterior'
							);
			?>
			<?=html_enlace_boton( 'bt_buscar', $parametros )?>
			<?php
				$parametros = array(
								'enlace'		 => 'http://google.com',
								'destino'		 => '_self',
								'tipo'           => 'boton_icono',
								'icono'          => 'icon-chevron-right',
								'tooltip'		 => 'Ir al anterior'
							);
			?>
			<?=html_enlace_boton( 'bt_buscar', $parametros )?>

		</div>

		<?=html_br('7px')?><?=html_hr()?><?=html_br('5px')?>
		
			<?php
				$pestanas = array(
					'tab_basicos'   => 'Componentes Básicos',
					'tab_derivados' => 'Componentes Derivados',
					'tab_avanzados' => 'Componentes Avanzados',
				);
				$parametros = array(
					'id_pestana_activa' => 'tab_basicos',
					'pestanas' => $pestanas,
				);
			?>
			<?=html_pestanas( 'id_pestanas', $parametros )?>		

			<?=html_formulario_ini( 'f' )?>
		
			<?=html_br()?>
			<?php
			/** 
			------------------------------------------------------- jjy v2
				TODOS LOS COMPONENTES!
			-------------------------------------------------------- 
			**/
			?>

			<div class="pestanas">

				<div style="height:350px;" class="area-desplazable">

				<div id="tab_basicos" class="tab_activo">

			<?php
						$items_ejemplo = array( 'item_1' => 'Item 1', 'item_2' => 'Item 2', 'item_3' => 'Item 3', 'item_4' => 'Item 4', );

            $parametros_comunes = array(
              'clases_adicionales_etiqueta' => 'ancho-120',
            );
          ?>
          <?php
          	$n=0;
            $campos = array(
							'c'.(++$n) => array( 'texto','texto', array( 'parametros_html' => 'placeholder = "escriba aquí"', 'info_ayuda' => 'Texto de ayuda para este campo.' ) ),
							'c'.(++$n) => array( 'contraseña','contraseña', array() ),
							'c'.(++$n) => array( 'lista','lista', array( 'items' => $items_ejemplo ) ), 
							'c'.(++$n) => array( 'archivo','archivo', array() ),
							'c'.(++$n) => array( 'imagen','imagen', array( 'url_imagen' => '../../imgs/logo_especial_gobierno.png' ) ),
							//'c'.(++$n) => array( 'texto_enriquecido','texto_enriquecido', array() ),
							'c'.(++$n) => array( 'buleano','buleano', array() ),
							'c'.(++$n) => array( 'oculto','oculto', array() ),
            );
          ?>
          <?php $i=0; foreach ( $campos as $id_campo => $info_parametros ) { $i++; ?>
            
            <table class="tabla-demo" style="width:100%;"><tr <?=($i%2)?" style='background-color:#fdfccf;' ":""?> ><td width='60%'>

	              <?php 
	                $parametros = $parametros_comunes 
	                            + array( 
	                                'etiqueta'      => ucfirst( $info_parametros[0] ) . ':',
	                              )
	                            + ( ( isset($info_parametros[2]) )?$info_parametros[2] : array() );  
	                            // $info_parametros[2] --> $parametros (array) segun componente ... jjy v2
	              ?>
	              <?=html_input( $id_campo, $info_parametros[1], $parametros, $html_general_componentes )?>

	              <?=html_sangria('10px')?><a class="" href="javascript:mostrar_codigo('cod_<?=$id_campo?>');"><small class="discreto">&lt;?&gt;</small></a>
	              <div id="cod_<?=$id_campo?>" style="display:none">
	              	<code>
	              		&lt;?=html_input( 'id_componente', '<?=$info_parametros[1]?>', $parametros )?&gt;
	              	</code><?=html_br('5')?>

	              	<pre>$parametros</pre>
	              	<?php prp($parametros); ?>

	              </div>
	            </td></tr>

	          </table>

          <?php } ?>
				
				</div><!-- tab_basicos -->
				
				<div id="tab_derivados" class="invisible">

				<?php
						$items_ejemplo = array( 'item_1' => 'Item 1', 'item_2' => 'Item 2', 'item_3' => 'Item 3', 'item_4' => 'Item 4', );

            $parametros_comunes = array(
              'clases_adicionales_etiqueta' => 'ancho-120',
            );
          ?>
          <?php
						$campos = array(
							//'c'.(++$n) => array( 'texto_funcion_especial','texto_funcion_especial', array() ),
							//'c'.(++$n) => array( 'texto_funciones_especiales','texto_funciones_especiales', array() ),
							//'c'.(++$n) => array( 'combo','combo', array() ),
							'c'.(++$n) => array( 'contador','contador', array( 'parametros_html' => 'placeholder = "entre -10 y 10"', 'parametros_adicionales' => array( 'valor_minimo' => -10, 'valor_maximo' => 10, 'factor' => 0.5 ) ) ),
							'c'.(++$n) => array( 'fecha','fecha', array() ),
							'c'.(++$n) => array( 'telefono-celular','text', array( 'parametros_html' => "alt='tlf_celular' placeholder='(999) 999-99.99'" ) ),
							'c'.(++$n) => array( 'hora','hora', array() ),
							'c'.(++$n) => array( 'calendario','calendario', array( 'valor_inicial' => date("d/m/Y") ) ),
							'c'.(++$n) => array( 'reloj','reloj', array() ),
							'c'.(++$n) => array( 'calculadora','calculadora', array( 'parametros_html' => "alt='decimal'" ) ),
							'c'.(++$n) => array( 'seleccionmultiple','seleccionmultiple', array( 'items' => $items_ejemplo ) ),
							'c'.(++$n) => array( 'seleccionsimple','seleccionsimple', array( 'items' => $items_ejemplo ) ),
							//'c'.(++$n) => array( 'texto_enriquecido','texto_enriquecido', array() ),
            );
          ?>
          <?php $i=0; foreach ( $campos as $id_campo => $info_parametros ) { $i++; ?>
            
            <table class="tabla-demo" style="width:100%;"><tr <?=($i%2)?" style='background-color:#fdfccf;' ":""?> ><td width='60%'>

	              <?php 
	                $parametros = $parametros_comunes 
	                            + array( 
	                                'etiqueta'      => ucfirst( $info_parametros[0] ) . ':',
	                              )
	                            + ( ( isset($info_parametros[2]) )?$info_parametros[2] : array() );  
	                            // $info_parametros[2] --> $parametros (array) segun componente ... jjy v2
	              ?>
	              <?=html_input( $id_campo, $info_parametros[1], $parametros, $html_general_componentes )?>

	              <?=html_sangria('10px')?><a class="" href="javascript:mostrar_codigo('cod_<?=$id_campo?>');"><small class="discreto">&lt;?&gt;</small></a>
	              <div id="cod_<?=$id_campo?>" style="display:none">
	              	<code>
	              		&lt;?=html_input( 'id_componente', '<?=$info_parametros[1]?>', $parametros )?&gt;
	              	</code><?=html_br('5')?>

	              	<pre>$parametros</pre>
	              	<?php prp($parametros); ?>

	              </div>
	            </td></tr>

	          </table>

          <?php } ?>
				</div><!-- tab_derivados -->
				
				<div id="tab_avanzados" class="invisible">

						<?php
							$data_ejemplo = array(
								0 => array( 'id' => 0, 'nombre' => 'Luis Perez', 'telefono' => '555.1111', 'correo' => 'correo@dominio.com' ),
								1 => array( 'id' => 1, 'nombre' => 'Jaime Lopez', 'telefono' => '555.2222', 'correo' => 'correo@dominio.com' ),
								2 => array( 'id' => 2, 'nombre' => 'Raul Sanchez', 'telefono' => '555.3333', 'correo' => 'correo@dominio.com' ),
								3 => array( 'id' => 3, 'nombre' => 'Javier Madrid', 'telefono' => '555.4444', 'correo' => 'correo@dominio.com' ),
								4 => array( 'id' => 4, 'nombre' => 'Jose Gomez', 'telefono' => '555.7777', 'correo' => 'correo@dominio.com' ),
								5 => array( 'id' => 5, 'nombre' => 'Alfredo Ramos', 'telefono' => '555.0000', 'correo' => 'correo@dominio.com' ),
							);

							$items_ejemplo = array( 'item_1' => 'Item 1', 'item_2' => 'Item 2', 'item_3' => 'Item 3', 'item_4' => 'Item 4', );

	            $parametros_comunes = array(
	              
	            );

	            $parametros = $parametros_comunes
	            						+ array(
	            							'mostrar-acciones' => TRUE,
	            							'iconos_accion'         => array( 'mostrar', 'imprimir', 'editar', 'eliminar' ), // opcionales ... jjy
	            						);
	            							
	            $id_campo = 'id_grid1';
	          ?>

	          	<table class="tabla-demo" style="width:100%;"><tr <?=(($i++)%2)?" style='background-color:#fdfccf;' ":""?> ><td width='60%'>
	          			<?=html_etiqueta('Grid simple:')?>
	          			<?=html_grid_simple( $id_campo, $data_ejemplo, $parametros )?>

		              <?=html_sangria('10px')?><a class="" href="javascript:mostrar_codigo('cod_<?=$id_campo?>');"><small class="discreto">&lt;?&gt;</small></a>
		              <div id="cod_<?=$id_campo?>" style="display:none">
		              	<code>
		              		&lt;?=html_grid_simple( $id_campo, $data, $parametros )?&gt;
		              	</code><?=html_br('5')?>

		              	<pre>$data</pre>
		              	<?php prp($data_ejemplo); ?>
		              	
		              	<pre>$parametros</pre>
		              	<?php prp($parametros); ?>

		              </div>
		            </td></tr>

		          </table>

	          	<?php
	          		$columnas = array( // deben conincidir con los datos!
									'id'       => 'ID',
									'nombre'   => 'Nombre',
									'telefono' => 'Teléfono',
									'correo' => 'Correo',
	          		);
	          		$formato_columnas = array();
	          		$ancho_columnas = array( // deben conincidir con los datos!
									'id'       => '50px',
									'nombre'   => '200px',
									'telefono' => '120px',
									'correo' => '120px',
	          		);

	          		$parametros += array( 
									'columnas'           => $columnas,
									'formato_columnas'   => $formato_columnas,
									'ancho_columnas'     => $ancho_columnas,
									'ancho_grid'         => '100%',
									'alto_grid'          => '150px',
	          		);
	          	?>
							<table class="tabla-demo" style="width:100%;"><tr <?=(($i++)%2)?" style='background-color:#fdfccf;' ":""?> ><td width='60%'>
	          			
	          			<?=html_etiqueta('Lista Datos:'); $id_campo = 'lista_datos_1';?>
	          			<?=html_lista_datos ( $id_campo, $data_ejemplo, $parametros )?>

		              <?=html_sangria('10px')?><a class="" href="javascript:mostrar_codigo('cod_<?=$id_campo?>');"><small class="discreto">&lt;?&gt;</small></a>
		              <div id="cod_<?=$id_campo?>" style="display:none">
		              	<code>
		              		&lt;?=html_grid_simple( $id_campo, $data, $parametros )?&gt;
		              	</code><?=html_br('5')?>

		              	<pre>$data</pre>
		              	<?php prp('Ver ejemplo Grid simple.'); ?>
		              	
		              	<pre>$parametros</pre>
		              	<?php prp($parametros); ?>

		              </div>
		            </td></tr>

		          </table>


							<table class="tabla-demo" style="width:100%;"><tr <?=(($i++)%2)?" style='background-color:#fdfccf;' ":""?> ><td width='60%'>
	          			
	          			<?=html_etiqueta('Grid Simple Plus Divs (revisar):'); $id_campo = 'gspdivsp_1';?>
	          			<?=html_grid_simple_plus_divs ( $id_campo, $data_ejemplo, $parametros )?>

		              <?=html_sangria('10px')?><a class="" href="javascript:mostrar_codigo('cod_<?=$id_campo?>');"><small class="discreto">&lt;?&gt;</small></a>
		              <div id="cod_<?=$id_campo?>" style="display:none">
		              	<code>
		              		&lt;?=html_grid_simple_plus_divs( $id_campo, $data, $parametros )?&gt;
		              	</code><?=html_br('5')?>

		              	<pre>$data</pre>
		              	<?php prp('Ver ejemplo Grid simple.'); ?>
		              	
		              	<pre>$parametros</pre>
		              	<?php prp($parametros); ?>

		              </div>
		            </td></tr>

		          </table>


							<table class="tabla-demo" style="width:100%;"><tr <?=(($i++)%2)?" style='background-color:#fdfccf;' ":""?> ><td width='60%'>
	          			
	          			<?=html_etiqueta('Grid Simple Plus Divs Prueba (revisar):'); $id_campo = 'gspdivs_1';?>
	          			<?=html_grid_simple_plus_divs_prueba( $id_campo, $data_ejemplo, $parametros )?>

		              <?=html_sangria('10px')?><a class="" href="javascript:mostrar_codigo('cod_<?=$id_campo?>');"><small class="discreto">&lt;?&gt;</small></a>
		              <div id="cod_<?=$id_campo?>" style="display:none">
		              	<code>
		              		&lt;?=html_grid_simple_plus_divs_prueba( $id_campo, $data, $parametros )?&gt;
		              	</code><?=html_br('5')?>

		              	<pre>$data</pre>
		              	<?php prp('Ver ejemplo Grid simple.'); ?>
		              	
		              	<pre>$parametros</pre>
		              	<?php prp($parametros); ?>

		              </div>
		            </td></tr>

		          </table>

					</div><!-- tab_avanzados -->

				</div><!-- area-desplazable -->

			</div><!-- pestanas -->

		<?=html_formulario_fin()?>

</div>
<?php 
	$parametros = array(
		'url_ajax' => '?',
	)	;
?>
<?=html_dialogo_emergente_ajax( $parametros, $html_general_componentes )?>
<?=$html_general_componentes?>

<script type="text/javascript">
	function mostrar_codigo( id_componente ){
		//alert(id_componente);
		$( '#' + id_componente ).toggle(100);
	}
	$(document).ready(function(){
		$.mask.masks.tlf_celular = {mask: '(0426) 999-99.99'}; // define formato personalizado! ... jjy v2
		$('.area-desplazable').ClassyScroll();
	});
</script>

<?php /**
	HASTA AQUI LLEGA EL CONTENIDO EXCLUSIVO DE ESTA VISTA ... jjy
**/?>
<?php $this->load->view('pie_propio_v') ?>